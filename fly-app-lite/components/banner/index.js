/** mode 有效值：mode 有 13 种模式，其中 4 种是缩放模式，9 种是裁剪模式。
 模式 值 说明
 缩放 scaleToFill 不保持纵横比缩放图片，使图片的宽高完全拉伸至填满 image 元素
 缩放 aspectFit 保持纵横比缩放图片，使图片的长边能完全显示出来。也就是说，可以完整地将图片显示出来。
 缩放 aspectFill 保持纵横比缩放图片，只保证图片的短边能完全显示出来。也就是说，图片通常只在水平或垂直方向是完整的，另一个方向将会发生截取。
 缩放 widthFix 宽度不变，高度自动变化，保持原图宽高比不变
 裁剪 top 不缩放图片，只显示图片的顶部区域
 裁剪 bottom 不缩放图片，只显示图片的底部区域
 裁剪 center 不缩放图片，只显示图片的中间区域
 裁剪 left 不缩放图片，只显示图片的左边区域
 裁剪 right 不缩放图片，只显示图片的右边区域
 裁剪 top left 不缩放图片，只显示图片的左上边区域
 裁剪 top right 不缩放图片，只显示图片的右上边区域
 裁剪 bottom left 不缩放图片，只显示图片的左下边区域
 裁剪 bottom right 不缩放图片，只显示图片的右下边区域
 */

const BANNER_TYPE = {
    NONE: '1', // 纯幻灯片
    LITE: '2', // 生活服务区
    SHOP: '3', // 商品
    LINK: '4', // 外部链接
    CONTENT: '5' // 自定义内容
}

/**
 * 轮播图
 */
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        data: {
            type: Object,
            value: []
        },
        /* 是否显示面板指示点 */
        indicatorDots: {
            type: Boolean,
            value: true
        },
        /* 指示点颜色  */
        indicatorColor: {
            type: String,
            value: '#F5F5F5'
        },
        /* 当前选中的指示点颜色 */
        indicatorActiveColor: {
            type: String,
            value: '#FFFFFF'
        },
        /* 是否自动切换 */
        autoplay: {
            type: Boolean,
            value: true
        },
        /* 指定 swiper 切换缓动动画类型  */
        easingFunction: {
            type: String,
            value: 'default'
        },
        /* 自动切换时间间隔 */
        interval: {
            type: Number,
            value: 5000
        },
        /* 滑动动画时长 */
        duration: {
            type: Number,
            value: 150
        },
        /* 是否采用衔接滑动 */
        circular: {
            type: Boolean,
            value: true
        },
        /* 滑动方向是否为纵向 */
        vertical: {
            type: Boolean,
            value: false
        },
        /* 前边距，可用于露出前一项的一小部分，接受 px 和 rpx 值 */
        previousMargin: {
            type: String,
            value: '15rpx'
        },
        /* 后边距，可用于露出后一项的一小部分，接受 px 和 rpx 值 */
        nextMargin: {
            type: String,
            value: '15rpx'
        },
        /* 当 swiper-item 的个数大于等于 2，关闭 circular 并且开启 previous-margin 或 next-margin 的时候，
           可以指定这个边距是否应用到第一个、最后一个元素 */
        snapToEdge: {
            type: Boolean,
            value: false
        },
        /* 同时显示的滑块数量 */
        displayMultipleItems: {
            type: Number,
            value: 1
        },
        /* 是否展示标题 */
        showTitle: {
            type: Boolean,
            value: false
        },
        /* 轮播图片缩放 */
        mode: {
            type: String,
            value: 'scaleToFill'
        }
    },

    /**
     * 组件的初始数据
     */
    data: {},

    /**
     * 组件的方法列表
     */
    methods: {
        handlerBannerTop(event) {
            let data = event.currentTarget.dataset;
            let type = data.type;
            /* 纯图片 */
            if (BANNER_TYPE.NONE === type) {
                return
            }
            /* 生活服务区 */
            if (BANNER_TYPE.LITE === type) {
                wx.navigateTo({
                    url: '/pages/litedetails/index?id=' + data.dataid
                })
            }
            /* 商品 */
            if (BANNER_TYPE.SHOP === type) {
                wx.navigateTo({
                    url: '/pages/activity-details/activity-details?id=' + data.dataid
                })
            }
            /* 外部链接 */
            if (BANNER_TYPE.LINK === type) {
                wx.navigateTo({
                    url: '/pages/webview/index?uri=' + data.link
                })
            }
            /* 自定义内容 */
            if (BANNER_TYPE.CONTENT === type) {
                wx.navigateTo({
                    url: '/pages/banner-content/index?id=' + data.id
                })
            }
        }
    }
})
