// components/swiper-images/swiper-images-item/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        banner: {
            type: Object,
            value: null
        },
    },

    /**
     * 组件的初始数据
     */
    data: {},

    /**
     * 组件的方法列表
     */
    methods: {
        handlerBannerTop(event) {
            let id = event.currentTarget.dataset.id;
            wx.navigateTo({
                url: `/pages/article-detail/article-detail?id=` + id
            })
        }
    }
})
