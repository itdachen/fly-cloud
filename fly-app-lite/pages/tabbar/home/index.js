// pages/tabbar/home/index.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        appletInfo: null,
        shopName: '优零邻go',
        backgroundColor: 'rgba(224, 48, 31, 100)',
        logo: 'https://itdachen.com/upload/2022/9/20220921005515769765723529953280.png',
        background: 'https://itdachen.com/upload/2022/9/202209211434221572474299393118208.png',
        banner: [
            {
                showTitle: "0",
                sortNum: null,
                title: "lalNNN",
                type: "1",
                typeId: null,
                url: "/images/more/swiper2.jpg"
            },
            {
                showTitle: "0",
                sortNum: null,
                title: "NNNNNNNNNNNNNNNNNN",
                type: "1",
                typeId: null,
                url: "/images/more/swiper1.jpg"
            },
            {
                showTitle: "0",
                sortNum: null,
                title: "lalNNN",
                type: "1",
                typeId: null,
                url: "/images/more/swiper3.jpg"
            }

        ],

        /* 菜单 */
        navMenuArr: [
    "/images/more/list1.png",
    "/images/more/list2.png",
    "/images/more/list3.png",
    "/images/more/list4.png",
    "/images/more/list5.png",
    "/images/more/list6.png",
    "/images/more/list7.png",
    "/images/more/list8.png",
    "/images/more/list9.png",
    "/images/more/list10.png"
        ],

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})