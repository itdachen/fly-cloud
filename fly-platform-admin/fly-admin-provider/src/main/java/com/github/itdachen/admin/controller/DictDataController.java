package com.github.itdachen.admin.controller;

import com.github.itdachen.admin.service.IDictDataService;
import com.github.itdachen.admin.sdk.dto.DictDataDTO;
import com.github.itdachen.admin.sdk.query.DictDataQuery;
import com.github.itdachen.admin.sdk.vo.DictDataVO;
import com.github.itdachen.framework.context.annotation.FuncTitle;
import com.github.itdachen.framework.webmvc.controller.BizController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 数据字典信息表
 *
 * @author 王大宸
 * @date 2025-01-15 20:38:26
 */
@RestController(value = "com.github.itdachen.admin.controller.DictDataController")
@RequestMapping("/dict/data")
@FuncTitle("数据字典信息表")
public class DictDataController extends BizController< IDictDataService, DictDataDTO, DictDataVO, DictDataQuery, String > {
    private static final Logger logger = LoggerFactory.getLogger(DictDataController.class);

}