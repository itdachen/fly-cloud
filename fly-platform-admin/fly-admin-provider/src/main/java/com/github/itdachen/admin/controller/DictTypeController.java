package com.github.itdachen.admin.controller;

import com.github.itdachen.admin.service.IDictTypeService;
import com.github.itdachen.admin.sdk.dto.DictTypeDTO;
import com.github.itdachen.admin.sdk.query.DictTypeQuery;
import com.github.itdachen.admin.sdk.vo.DictTypeVO;
import com.github.itdachen.framework.context.annotation.FuncTitle;
import com.github.itdachen.framework.webmvc.controller.BizController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 数据字典类型
 *
 * @author 王大宸
 * @date 2025-01-15 20:38:26
 */
@RestController(value = "com.github.itdachen.admin.controller.DictTypeController")
@RequestMapping("/dict/type")
@FuncTitle("数据字典类型")
public class DictTypeController extends BizController< IDictTypeService, DictTypeDTO, DictTypeVO, DictTypeQuery, String > {
    private static final Logger logger = LoggerFactory.getLogger(DictTypeController.class);

}