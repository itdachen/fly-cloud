package com.github.itdachen.admin.controller;

import com.github.itdachen.admin.sdk.dto.UserRoleInfoDTO;
import com.github.itdachen.admin.sdk.query.UserRoleInfoQuery;
import com.github.itdachen.admin.sdk.vo.UserRoleInfoVO;
import com.github.itdachen.admin.service.IUserRoleInfoService;
import com.github.itdachen.framework.context.annotation.FuncTitle;
import com.github.itdachen.framework.context.annotation.Log;
import com.github.itdachen.framework.core.constants.LogType;
import com.github.itdachen.framework.core.response.ServerResponse;
import com.github.itdachen.framework.core.response.TableData;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

/**
 * 人员身份信息
 *
 * @author 王大宸
 * @date 2025-01-25 16:33
 */
@RestController
@RequestMapping("/employee/identity")
@FuncTitle("人员身份信息")
public class EmployeeIdentityController {
    private static final Logger logger = LoggerFactory.getLogger(EmployeeIdentityController.class);

    private final IUserRoleInfoService userRoleInfoService;

    public EmployeeIdentityController(IUserRoleInfoService userRoleInfoService) {
        this.userRoleInfoService = userRoleInfoService;
    }


    @GetMapping(value = "/page")
    @ResponseBody
    @Log(title = "分页查询", type = LogType.GET_PAGE_DATA)
    public ServerResponse<TableData<UserRoleInfoVO>> page(UserRoleInfoQuery params) throws Exception {
        return ServerResponse.ok(userRoleInfoService.page(params));
    }

    @PutMapping(value = "/{pk}")
    @ResponseBody
    @Log(title = "修改/编辑", type = LogType.UPDATE)
    public ServerResponse<UserRoleInfoVO> updateInfo(@Valid @RequestBody UserRoleInfoDTO d) throws Exception {
        return ServerResponse.ok(userRoleInfoService.updateInfo(d));
    }

}
