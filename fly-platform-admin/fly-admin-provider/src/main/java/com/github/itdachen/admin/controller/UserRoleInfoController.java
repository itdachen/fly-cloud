package com.github.itdachen.admin.controller;

import com.github.itdachen.admin.service.IUserRoleInfoService;
import com.github.itdachen.admin.sdk.dto.UserRoleInfoDTO;
import com.github.itdachen.admin.sdk.query.UserRoleInfoQuery;
import com.github.itdachen.admin.sdk.vo.UserRoleInfoVO;
import com.github.itdachen.framework.context.annotation.FuncTitle;
import com.github.itdachen.framework.webmvc.controller.BizController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 身份信息
 *
 * @author 王大宸
 * @date 2025-01-11 16:32:42
 */
@RestController
@RequestMapping("/user/role/info")
@FuncTitle("身份信息")
public class UserRoleInfoController extends BizController< IUserRoleInfoService, UserRoleInfoDTO, UserRoleInfoVO, UserRoleInfoQuery, String > {
    private static final Logger logger = LoggerFactory.getLogger(UserRoleInfoController.class);



}