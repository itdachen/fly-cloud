package com.github.itdachen.admin.convert;

import com.github.itdachen.admin.entity.UserRoleInfo;
import com.github.itdachen.admin.sdk.dto.UserRoleInfoDTO;
import com.github.itdachen.admin.sdk.vo.UserRoleInfoVO;
import com.github.itdachen.framework.webmvc.convert.IBizConvertMapper;

/**
 * 身份信息 类型转换
 *
 * @author 王大宸
 * @date 2025-01-11 16:32:42
 */
public class UserRoleInfoConvert implements IBizConvertMapper<UserRoleInfo, UserRoleInfoDTO, UserRoleInfoVO> {

    @Override
    public UserRoleInfo toJavaObject(UserRoleInfoDTO userRoleInfoDTO) {
        if (null == userRoleInfoDTO) {
            return null;
        }
        UserRoleInfo userRoleInfo = new UserRoleInfo();
        userRoleInfo.setId(userRoleInfoDTO.getId());
        userRoleInfo.setTenantId(userRoleInfoDTO.getTenantId());
        userRoleInfo.setUserId(userRoleInfoDTO.getUserId());
        userRoleInfo.setUsername(userRoleInfoDTO.getUsername());
        userRoleInfo.setNickName(userRoleInfoDTO.getNickName());
        userRoleInfo.setRoleName(userRoleInfoDTO.getRoleName());
        userRoleInfo.setMainFlag(userRoleInfoDTO.getMainFlag());
        userRoleInfo.setValidFlag(userRoleInfoDTO.getValidFlag());
        userRoleInfo.setDeptCode(userRoleInfoDTO.getDeptCode());
        userRoleInfo.setDeptTitle(userRoleInfoDTO.getDeptTitle());
        userRoleInfo.setStartTime(userRoleInfoDTO.getStartTime());
        userRoleInfo.setEndTime(userRoleInfoDTO.getEndTime());
        userRoleInfo.setOrderNum(userRoleInfoDTO.getOrderNum());
        userRoleInfo.setRemarks(userRoleInfoDTO.getRemarks());
        return userRoleInfo;
    }


    @Override
    public UserRoleInfoVO toJavaObjectVO(UserRoleInfo userRoleInfo) {
        if (null == userRoleInfo) {
            return null;
        }
        UserRoleInfoVO userRoleInfoVO = new UserRoleInfoVO();
        userRoleInfoVO.setId(userRoleInfo.getId());
        userRoleInfoVO.setTenantId(userRoleInfo.getTenantId());
        userRoleInfoVO.setUserId(userRoleInfo.getUserId());
        userRoleInfoVO.setUsername(userRoleInfo.getUsername());
        userRoleInfoVO.setNickName(userRoleInfo.getNickName());
        userRoleInfoVO.setRoleName(userRoleInfo.getRoleName());
        userRoleInfoVO.setMainFlag(userRoleInfo.getMainFlag());
        userRoleInfoVO.setValidFlag(userRoleInfo.getValidFlag());
        userRoleInfoVO.setDeptCode(userRoleInfo.getDeptCode());
        userRoleInfoVO.setDeptTitle(userRoleInfo.getDeptTitle());
        userRoleInfoVO.setStartTime(userRoleInfo.getStartTime());
        userRoleInfoVO.setEndTime(userRoleInfo.getEndTime());
        userRoleInfoVO.setOrderNum(userRoleInfo.getOrderNum());
        userRoleInfoVO.setRemarks(userRoleInfo.getRemarks());
        return userRoleInfoVO;
    }

    public UserRoleInfo toJavaObject(UserRoleInfoVO userRoleInfoVO) {
        if (null == userRoleInfoVO) {
            return null;
        }
        UserRoleInfo userRoleInfo = new UserRoleInfo();
        userRoleInfo.setId(userRoleInfoVO.getId());
        userRoleInfo.setTenantId(userRoleInfoVO.getTenantId());
        userRoleInfo.setUserId(userRoleInfoVO.getUserId());
        userRoleInfo.setUsername(userRoleInfoVO.getUsername());
        userRoleInfo.setNickName(userRoleInfoVO.getNickName());
        userRoleInfo.setRoleName(userRoleInfoVO.getRoleName());
        userRoleInfo.setMainFlag(userRoleInfoVO.getMainFlag());
        userRoleInfo.setValidFlag(userRoleInfoVO.getValidFlag());
        userRoleInfo.setDeptCode(userRoleInfoVO.getDeptCode());
        userRoleInfo.setDeptTitle(userRoleInfoVO.getDeptTitle());
        userRoleInfo.setStartTime(userRoleInfoVO.getStartTime());
        userRoleInfo.setEndTime(userRoleInfoVO.getEndTime());
        userRoleInfo.setOrderNum(userRoleInfoVO.getOrderNum());
        userRoleInfo.setRemarks(userRoleInfoVO.getRemarks());
        return userRoleInfo;
    }

    public UserRoleInfoVO toJavaObjectVO(UserRoleInfoDTO userRoleInfoDTO) {
        if (null == userRoleInfoDTO) {
            return null;
        }
        UserRoleInfoVO userRoleInfoVO = new UserRoleInfoVO();
        userRoleInfoVO.setId(userRoleInfoDTO.getId());
        userRoleInfoVO.setTenantId(userRoleInfoDTO.getTenantId());
        userRoleInfoVO.setUserId(userRoleInfoDTO.getUserId());
        userRoleInfoVO.setUsername(userRoleInfoDTO.getUsername());
        userRoleInfoVO.setNickName(userRoleInfoDTO.getNickName());
        userRoleInfoVO.setRoleName(userRoleInfoDTO.getRoleName());
        userRoleInfoVO.setMainFlag(userRoleInfoDTO.getMainFlag());
        userRoleInfoVO.setValidFlag(userRoleInfoDTO.getValidFlag());
        userRoleInfoVO.setDeptCode(userRoleInfoDTO.getDeptCode());
        userRoleInfoVO.setDeptTitle(userRoleInfoDTO.getDeptTitle());
        userRoleInfoVO.setStartTime(userRoleInfoDTO.getStartTime());
        userRoleInfoVO.setEndTime(userRoleInfoDTO.getEndTime());
        userRoleInfoVO.setOrderNum(userRoleInfoDTO.getOrderNum());
        userRoleInfoVO.setRemarks(userRoleInfoDTO.getRemarks());
        return userRoleInfoVO;
    }


}