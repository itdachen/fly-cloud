package com.github.itdachen.admin.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import jakarta.persistence.Column;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;



/**
 * 数据字典类型
 *
 * @author 王大宸
 * @date 2025-01-15 20:38:26
 */
@Table(name = "fly_next_dict_type")
public class DictType implements Serializable{
private static final long serialVersionUID=1L;

    /** 数据字典类型ID */
    @Id
    @Column(name = "id")
    private String id;

    /** 字典标签 */
    @Column(name = "dict_label")
    private String dictLabel;

    /** 字典类型 */
    @Column(name = "dict_type")
    private String dictType;

    /** 是否默认（1是 0否） */
    @Column(name = "is_default")
    private String isDefault;

    /** 字典排序 */
    @Column(name = "dict_sort")
    private String dictSort;

    /** 是否展示/有效标志: Y-有效;N-无效 */
    @Column(name = "valid_flag")
    private String validFlag;

    /** 备注 */
    @Column(name = "remarks")
    private String remarks;

    /** 创建者 */
    @Column(name = "create_user")
    private String createUser;

    /** 创建者ID */
    @Column(name = "create_user_id")
    private String createUserId;

    /** 创建时间 */
    @Column(name = "create_time")
    private LocalDateTime createTime;

    /** 更新者 */
    @Column(name = "update_user")
    private String updateUser;

    /** 更新时间 */
    @Column(name = "update_time")
    private LocalDateTime updateTime;

    /** 更新者ID */
    @Column(name = "update_user_id")
    private String updateUserId;





            public void setId(String id) {
                this.id = id;
            }

            public String getId() {
                return id;
            }

            public void setDictLabel(String dictLabel) {
                this.dictLabel = dictLabel;
            }

            public String getDictLabel() {
                return dictLabel;
            }

            public void setDictType(String dictType) {
                this.dictType = dictType;
            }

            public String getDictType() {
                return dictType;
            }

            public void setIsDefault(String isDefault) {
                this.isDefault = isDefault;
            }

            public String getIsDefault() {
                return isDefault;
            }

            public void setDictSort(String dictSort) {
                this.dictSort = dictSort;
            }

            public String getDictSort() {
                return dictSort;
            }

            public void setValidFlag(String validFlag) {
                this.validFlag = validFlag;
            }

            public String getValidFlag() {
                return validFlag;
            }

            public void setRemarks(String remarks) {
                this.remarks = remarks;
            }

            public String getRemarks() {
                return remarks;
            }

            public void setCreateUser(String createUser) {
                this.createUser = createUser;
            }

            public String getCreateUser() {
                return createUser;
            }

            public void setCreateUserId(String createUserId) {
                this.createUserId = createUserId;
            }

            public String getCreateUserId() {
                return createUserId;
            }

            public void setCreateTime(LocalDateTime createTime) {
                this.createTime = createTime;
            }

            public LocalDateTime getCreateTime() {
                return createTime;
            }

            public void setUpdateUser(String updateUser) {
                this.updateUser = updateUser;
            }

            public String getUpdateUser() {
                return updateUser;
            }

            public void setUpdateTime(LocalDateTime updateTime) {
                this.updateTime = updateTime;
            }

            public LocalDateTime getUpdateTime() {
                return updateTime;
            }

            public void setUpdateUserId(String updateUserId) {
                this.updateUserId = updateUserId;
            }

            public String getUpdateUserId() {
                return updateUserId;
            }


    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                    .append("id", getId())
                    .append("dictLabel", getDictLabel())
                    .append("dictType", getDictType())
                    .append("isDefault", getIsDefault())
                    .append("dictSort", getDictSort())
                    .append("validFlag", getValidFlag())
                    .append("remarks", getRemarks())
                    .append("createUser", getCreateUser())
                    .append("createUserId", getCreateUserId())
                    .append("createTime", getCreateTime())
                    .append("updateUser", getUpdateUser())
                    .append("updateTime", getUpdateTime())
                    .append("updateUserId", getUpdateUserId())
                .toString();
    }


}
