package com.github.itdachen.admin.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 操作日志
 *
 * @author 王大宸
 * @date 2025-02-08 15:41:58
 */
@Table(name = "fly_next_oplog_info")
public class OplogInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Id
    @Column(name = "id")
    private String id;

    /**
     * 平台ID
     */
    @Column(name = "plat_id")
    private String platId;

    /**
     * 平台名称
     */
    @Column(name = "plat_title")
    private String platTitle;

    /**
     * 应用ID
     */
    @Column(name = "app_id")
    private String appId;

    /**
     * 应用名称
     */
    @Column(name = "app_name")
    private String appName;

    /**
     * 应用版本号
     */
    @Column(name = "app_version")
    private String appVersion;

    /**
     * 租户ID/公司ID
     */
    @Column(name = "tenant_id")
    private String tenantId;

    /**
     * 租户名称/公司名称
     */
    @Column(name = "tenant_title")
    private String tenantTitle;

    /**
     * 省代码
     */
    @Column(name = "prov_id")
    private String provId;

    /**
     * 省名称
     */
    @Column(name = "prov_name")
    private String provName;

    /**
     * 市/州代码
     */
    @Column(name = "city_id")
    private String cityId;

    /**
     * 市/州名称
     */
    @Column(name = "city_name")
    private String cityName;

    /**
     * 区/县代码
     */
    @Column(name = "county_id")
    private String countyId;

    /**
     * 区/县名称
     */
    @Column(name = "county_name")
    private String countyName;

    /**
     * 部门ID
     */
    @Column(name = "dept_id")
    private String deptId;

    /**
     * 部门名称
     */
    @Column(name = "dept_title")
    private String deptTitle;

    /**
     * 部门等级
     */
    @Column(name = "dept_level")
    private String deptLevel;

    /**
     * 上级部门代码
     */
    @Column(name = "dept_parent_id")
    private String deptParentId;

    /**
     * 用户ID/用户代码
     */
    @Column(name = "user_id")
    private String userId;

    /**
     * 昵称
     */
    @Column(name = "nick_name")
    private String nickName;

    /**
     * 身份ID/角色ID
     */
    @Column(name = "role_id")
    private String roleId;

    /**
     * 身份名称
     */
    @Column(name = "role_name")
    private String roleName;

    /**
     * 日志类型: button, uri
     */
    @Column(name = "element_type")
    private String elementType;

    /**
     * 日志类型: 按钮, 链接
     */
    @Column(name = "element_title")
    private String elementTitle;

    /**
     * 功能标题: 用户管理, 菜单管理等
     */
    @Column(name = "oplog_func")
    private String oplogFunc;

    /**
     * 操作类型: SAVE,UPDATE,JUMP,REMOVE,DELETE
     */
    @Column(name = "oplog_type")
    private String oplogType;

    /**
     * 操作类型: 新增, 修改, 删除, 查看
     */
    @Column(name = "oplog_title")
    private String oplogTitle;

    /**
     * 操作IP地址
     */
    @Column(name = "host_ip")
    private String hostIp;

    /**
     * 操作地址
     */
    @Column(name = "host_address")
    private String hostAddress;

    /**
     * 操作系统
     */
    @Column(name = "host_os")
    private String hostOs;

    /**
     * 操作浏览器
     */
    @Column(name = "host_browser")
    private String hostBrowser;

    /**
     * 用户代理
     */
    @Column(name = "user_agent")
    private String userAgent;

    /**
     * 请求ID
     */
    @Column(name = "request_id")
    private String requestId;

    /**
     * 请求URI
     */
    @Column(name = "request_uri")
    private String requestUri;

    /**
     * 请求方式: POST, PUT, GET, DELETE
     */
    @Column(name = "request_method")
    private String requestMethod;

    /**
     * 请求参数
     */
    @Column(name = "request_body")
    private String requestBody;

    /**
     * 请求时间
     */
    @Column(name = "request_time")
    private LocalDateTime requestTime;

    /**
     * 相应数据
     */
    @Column(name = "response_body")
    private String responseBody;

    /**
     * 相应状态码
     */
    @Column(name = "response_code")
    private String responseCode;

    /**
     * 返回消息
     */
    @Column(name = "response_msg")
    private String responseMsg;

    /**
     * 操作状态: 成功, 失败, 异常
     */
    @Column(name = "response_status")
    private String responseStatus;

    /**
     * 响应时间
     */
    @Column(name = "response_time")
    private LocalDateTime responseTime;

    /**
     * 异常信息
     */
    @Column(name = "ex_info")
    private String exInfo;

    /**
     * 服务端处理耗时
     */
    @Column(name = "execute_time")
    private String executeTime;

    /**
     * 备注
     */
    @Column(name = "remarks")
    private String remarks;

    /**
     * 删除标记:N-未删除;Y-已删除
     */
    @Column(name = "delete_flag")
    private String deleteFlag;

    /**
     * 年份
     */
    @Column(name = "yearly")
    private String yearly;

    /**
     * 月份
     */
    @Column(name = "monthly")
    private String monthly;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private LocalDateTime createTime;

    /**
     * 创建人
     */
    @Column(name = "create_user")
    private String createUser;

    /**
     * 创建人id
     */
    @Column(name = "create_user_id")
    private String createUserId;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private LocalDateTime updateTime;

    /**
     * 更新人
     */
    @Column(name = "update_user")
    private String updateUser;

    /**
     * 更新人id
     */
    @Column(name = "update_user_id")
    private String updateUserId;


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setPlatId(String platId) {
        this.platId = platId;
    }

    public String getPlatId() {
        return platId;
    }

    public void setPlatTitle(String platTitle) {
        this.platTitle = platTitle;
    }

    public String getPlatTitle() {
        return platTitle;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantTitle(String tenantTitle) {
        this.tenantTitle = tenantTitle;
    }

    public String getTenantTitle() {
        return tenantTitle;
    }

    public void setProvId(String provId) {
        this.provId = provId;
    }

    public String getProvId() {
        return provId;
    }

    public void setProvName(String provName) {
        this.provName = provName;
    }

    public String getProvName() {
        return provName;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCountyId(String countyId) {
        this.countyId = countyId;
    }

    public String getCountyId() {
        return countyId;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptTitle(String deptTitle) {
        this.deptTitle = deptTitle;
    }

    public String getDeptTitle() {
        return deptTitle;
    }

    public void setDeptLevel(String deptLevel) {
        this.deptLevel = deptLevel;
    }

    public String getDeptLevel() {
        return deptLevel;
    }

    public void setDeptParentId(String deptParentId) {
        this.deptParentId = deptParentId;
    }

    public String getDeptParentId() {
        return deptParentId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setElementType(String elementType) {
        this.elementType = elementType;
    }

    public String getElementType() {
        return elementType;
    }

    public void setElementTitle(String elementTitle) {
        this.elementTitle = elementTitle;
    }

    public String getElementTitle() {
        return elementTitle;
    }

    public void setOplogFunc(String oplogFunc) {
        this.oplogFunc = oplogFunc;
    }

    public String getOplogFunc() {
        return oplogFunc;
    }

    public void setOplogType(String oplogType) {
        this.oplogType = oplogType;
    }

    public String getOplogType() {
        return oplogType;
    }

    public void setOplogTitle(String oplogTitle) {
        this.oplogTitle = oplogTitle;
    }

    public String getOplogTitle() {
        return oplogTitle;
    }

    public void setHostIp(String hostIp) {
        this.hostIp = hostIp;
    }

    public String getHostIp() {
        return hostIp;
    }

    public void setHostAddress(String hostAddress) {
        this.hostAddress = hostAddress;
    }

    public String getHostAddress() {
        return hostAddress;
    }

    public void setHostOs(String hostOs) {
        this.hostOs = hostOs;
    }

    public String getHostOs() {
        return hostOs;
    }

    public void setHostBrowser(String hostBrowser) {
        this.hostBrowser = hostBrowser;
    }

    public String getHostBrowser() {
        return hostBrowser;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri;
    }

    public String getRequestUri() {
        return requestUri;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setRequestTime(LocalDateTime requestTime) {
        this.requestTime = requestTime;
    }

    public LocalDateTime getRequestTime() {
        return requestTime;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseTime(LocalDateTime responseTime) {
        this.responseTime = responseTime;
    }

    public LocalDateTime getResponseTime() {
        return responseTime;
    }

    public void setExInfo(String exInfo) {
        this.exInfo = exInfo;
    }

    public String getExInfo() {
        return exInfo;
    }

    public void setExecuteTime(String executeTime) {
        this.executeTime = executeTime;
    }

    public String getExecuteTime() {
        return executeTime;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setYearly(String yearly) {
        this.yearly = yearly;
    }

    public String getYearly() {
        return yearly;
    }

    public void setMonthly(String monthly) {
        this.monthly = monthly;
    }

    public String getMonthly() {
        return monthly;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public String getUpdateUserId() {
        return updateUserId;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("platId", getPlatId())
                .append("platTitle", getPlatTitle())
                .append("appId", getAppId())
                .append("appName", getAppName())
                .append("appVersion", getAppVersion())
                .append("tenantId", getTenantId())
                .append("tenantTitle", getTenantTitle())
                .append("provId", getProvId())
                .append("provName", getProvName())
                .append("cityId", getCityId())
                .append("cityName", getCityName())
                .append("countyId", getCountyId())
                .append("countyName", getCountyName())
                .append("deptId", getDeptId())
                .append("deptTitle", getDeptTitle())
                .append("deptLevel", getDeptLevel())
                .append("deptParentId", getDeptParentId())
                .append("userId", getUserId())
                .append("nickName", getNickName())
                .append("roleId", getRoleId())
                .append("roleName", getRoleName())
                .append("elementType", getElementType())
                .append("elementTitle", getElementTitle())
                .append("oplogFunc", getOplogFunc())
                .append("oplogType", getOplogType())
                .append("oplogTitle", getOplogTitle())
                .append("hostIp", getHostIp())
                .append("hostAddress", getHostAddress())
                .append("hostOs", getHostOs())
                .append("hostBrowser", getHostBrowser())
                .append("userAgent", getUserAgent())
                .append("requestId", getRequestId())
                .append("requestUri", getRequestUri())
                .append("requestMethod", getRequestMethod())
                .append("requestTime", getRequestTime())
                .append("requestBody", getRequestBody())
                .append("responseBody", getResponseBody())
                .append("responseCode", getResponseCode())
                .append("responseMsg", getResponseMsg())
                .append("responseStatus", getResponseStatus())
                .append("responseTime", getResponseTime())
                .append("exInfo", getExInfo())
                .append("executeTime", getExecuteTime())
                .append("remarks", getRemarks())
                .append("deleteFlag", getDeleteFlag())
                .append("yearly", getYearly())
                .append("monthly", getMonthly())
                .append("createTime", getCreateTime())
                .append("createUser", getCreateUser())
                .append("createUserId", getCreateUserId())
                .append("updateTime", getUpdateTime())
                .append("updateUser", getUpdateUser())
                .append("updateUserId", getUpdateUserId())
                .toString();
    }

}
