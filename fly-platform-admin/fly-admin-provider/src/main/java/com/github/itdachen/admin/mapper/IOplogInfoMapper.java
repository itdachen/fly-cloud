package com.github.itdachen.admin.mapper;

import com.github.itdachen.admin.entity.OplogInfo;
import com.github.itdachen.admin.sdk.query.OplogInfoQuery;
import com.github.itdachen.admin.sdk.vo.OplogInfoVO;
import tk.mybatis.mapper.common.Mapper;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * IOplogInfoMapper
 *
 * @author 王大宸
 * @date 2025-02-08 15:45
 */
public interface IOplogInfoMapper extends Mapper<OplogInfo> {

    /***
     * 查询集合
     *
     * @author 王大宸
     * @date 2025-02-08 15:41:58
     * @param params params
     * @return com.github.itdachen.admin.sdk.vo.oplogInfoVO
     */
    List<OplogInfoVO> list(OplogInfoQuery params);

    /***
     * 根据id查询
     *
     * @author 王大宸
     * @date 2025-02-08 15:41:58
     * @param id 需要查询的数据id
     * @return com.github.itdachen.admin.sdk.vo.oplogInfoVo
     */
    OplogInfoVO selectOplogInfoVO(String id);

    /***
     * 查询导出数据
     *
     * @author 王大宸
     * @date 2025-02-08 15:41:58
     * @param params 查询参数
     * @return java.util.LinkedHashMap
     */
    List<LinkedHashMap<String, String>> selectOplogInfoExpData(OplogInfoQuery params);


}
