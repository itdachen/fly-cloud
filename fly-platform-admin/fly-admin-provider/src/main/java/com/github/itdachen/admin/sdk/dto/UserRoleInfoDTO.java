package com.github.itdachen.admin.sdk.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


import java.io.Serializable;

/**
 * 身份信息 DTO
 *
 * @author 王大宸
 * @date 2025-01-11 16:32:42
 */
public class UserRoleInfoDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 身份ID
     */
    private String id;

    /**
     * 租户ID
     */
    private String tenantId;

    /**
     * 用户ID
     */
    @NotBlank(message = "用户ID不能为空")
    private String userId;


    /**
     * 登录账号/人员代码
     */
    // @NotBlank(message = "登录账号/人员代码不能为空")
    private String username;

    /**
     * 昵称
     */
    @NotBlank(message = "昵称不能为空")
    private String nickName;

    /**
     * 身份名称
     */
    @NotBlank(message = "身份名称不能为空")
    private String roleName;

    /**
     * 主身份标志: Y/N
     */
    @NotBlank(message = "主身份标志: Y/N不能为空")
    private String mainFlag;

    /**
     * 有效标志: Y/N
     */
    @NotBlank(message = "有效标志: Y/N不能为空")
    private String validFlag;

    /**
     * 身份部门ID
     */
    @NotBlank(message = "部门代码不能为空")
    private String deptCode;

    /**
     * 身份部门名称
     */
    @NotBlank(message = "身份部门名称不能为空")
    private String deptTitle;

    /**
     * 身份有效期起
     */
    @NotBlank(message = "身份有效期起不能为空")
    private String startTime;

    /**
     * 身份有效期止
     */
    @NotBlank(message = "身份有效期止不能为空")
    private String endTime;

    /**
     * 排序
     */
    @NotBlank(message = "排序不能为空")
    private String orderNum;

    /**
     * 备注
     */
    private String remarks;


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setMainFlag(String mainFlag) {
        this.mainFlag = mainFlag;
    }

    public String getMainFlag() {
        return mainFlag;
    }

    public void setValidFlag(String validFlag) {
        this.validFlag = validFlag;
    }

    public String getValidFlag() {
        return validFlag;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public void setDeptTitle(String deptTitle) {
        this.deptTitle = deptTitle;
    }

    public String getDeptTitle() {
        return deptTitle;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRemarks() {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("tenantId", getTenantId())
                .append("userId", getUserId())
                .append("username", getUsername())
                .append("nickName", getNickName())
                .append("roleName", getRoleName())
                .append("mainFlag", getMainFlag())
                .append("validFlag", getValidFlag())
                .append("deptCode", getDeptCode())
                .append("deptTitle", getDeptTitle())
                .append("startTime", getStartTime())
                .append("endTime", getEndTime())
                .append("orderNum", getOrderNum())
                .append("remarks", getRemarks())
                .toString();
    }

}
