package com.github.itdachen.admin.sdk.query;

import com.github.itdachen.framework.core.biz.BizQuery;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * OplogInfoQuery
 *
 * @author 王大宸
 * @date 2025-02-08 15:48
 */
public class OplogInfoQuery  extends BizQuery implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 平台ID */
    private String platId;

    /** 应用ID */
    private String appId;

    /** 租户ID/公司ID */
    private String tenantId;

    /** 省代码 */
    private String provId;

    /** 省名称 */
    private String provName;

    /** 市/州代码 */
    private String cityId;

    /** 市/州名称 */
    private String cityName;

    /** 区/县代码 */
    private String countyId;

    /** 区/县名称 */
    private String countyName;

    /** 部门ID */
    private String deptId;

    /** 部门名称 */
    private String deptTitle;

    /** 部门等级 */
    private String deptLevel;

    /** 上级部门代码 */
    private String deptParentId;

    /** 用户ID/用户代码 */
    private String userId;

    /** 昵称 */
    private String nickName;

    /** 功能标题: 用户管理, 菜单管理等 */
    private String oplogFunc;

    /** 操作类型: SAVE,UPDATE,JUMP,REMOVE,DELETE */
    private String oplogType;

    /** 操作类型: 新增, 修改, 删除, 查看 */
    private String oplogTitle;

    /** 相应状态码 */
    private String responseCode;

    /** 操作状态: 成功, 失败, 异常 */
    private String responseStatus;

    /** 删除标记:N-未删除;Y-已删除 */
    private String deleteFlag;

    /** 年份 */
    private String yearly;

    /** 月份 */
    private String monthly;



    public OplogInfoQuery() {
    }

    public OplogInfoQuery(int page, int limit, String platId, String appId, String tenantId, String provId, String provName, String cityId, String cityName, String countyId, String countyName, String deptId, String deptTitle, String deptLevel, String deptParentId, String userId, String nickName, String oplogFunc, String oplogType, String oplogTitle, String responseCode, String responseStatus, String deleteFlag, String yearly, String monthly) {
        super(page, limit);
        this.platId = platId;
        this.appId = appId;
        this.tenantId = tenantId;
        this.provId = provId;
        this.provName = provName;
        this.cityId = cityId;
        this.cityName = cityName;
        this.countyId = countyId;
        this.countyName = countyName;
        this.deptId = deptId;
        this.deptTitle = deptTitle;
        this.deptLevel = deptLevel;
        this.deptParentId = deptParentId;
        this.userId = userId;
        this.nickName = nickName;
        this.oplogFunc = oplogFunc;
        this.oplogType = oplogType;
        this.oplogTitle = oplogTitle;
        this.responseCode = responseCode;
        this.responseStatus = responseStatus;
        this.deleteFlag = deleteFlag;
        this.yearly = yearly;
        this.monthly = monthly;
    }

    public static OplogInfoQueryBuilder builder() {
        return new OplogInfoQueryBuilder();
    }

    public static class OplogInfoQueryBuilder {
        private Integer page = 1;
        private Integer limit = 10;
        private String platId;
        private String appId;
        private String tenantId;
        private String provId;
        private String provName;
        private String cityId;
        private String cityName;
        private String countyId;
        private String countyName;
        private String deptId;
        private String deptTitle;
        private String deptLevel;
        private String deptParentId;
        private String userId;
        private String nickName;
        private String oplogFunc;
        private String oplogType;
        private String oplogTitle;
        private String responseCode;
        private String responseStatus;
        private String deleteFlag;
        private String yearly;
        private String monthly;

        public OplogInfoQueryBuilder() {
        }

        public OplogInfoQueryBuilder page(Integer page) {
            this.page = page;
            return this;
        }

        public OplogInfoQueryBuilder limit(Integer limit) {
            this.limit = limit;
            return this;
        }

        /* 平台ID */
        public OplogInfoQueryBuilder platId(String platId) {
            this.platId = platId;
            return this;
        }
        /* 应用ID */
        public OplogInfoQueryBuilder appId(String appId) {
            this.appId = appId;
            return this;
        }
        /* 租户ID/公司ID */
        public OplogInfoQueryBuilder tenantId(String tenantId) {
            this.tenantId = tenantId;
            return this;
        }
        /* 省代码 */
        public OplogInfoQueryBuilder provId(String provId) {
            this.provId = provId;
            return this;
        }
        /* 省名称 */
        public OplogInfoQueryBuilder provName(String provName) {
            this.provName = provName;
            return this;
        }
        /* 市/州代码 */
        public OplogInfoQueryBuilder cityId(String cityId) {
            this.cityId = cityId;
            return this;
        }
        /* 市/州名称 */
        public OplogInfoQueryBuilder cityName(String cityName) {
            this.cityName = cityName;
            return this;
        }
        /* 区/县代码 */
        public OplogInfoQueryBuilder countyId(String countyId) {
            this.countyId = countyId;
            return this;
        }
        /* 区/县名称 */
        public OplogInfoQueryBuilder countyName(String countyName) {
            this.countyName = countyName;
            return this;
        }
        /* 部门ID */
        public OplogInfoQueryBuilder deptId(String deptId) {
            this.deptId = deptId;
            return this;
        }
        /* 部门名称 */
        public OplogInfoQueryBuilder deptTitle(String deptTitle) {
            this.deptTitle = deptTitle;
            return this;
        }
        /* 部门等级 */
        public OplogInfoQueryBuilder deptLevel(String deptLevel) {
            this.deptLevel = deptLevel;
            return this;
        }
        /* 上级部门代码 */
        public OplogInfoQueryBuilder deptParentId(String deptParentId) {
            this.deptParentId = deptParentId;
            return this;
        }
        /* 用户ID/用户代码 */
        public OplogInfoQueryBuilder userId(String userId) {
            this.userId = userId;
            return this;
        }
        /* 昵称 */
        public OplogInfoQueryBuilder nickName(String nickName) {
            this.nickName = nickName;
            return this;
        }
        /* 功能标题: 用户管理, 菜单管理等 */
        public OplogInfoQueryBuilder oplogFunc(String oplogFunc) {
            this.oplogFunc = oplogFunc;
            return this;
        }
        /* 操作类型: SAVE,UPDATE,JUMP,REMOVE,DELETE */
        public OplogInfoQueryBuilder oplogType(String oplogType) {
            this.oplogType = oplogType;
            return this;
        }
        /* 操作类型: 新增, 修改, 删除, 查看 */
        public OplogInfoQueryBuilder oplogTitle(String oplogTitle) {
            this.oplogTitle = oplogTitle;
            return this;
        }
        /* 相应状态码 */
        public OplogInfoQueryBuilder responseCode(String responseCode) {
            this.responseCode = responseCode;
            return this;
        }
        /* 操作状态: 成功, 失败, 异常 */
        public OplogInfoQueryBuilder responseStatus(String responseStatus) {
            this.responseStatus = responseStatus;
            return this;
        }
        /* 删除标记:N-未删除;Y-已删除 */
        public OplogInfoQueryBuilder deleteFlag(String deleteFlag) {
            this.deleteFlag = deleteFlag;
            return this;
        }
        /* 年份 */
        public OplogInfoQueryBuilder yearly(String yearly) {
            this.yearly = yearly;
            return this;
        }
        /* 月份 */
        public OplogInfoQueryBuilder monthly(String monthly) {
            this.monthly = monthly;
            return this;
        }

        public OplogInfoQuery build() {
            return new OplogInfoQuery(page, limit, platId, appId, tenantId, provId, provName, cityId, cityName, countyId, countyName, deptId, deptTitle, deptLevel, deptParentId, userId, nickName, oplogFunc, oplogType, oplogTitle, responseCode, responseStatus, deleteFlag, yearly, monthly);
        }

    }



    public void setPlatId(String platId) {
        this.platId = platId;
    }

    public String getPlatId() {
        return platId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppId() {
        return appId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setProvId(String provId) {
        this.provId = provId;
    }

    public String getProvId() {
        return provId;
    }

    public void setProvName(String provName) {
        this.provName = provName;
    }

    public String getProvName() {
        return provName;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCountyId(String countyId) {
        this.countyId = countyId;
    }

    public String getCountyId() {
        return countyId;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptTitle(String deptTitle) {
        this.deptTitle = deptTitle;
    }

    public String getDeptTitle() {
        return deptTitle;
    }

    public void setDeptLevel(String deptLevel) {
        this.deptLevel = deptLevel;
    }

    public String getDeptLevel() {
        return deptLevel;
    }

    public void setDeptParentId(String deptParentId) {
        this.deptParentId = deptParentId;
    }

    public String getDeptParentId() {
        return deptParentId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setOplogFunc(String oplogFunc) {
        this.oplogFunc = oplogFunc;
    }

    public String getOplogFunc() {
        return oplogFunc;
    }

    public void setOplogType(String oplogType) {
        this.oplogType = oplogType;
    }

    public String getOplogType() {
        return oplogType;
    }

    public void setOplogTitle(String oplogTitle) {
        this.oplogTitle = oplogTitle;
    }

    public String getOplogTitle() {
        return oplogTitle;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setYearly(String yearly) {
        this.yearly = yearly;
    }

    public String getYearly() {
        return yearly;
    }

    public void setMonthly(String monthly) {
        this.monthly = monthly;
    }

    public String getMonthly() {
        return monthly;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("page", getPage())
                .append("limit", getLimit())
                .append("platId", getPlatId())
                .append("appId", getAppId())
                .append("tenantId", getTenantId())
                .append("provId", getProvId())
                .append("provName", getProvName())
                .append("cityId", getCityId())
                .append("cityName", getCityName())
                .append("countyId", getCountyId())
                .append("countyName", getCountyName())
                .append("deptId", getDeptId())
                .append("deptTitle", getDeptTitle())
                .append("deptLevel", getDeptLevel())
                .append("deptParentId", getDeptParentId())
                .append("userId", getUserId())
                .append("nickName", getNickName())
                .append("oplogFunc", getOplogFunc())
                .append("oplogType", getOplogType())
                .append("oplogTitle", getOplogTitle())
                .append("responseCode", getResponseCode())
                .append("responseStatus", getResponseStatus())
                .append("deleteFlag", getDeleteFlag())
                .append("yearly", getYearly())
                .append("monthly", getMonthly())
                .toString();
    }

}
