package com.github.itdachen.admin.sdk.query;

import com.github.itdachen.framework.core.biz.BizQuery;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


import java.io.Serializable;


/**
 * 基础用户信息 查询参数
 *
 * @author 王大宸
 * @date 2025-01-11 16:32:42
 */
public class UserInfoQuery extends BizQuery implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 租户ID
     */
    private String tenantId;

    /**
     * 租户名称
     */
    private String tenantTitle;

    /**
     * 登录账号/人员代码
     */
    private String username;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 性别
     */
    private String sex;

    /**
     * 电话号码
     */
    private String telephone;

    /**
     * 用户类型
     */
    private String userType;

    /**
     * 有效标志: Y-是;N-否; 与 ta_fly_login_info 表保持一致
     */
    private String validFlag;

    /**
     * 是否注销: Y-是;N-否;
     */
    private String logOff;

    /**
     * 删除标志: Y-是;N-否;
     */
    private String delFlag;


    public UserInfoQuery() {
    }

    public UserInfoQuery(int page, int limit, String tenantId, String tenantTitle, String username, String nickName, String sex, String telephone, String userType, String validFlag, String logOff, String delFlag) {
        super(page, limit);
        this.tenantId = tenantId;
        this.tenantTitle = tenantTitle;
        this.username = username;
        this.nickName = nickName;
        this.sex = sex;
        this.telephone = telephone;
        this.userType = userType;
        this.validFlag = validFlag;
        this.logOff = logOff;
        this.delFlag = delFlag;
    }

    public static UserInfoQueryBuilder builder() {
        return new UserInfoQueryBuilder();
    }

    public static class UserInfoQueryBuilder {
        private Integer page = 1;
        private Integer limit = 10;
        private String tenantId;
        private String tenantTitle;
        private String username;
        private String nickName;
        private String sex;
        private String telephone;
        private String userType;
        private String validFlag;
        private String logOff;
        private String delFlag;

        public UserInfoQueryBuilder() {
        }

        public UserInfoQueryBuilder page(Integer page) {
            this.page = page;
            return this;
        }

        public UserInfoQueryBuilder limit(Integer limit) {
            this.limit = limit;
            return this;
        }

        /* 租户ID */
        public UserInfoQueryBuilder tenantId(String tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        /* 租户名称 */
        public UserInfoQueryBuilder tenantTitle(String tenantTitle) {
            this.tenantTitle = tenantTitle;
            return this;
        }

        /* 登录账号/人员代码 */
        public UserInfoQueryBuilder username(String username) {
            this.username = username;
            return this;
        }

        /* 昵称 */
        public UserInfoQueryBuilder nickName(String nickName) {
            this.nickName = nickName;
            return this;
        }

        /* 性别 */
        public UserInfoQueryBuilder sex(String sex) {
            this.sex = sex;
            return this;
        }

        /* 电话号码 */
        public UserInfoQueryBuilder telephone(String telephone) {
            this.telephone = telephone;
            return this;
        }

        /* 用户类型 */
        public UserInfoQueryBuilder userType(String userType) {
            this.userType = userType;
            return this;
        }

        /* 有效标志: Y-是;N-否; 与 ta_fly_login_info 表保持一致 */
        public UserInfoQueryBuilder validFlag(String validFlag) {
            this.validFlag = validFlag;
            return this;
        }

        /* 是否注销: Y-是;N-否;  */
        public UserInfoQueryBuilder logOff(String logOff) {
            this.logOff = logOff;
            return this;
        }

        /* 删除标志: Y-是;N-否;  */
        public UserInfoQueryBuilder delFlag(String delFlag) {
            this.delFlag = delFlag;
            return this;
        }

        public UserInfoQuery build() {
            return new UserInfoQuery(page, limit, tenantId, tenantTitle, username, nickName, sex, telephone, userType, validFlag, logOff, delFlag);
        }

    }


    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantTitle(String tenantTitle) {
        this.tenantTitle = tenantTitle;
    }

    public String getTenantTitle() {
        return tenantTitle;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSex() {
        return sex;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserType() {
        return userType;
    }

    public void setValidFlag(String validFlag) {
        this.validFlag = validFlag;
    }

    public String getValidFlag() {
        return validFlag;
    }

    public void setLogOff(String logOff) {
        this.logOff = logOff;
    }

    public String getLogOff() {
        return logOff;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getDelFlag() {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("page", getPage())
                .append("limit", getLimit())
                .append("tenantId", getTenantId())
                .append("tenantTitle", getTenantTitle())
                .append("username", getUsername())
                .append("nickName", getNickName())
                .append("sex", getSex())
                .append("telephone", getTelephone())
                .append("userType", getUserType())
                .append("validFlag", getValidFlag())
                .append("logOff", getLogOff())
                .append("delFlag", getDelFlag())
                .toString();
    }

}
