package com.github.itdachen.admin.sdk.query;

import com.github.itdachen.framework.core.biz.BizQuery;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


import java.io.Serializable;


/**
 * 身份信息 查询参数
 *
 * @author 王大宸
 * @date 2025-01-11 16:32:42
 */
public class UserRoleInfoQuery extends BizQuery implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 租户ID
     */
    private String tenantId;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 登录账号/人员代码
     */
    private String username;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 身份名称
     */
    private String roleName;

    /**
     * 主身份标志: Y/N
     */
    private String mainFlag;

    /**
     * 有效标志: Y/N
     */
    private String validFlag;

    /**
     * 身份部门ID
     */
    private String deptCode;

    /**
     * 身份部门名称
     */
    private String deptTitle;

    /**
     * 身份有效期起
     */
    private String startTime;

    /**
     * 身份有效期止
     */
    private String endTime;


    public UserRoleInfoQuery() {
    }

    public UserRoleInfoQuery(int page, int limit, String tenantId, String userId, String username, String nickName, String roleName, String mainFlag, String validFlag, String deptCode, String deptTitle, String startTime, String endTime) {
        super(page, limit);
        this.tenantId = tenantId;
        this.userId = userId;
        this.username = username;
        this.nickName = nickName;
        this.roleName = roleName;
        this.mainFlag = mainFlag;
        this.validFlag = validFlag;
        this.deptCode = deptCode;
        this.deptTitle = deptTitle;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public static UserRoleInfoQueryBuilder builder() {
        return new UserRoleInfoQueryBuilder();
    }

    public static class UserRoleInfoQueryBuilder {
        private Integer page = 1;
        private Integer limit = 10;
        private String tenantId;
        private String userId;
        private String username;
        private String nickName;
        private String roleName;
        private String mainFlag;
        private String validFlag;
        private String deptCode;
        private String deptTitle;
        private String startTime;
        private String endTime;

        public UserRoleInfoQueryBuilder() {
        }

        public UserRoleInfoQueryBuilder page(Integer page) {
            this.page = page;
            return this;
        }

        public UserRoleInfoQueryBuilder limit(Integer limit) {
            this.limit = limit;
            return this;
        }

        /* 租户ID */
        public UserRoleInfoQueryBuilder tenantId(String tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        /* 用户ID */
        public UserRoleInfoQueryBuilder userId(String userId) {
            this.userId = userId;
            return this;
        }

        /* 登录账号/人员代码 */
        public UserRoleInfoQueryBuilder username(String username) {
            this.username = username;
            return this;
        }

        /* 昵称 */
        public UserRoleInfoQueryBuilder nickName(String nickName) {
            this.nickName = nickName;
            return this;
        }

        /* 身份名称 */
        public UserRoleInfoQueryBuilder roleName(String roleName) {
            this.roleName = roleName;
            return this;
        }

        /* 主身份标志: Y/N */
        public UserRoleInfoQueryBuilder mainFlag(String mainFlag) {
            this.mainFlag = mainFlag;
            return this;
        }

        /* 有效标志: Y/N */
        public UserRoleInfoQueryBuilder validFlag(String validFlag) {
            this.validFlag = validFlag;
            return this;
        }

        /* 身份部门ID */
        public UserRoleInfoQueryBuilder deptCode(String deptCode) {
            this.deptCode = deptCode;
            return this;
        }

        /* 身份部门名称 */
        public UserRoleInfoQueryBuilder deptTitle(String deptTitle) {
            this.deptTitle = deptTitle;
            return this;
        }

        /* 身份有效期起 */
        public UserRoleInfoQueryBuilder startTime(String startTime) {
            this.startTime = startTime;
            return this;
        }

        /* 身份有效期止 */
        public UserRoleInfoQueryBuilder endTime(String endTime) {
            this.endTime = endTime;
            return this;
        }

        public UserRoleInfoQuery build() {
            return new UserRoleInfoQuery(page, limit, tenantId, userId, username, nickName, roleName, mainFlag, validFlag, deptCode, deptTitle, startTime, endTime);
        }

    }


    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setMainFlag(String mainFlag) {
        this.mainFlag = mainFlag;
    }

    public String getMainFlag() {
        return mainFlag;
    }

    public void setValidFlag(String validFlag) {
        this.validFlag = validFlag;
    }

    public String getValidFlag() {
        return validFlag;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public void setDeptTitle(String deptTitle) {
        this.deptTitle = deptTitle;
    }

    public String getDeptTitle() {
        return deptTitle;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getEndTime() {
        return endTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("page", getPage())
                .append("limit", getLimit())
                .append("tenantId", getTenantId())
                .append("userId", getUserId())
                .append("username", getUsername())
                .append("nickName", getNickName())
                .append("roleName", getRoleName())
                .append("mainFlag", getMainFlag())
                .append("validFlag", getValidFlag())
                .append("deptCode", getDeptCode())
                .append("deptTitle", getDeptTitle())
                .append("startTime", getStartTime())
                .append("endTime", getEndTime())
                .toString();
    }

}
