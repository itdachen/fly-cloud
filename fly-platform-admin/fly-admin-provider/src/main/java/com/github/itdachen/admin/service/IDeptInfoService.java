package com.github.itdachen.admin.service;

import com.github.itdachen.admin.sdk.dto.DeptInfoDTO;
import com.github.itdachen.admin.sdk.query.DeptInfoQuery;
import com.github.itdachen.admin.sdk.vo.ClazzInfoVO;
import com.github.itdachen.admin.sdk.vo.DeptInfoVO;
import com.github.itdachen.framework.context.tree.lay.LayTree;
import com.github.itdachen.framework.core.response.TableData;
import com.github.itdachen.framework.webmvc.service.IBizService;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * 部门信息 业务接口
 *
 * @author 王大宸
 * @date 2024-12-22 22:30:09
 */
public interface IDeptInfoService extends IBizService<DeptInfoDTO, DeptInfoVO, DeptInfoQuery, String> {


    LayTree findDeptTree(String deptFlag) throws Exception;

    /***
     * 部门岗位设置时, 获取岗位
     *
     * @author 王大宸
     * @date 2025/1/2 19:58
     * @param  deptId 部门ID
     * @return java.util.List<com.github.itdachen.admin.sdk.vo.ClazzInfoVO>
     */


    /***
     * 部门岗位设置时, 获取岗位
     *
     * @author 王大宸
     * @date 2025/1/2 20:05
     * @param deptId 部门ID
     * @param title  岗位标题
     * @param page   页数
     * @param limit  每页条数
     * @return com.github.itdachen.framework.core.response.TableData<com.github.itdachen.admin.sdk.vo.ClazzInfoVO>
     */
    TableData<ClazzInfoVO> findClazzList(String deptId, String title, Integer page, Integer limit) throws Exception;


}
