package com.github.itdachen.admin.service;

import com.github.itdachen.admin.sdk.dto.UserRoleInfoDTO;
import com.github.itdachen.admin.sdk.query.UserRoleInfoQuery;
import com.github.itdachen.admin.sdk.vo.UserRoleInfoVO;
import com.github.itdachen.framework.webmvc.service.IBizService;

/**
 * 身份信息 业务接口
 *
 * @author 王大宸
 * @date 2025-01-11 16:32:42
 */
public interface IUserRoleInfoService extends IBizService< UserRoleInfoDTO, UserRoleInfoVO, UserRoleInfoQuery, String > {

}
