package com.github.itdachen.admin.service.impl;

import com.github.itdachen.framework.core.response.TableData;
import com.github.itdachen.framework.webmvc.service.impl.BizServiceImpl;
import com.github.itdachen.framework.webmvc.poi.WorkBookUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.itdachen.admin.entity.DictData;
import com.github.itdachen.admin.sdk.dto.DictDataDTO;
import com.github.itdachen.admin.sdk.query.DictDataQuery;
import com.github.itdachen.admin.sdk.vo.DictDataVO;
import com.github.itdachen.admin.mapper.IDictDataMapper;
import com.github.itdachen.admin.service.IDictDataService;
import com.github.itdachen.admin.convert.DictDataConvert;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 数据字典信息表 业务实现
 *
 * @author 王大宸
 * @date 2025-01-15 20:38:26
 */
@Service
public class DictDataServiceImpl extends BizServiceImpl< IDictDataMapper, DictData, DictDataDTO,  DictDataVO, DictDataQuery, String > implements IDictDataService {
    private static final Logger logger = LoggerFactory.getLogger(DictDataServiceImpl.class);
    private static final DictDataConvert bizConvert = new DictDataConvert();
    private final List<String> EXP_FIELDS = new ArrayList<>();
    public DictDataServiceImpl() {
        super(bizConvert);
            EXP_FIELDS.add("数据字典类型ID");
            EXP_FIELDS.add("数据字典类型名称");
            EXP_FIELDS.add("字典类型, 例如:Y_OR_N");
            EXP_FIELDS.add("字典标签");
            EXP_FIELDS.add("字典键值");
            EXP_FIELDS.add("是否默认（1是 0否）");
            EXP_FIELDS.add("字典排序");
            EXP_FIELDS.add("是否展示/有效标志: Y-有效;N-无效");
    }

    /***
    * 分页
    *
    * @author 王大宸
    * @date 2025-01-15 20:38:26
    * @param params params
    * @return com.github.itdachen.framework.core.response.TableData<com.github.itdachen.admin.sdk.vo.dictDataVo>
    */
    @Override
    public TableData< DictDataVO > page(DictDataQuery params) throws Exception {
        Page< DictDataVO > page = PageHelper.startPage(params.getPage(), params.getLimit());
        List< DictDataVO > list = bizMapper.list(params);
        return new TableData< DictDataVO >(page.getTotal(), list);
    }


    /***
     * 导出
     *
     * @author 王大宸
     * @date 2025-01-15 20:38:26
     * @param params com.github.itdachen.admin.sdk.query.DictDataQuery
     * @return void
     */
    @Override
    public void dataExpToExcel(HttpServletRequest request,
                               HttpServletResponse response,
                               DictDataQuery params) throws Exception{
        List < LinkedHashMap < String, String > >  list = bizMapper.selectDictDataExpData(params);
       WorkBookUtils.export(request, response)
                .params(params)
                .title("数据字典信息表")
                .rowNum(true)
                .fields(EXP_FIELDS)
                .data(list)
                .execute();
    }

}
