package com.github.itdachen.admin.service.impl;

import com.github.itdachen.framework.core.response.TableData;
import com.github.itdachen.framework.webmvc.service.impl.BizServiceImpl;
import com.github.itdachen.framework.webmvc.poi.WorkBookUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.itdachen.admin.entity.DictType;
import com.github.itdachen.admin.sdk.dto.DictTypeDTO;
import com.github.itdachen.admin.sdk.query.DictTypeQuery;
import com.github.itdachen.admin.sdk.vo.DictTypeVO;
import com.github.itdachen.admin.mapper.IDictTypeMapper;
import com.github.itdachen.admin.service.IDictTypeService;
import com.github.itdachen.admin.convert.DictTypeConvert;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 数据字典类型 业务实现
 *
 * @author 王大宸
 * @date 2025-01-15 20:38:26
 */
@Service
public class DictTypeServiceImpl extends BizServiceImpl< IDictTypeMapper, DictType, DictTypeDTO,  DictTypeVO, DictTypeQuery, String > implements IDictTypeService {
    private static final Logger logger = LoggerFactory.getLogger(DictTypeServiceImpl.class);
    private static final DictTypeConvert bizConvert = new DictTypeConvert();
    private final List<String> EXP_FIELDS = new ArrayList<>();
    public DictTypeServiceImpl() {
        super(bizConvert);
            EXP_FIELDS.add("字典标签");
            EXP_FIELDS.add("字典类型");
            EXP_FIELDS.add("是否默认（1是 0否）");
            EXP_FIELDS.add("字典排序");
            EXP_FIELDS.add("是否展示/有效标志: Y-有效;N-无效");
    }

    /***
    * 分页
    *
    * @author 王大宸
    * @date 2025-01-15 20:38:26
    * @param params params
    * @return com.github.itdachen.framework.core.response.TableData<com.github.itdachen.admin.sdk.vo.dictTypeVo>
    */
    @Override
    public TableData< DictTypeVO > page(DictTypeQuery params) throws Exception {
        Page< DictTypeVO > page = PageHelper.startPage(params.getPage(), params.getLimit());
        List< DictTypeVO > list = bizMapper.list(params);
        return new TableData< DictTypeVO >(page.getTotal(), list);
    }


    /***
     * 导出
     *
     * @author 王大宸
     * @date 2025-01-15 20:38:26
     * @param params com.github.itdachen.admin.sdk.query.DictTypeQuery
     * @return void
     */
    @Override
    public void dataExpToExcel(HttpServletRequest request,
                               HttpServletResponse response,
                               DictTypeQuery params) throws Exception{
        List < LinkedHashMap < String, String > >  list = bizMapper.selectDictTypeExpData(params);
       WorkBookUtils.export(request, response)
                .params(params)
                .title("数据字典类型")
                .rowNum(true)
                .fields(EXP_FIELDS)
                .data(list)
                .execute();
    }

}
