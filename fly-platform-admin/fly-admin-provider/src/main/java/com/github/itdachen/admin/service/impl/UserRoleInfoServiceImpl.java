package com.github.itdachen.admin.service.impl;

import com.github.itdachen.framework.core.response.TableData;
import com.github.itdachen.framework.webmvc.service.impl.BizServiceImpl;
import com.github.itdachen.framework.webmvc.poi.WorkBookUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.itdachen.admin.entity.UserRoleInfo;
import com.github.itdachen.admin.sdk.dto.UserRoleInfoDTO;
import com.github.itdachen.admin.sdk.query.UserRoleInfoQuery;
import com.github.itdachen.admin.sdk.vo.UserRoleInfoVO;
import com.github.itdachen.admin.mapper.IUserRoleInfoMapper;
import com.github.itdachen.admin.service.IUserRoleInfoService;
import com.github.itdachen.admin.convert.UserRoleInfoConvert;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 身份信息 业务实现
 *
 * @author 王大宸
 * @date 2025-01-11 16:32:42
 */
@Service
public class UserRoleInfoServiceImpl extends BizServiceImpl< IUserRoleInfoMapper, UserRoleInfo, UserRoleInfoDTO,  UserRoleInfoVO, UserRoleInfoQuery, String > implements IUserRoleInfoService {
    private static final Logger logger = LoggerFactory.getLogger(UserRoleInfoServiceImpl.class);
    private static final UserRoleInfoConvert bizConvert = new UserRoleInfoConvert();
    private final List<String> EXP_FIELDS = new ArrayList<>();
    public UserRoleInfoServiceImpl() {
        super(bizConvert);
            EXP_FIELDS.add("用户ID");
            EXP_FIELDS.add("昵称");
            EXP_FIELDS.add("身份名称");
            EXP_FIELDS.add("主身份标志: Y/N");
            EXP_FIELDS.add("有效标志: Y/N");
            EXP_FIELDS.add("身份部门ID");
            EXP_FIELDS.add("身份部门名称");
            EXP_FIELDS.add("身份有效期起");
            EXP_FIELDS.add("身份有效期止");
            EXP_FIELDS.add("排序");
    }

    /***
    * 分页
    *
    * @author 王大宸
    * @date 2025-01-11 16:32:42
    * @param params params
    * @return com.github.itdachen.framework.core.response.TableData<com.github.itdachen.admin.sdk.vo.userRoleInfoVo>
    */
    @Override
    public TableData< UserRoleInfoVO > page(UserRoleInfoQuery params) throws Exception {
        Page< UserRoleInfoVO > page = PageHelper.startPage(params.getPage(), params.getLimit());
        List< UserRoleInfoVO > list = bizMapper.list(params);
        return new TableData< UserRoleInfoVO >(page.getTotal(), list);
    }


    /***
     * 导出
     *
     * @author 王大宸
     * @date 2025-01-11 16:32:42
     * @param params com.github.itdachen.admin.sdk.query.UserRoleInfoQuery
     * @return void
     */
    @Override
    public void dataExpToExcel(HttpServletRequest request,
                               HttpServletResponse response,
                               UserRoleInfoQuery params) throws Exception{
        List < LinkedHashMap < String, String > >  list = bizMapper.selectUserRoleInfoExpData(params);
       WorkBookUtils.export(request, response)
                .params(params)
                .title("身份信息")
                .rowNum(true)
                .fields(EXP_FIELDS)
                .data(list)
                .execute();
    }

}
