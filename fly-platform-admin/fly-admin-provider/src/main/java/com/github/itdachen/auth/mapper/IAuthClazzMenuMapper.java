package com.github.itdachen.auth.mapper;

import com.github.itdachen.auth.entity.AuthClazzMenu;
import com.github.itdachen.auth.sdk.query.AuthClazzMenuQuery;
import com.github.itdachen.auth.sdk.vo.AuthClazzMenuVO;
import tk.mybatis.mapper.common.Mapper;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * 岗位菜单 持久层接口
 *
 * @author 王大宸
 * @date 2025-01-01 17:12:50
 */
public interface IAuthClazzMenuMapper extends Mapper<AuthClazzMenu> {


    /***
     * 获取已经选中的菜单ID/按钮资源ID
     *
     * @author 王大宸
     * @date 2025/1/2 21:11
     * @param tenantId  租户ID
     * @param appId     应用ID
     * @param clazzCode 岗位代码
     * @return java.util.List<java.lang.String>
     */
    List<String> findClazzChecked(String tenantId, String appId, String clazzCode);

    void removeCheckedMenu(String tenantId, String appId, String clazzCode);

    /***
     * 批量添加
     *
     * @author 王大宸
     * @date 2025-01-01 17:12:50
     * @param list java.util.List<com.github.itdachen.auth.entity.AuthClazzMenu>
     */
    void batchSave(List<AuthClazzMenu> list);


}
