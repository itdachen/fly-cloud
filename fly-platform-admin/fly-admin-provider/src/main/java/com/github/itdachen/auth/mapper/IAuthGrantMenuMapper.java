package com.github.itdachen.auth.mapper;

import com.github.itdachen.auth.entity.AuthGrantMenu;
import com.github.itdachen.framework.context.tree.lay.TreeNode;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 权限下发 持久层接口
 *
 * @author 王大宸
 * @date 2025-01-01 17:12:49
 */
public interface IAuthGrantMenuMapper extends Mapper<AuthGrantMenu> {

    /***
     * 获取所有的菜单ID/按钮资源ID
     *
     * @author 王大宸
     * @date 2025/1/2 20:55
     * @param appId 应用ID
     * @return java.util.List<java.lang.String>
     */
    List<String> findAllByAppId(String appId);

    /***
     * 根据身份ID获取所有的菜单ID/按钮资源ID
     *
     * @author 王大宸
     * @date 2025/1/2 21:00
     * @param appId  应用ID
     * @param roleId 身份ID
     * @return java.util.List<java.lang.String>
     */
    List<String> findGrantMenuByAppId(String tenantId, String appId, String roleId);

    /***
     * 批量添加
     *
     * @author 王大宸
     * @date 2025-01-01 17:12:49
     * @param list java.util.List<com.github.itdachen.auth.entity.AuthGrantMenu>
     */
    void batchSave(List<AuthGrantMenu> list);

    /***
     * 获取菜单
     *
     * @author 王大宸
     * @date 2025/1/2 21:22
     * @param appId  应用ID
     * @param parentId 上级菜单
     * @param list 可用菜单ID
     * @return java.util.List<com.github.itdachen.framework.context.tree.lay.TreeNode>
     */
    List<TreeNode> findMenuTreeChildren(@Param("appId") String appId, @Param("parentId") String parentId, @Param("list") List<String> list);

    /***
     * 获取按钮
     *
     * @author 王大宸
     * @date 2025/1/2 21:22
     * @param appId  应用ID
     * @param list   可用菜单ID
     * @return java.util.List<com.github.itdachen.framework.context.tree.lay.TreeNode>
     */
    List<TreeNode> findElementChildren(@Param("appId") String appId, @Param("menuId") String menuId, @Param("list") List<String> list);

}
