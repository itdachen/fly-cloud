package com.github.itdachen.auth.service;

import com.github.itdachen.auth.sdk.dto.AuthGrantMenuDTO;
import com.github.itdachen.auth.sdk.query.AuthGrantMenuQuery;
import com.github.itdachen.auth.sdk.vo.AuthGrantMenuVO;
import com.github.itdachen.framework.context.tree.lay.LayTree;
import com.github.itdachen.framework.webmvc.service.IBizService;

/**
 * 权限下发 业务接口
 *
 * @author 王大宸
 * @date 2025-01-01 17:12:49
 */
public interface IAuthGrantMenuService extends IBizService<AuthGrantMenuDTO, AuthGrantMenuVO, AuthGrantMenuQuery, String> {

    /***
     * 获取下发可用的菜单/按钮资源
     *
     * @author 王大宸
     * @date 2025/1/2 21:04
     * @param appId      应用ID
     * @param clazzCode  岗位代码
     * @return com.github.itdachen.framework.context.tree.lay.LayTree
     */
    LayTree findGrantMenuTree(String appId, String clazzCode) throws Exception;

}
