package com.github.itdachen.auth.service.impl;

import com.github.itdachen.admin.convert.MenuInfoConvert;
import com.github.itdachen.admin.mapper.IElementInfoMapper;
import com.github.itdachen.admin.mapper.IMenuInfoMapper;
import com.github.itdachen.admin.sdk.vo.ElementInfoVO;
import com.github.itdachen.admin.sdk.vo.MenuInfoVO;
import com.github.itdachen.auth.interfaces.constants.AppConstants;
import com.github.itdachen.framework.context.BizContextHandler;
import com.github.itdachen.framework.core.response.TableData;
import com.github.itdachen.framework.core.utils.StringUtils;
import com.github.itdachen.framework.webmvc.entity.EntityUtils;
import com.github.itdachen.framework.webmvc.service.impl.BizServiceImpl;
import com.github.itdachen.framework.webmvc.poi.WorkBookUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.itdachen.auth.entity.AuthClazzMenu;
import com.github.itdachen.auth.sdk.dto.AuthClazzMenuDTO;
import com.github.itdachen.auth.sdk.query.AuthClazzMenuQuery;
import com.github.itdachen.auth.sdk.vo.AuthClazzMenuVO;
import com.github.itdachen.auth.mapper.IAuthClazzMenuMapper;
import com.github.itdachen.auth.service.IAuthClazzMenuService;
import com.github.itdachen.auth.convert.AuthClazzMenuConvert;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 岗位菜单 业务实现
 *
 * @author 王大宸
 * @date 2025-01-01 17:12:50
 */
@Service
public class AuthClazzMenuServiceImpl extends BizServiceImpl<IAuthClazzMenuMapper, AuthClazzMenu, AuthClazzMenuDTO, AuthClazzMenuVO, AuthClazzMenuQuery, String> implements IAuthClazzMenuService {
    private static final Logger logger = LoggerFactory.getLogger(AuthClazzMenuServiceImpl.class);
    private static final AuthClazzMenuConvert bizConvert = new AuthClazzMenuConvert();
    private final List<String> EXP_FIELDS = new ArrayList<>();
    private final IMenuInfoMapper menuInfoMapper;
    private final IElementInfoMapper elementInfoMapper;

    public AuthClazzMenuServiceImpl(IMenuInfoMapper menuInfoMapper, IElementInfoMapper elementInfoMapper) {
        super(bizConvert);
        EXP_FIELDS.add("应用ID");
        EXP_FIELDS.add("岗位代码");
        EXP_FIELDS.add("岗位名称");
        EXP_FIELDS.add("菜单ID/资源ID");
        EXP_FIELDS.add("菜单名称/资源名称");
        this.menuInfoMapper = menuInfoMapper;
        this.elementInfoMapper = elementInfoMapper;
    }

    /***
     * 分页
     *
     * @author 王大宸
     * @date 2025-01-01 17:12:50
     * @param params params
     * @return com.github.itdachen.framework.core.response.TableData<com.github.itdachen.auth.sdk.vo.authClazzMenuVo>
     */
    @Override
    public TableData<AuthClazzMenuVO> page(AuthClazzMenuQuery params) throws Exception {
        return new TableData<AuthClazzMenuVO>(0, new ArrayList<>());
    }

    /***
     * 新增
     *
     * @author 王大宸
     * @date 2025/1/2 22:16
     * @param authClazzMenuDTO authClazzMenuDTO
     * @return com.github.itdachen.auth.sdk.vo.AuthClazzMenuVO
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public AuthClazzMenuVO saveInfo(AuthClazzMenuDTO authClazzMenuDTO) throws Exception {
        if (StringUtils.isEmpty(authClazzMenuDTO.getAppId())) {
            authClazzMenuDTO.setAppId(MenuInfoConvert.ROOT_ID);
        }
        bizMapper.removeCheckedMenu(BizContextHandler.getTenantId(), authClazzMenuDTO.getAppId(), authClazzMenuDTO.getClazzCode());

        List<String> menuIds = new ArrayList<>(Arrays.asList(authClazzMenuDTO.getMenuId().split(",")));
        if (menuIds.isEmpty() || (1 == menuIds.size() && StringUtils.isEmpty(menuIds.get(0)))) {
            return null;
        }

        MenuInfoVO menuInfoVO = null;
        ElementInfoVO elementInfoVO = null;

        /* 这里是 layui-tree 获取选中的节点问题 START */
        List<String> oMenuIds = new ArrayList<>(menuIds);
        for (String menuId : oMenuIds) {
            elementInfoVO = elementInfoMapper.selectElementInfoVO(menuId);
            if (null == elementInfoVO) {
                menuInfoVO = menuInfoMapper.selectMenuInfoVO(menuId);
                findParentMenu(menuIds, menuInfoVO.getId());
                continue;
            }
            findParentMenu(menuIds, elementInfoVO.getMenuId());
        }

        // 去重
        HashSet<String> set = new HashSet<>(menuIds);
        menuIds = new ArrayList<>(set);

        /* 这里是 layui-tree 获取选中的节点问题 END */

        List<AuthClazzMenu> list = new ArrayList<>();
        AuthClazzMenu clazzMenu;

        for (String menuId : menuIds) {
            if (menuId.equals(authClazzMenuDTO.getAppId()) || StringUtils.isEmpty(menuId)) {
                continue;
            }
            clazzMenu = new AuthClazzMenu();
            clazzMenu.setId(EntityUtils.getId());
            clazzMenu.setTenantId(BizContextHandler.getTenantId());
            clazzMenu.setAppId(AppConstants.APP_ID);
            clazzMenu.setClazzCode(authClazzMenuDTO.getClazzCode());
            clazzMenu.setClazzTitle(authClazzMenuDTO.getClazzTitle());
            clazzMenu.setMenuId(menuId);
            clazzMenu.setMenuTitle("-");
            menuInfoVO = menuInfoMapper.selectMenuInfoVO(menuId);
            if (null != menuInfoVO) {
                clazzMenu.setMenuTitle(menuInfoVO.getTitle());
            }
            if (null == menuInfoVO) {
                elementInfoVO = elementInfoMapper.selectElementInfoVO(menuId);
                if (null != elementInfoVO) {
                    clazzMenu.setMenuTitle(elementInfoVO.getMenuTitle() + " — " + elementInfoVO.getTitle());
                }
            }
            list.add(clazzMenu);
        }
        if (list.isEmpty()) {
            return null;
        }
        bizMapper.batchSave(list);
        return null;
    }


    private void findParentMenu(List<String> menuIds, String menuId) {
        MenuInfoVO menuInfoVO = menuInfoMapper.selectMenuInfoVO(menuId);
        if (null != menuInfoVO) {
            menuIds.add(menuId);
            menuIds.add(menuInfoVO.getParentId());
            findParentMenu(menuIds, menuInfoVO.getParentId());
        }
    }


}
