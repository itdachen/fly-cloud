package com.github.itdachen.auth.service.impl;

import com.github.itdachen.admin.mapper.IClazzInfoMapper;
import com.github.itdachen.admin.sdk.query.ClazzInfoQuery;
import com.github.itdachen.admin.sdk.vo.ClazzInfoVO;
import com.github.itdachen.auth.interfaces.constants.AppConstants;
import com.github.itdachen.boot.autoconfigure.AppHelper;
import com.github.itdachen.framework.context.BizContextHandler;
import com.github.itdachen.framework.context.constants.YesOrNotConstant;
import com.github.itdachen.framework.context.exception.BizException;
import com.github.itdachen.framework.core.response.TableData;
import com.github.itdachen.framework.webmvc.entity.EntityUtils;
import com.github.itdachen.framework.webmvc.service.impl.BizServiceImpl;
import com.github.itdachen.framework.webmvc.poi.WorkBookUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.itdachen.auth.entity.AuthClazzRole;
import com.github.itdachen.auth.sdk.dto.AuthClazzRoleDTO;
import com.github.itdachen.auth.sdk.query.AuthClazzRoleQuery;
import com.github.itdachen.auth.sdk.vo.AuthClazzRoleVO;
import com.github.itdachen.auth.mapper.IAuthClazzRoleMapper;
import com.github.itdachen.auth.service.IAuthClazzRoleService;
import com.github.itdachen.auth.convert.AuthClazzRoleConvert;
import jakarta.persistence.Column;
import jakarta.persistence.Id;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 身份岗位管理表 业务实现
 *
 * @author 王大宸
 * @date 2025-01-01 17:12:50
 */
@Service
public class AuthClazzRoleServiceImpl extends BizServiceImpl<IAuthClazzRoleMapper, AuthClazzRole, AuthClazzRoleDTO, AuthClazzRoleVO, AuthClazzRoleQuery, String> implements IAuthClazzRoleService {
    private static final Logger logger = LoggerFactory.getLogger(AuthClazzRoleServiceImpl.class);
    private static final AuthClazzRoleConvert bizConvert = new AuthClazzRoleConvert();
    private final List<String> EXP_FIELDS = new ArrayList<>();

    @Autowired
    private IClazzInfoMapper clazzInfoMapper;

    public AuthClazzRoleServiceImpl() {
        super(bizConvert);
        EXP_FIELDS.add("应用ID");
        EXP_FIELDS.add("岗位代码");
        EXP_FIELDS.add("岗位名称");
        EXP_FIELDS.add("身份ID");
        EXP_FIELDS.add("身份名称");
        EXP_FIELDS.add("有效标志: Y-是;N-否");
    }

    /***
     * 分页
     *
     * @author 王大宸
     * @date 2025-01-01 17:12:50
     * @param params params
     * @return com.github.itdachen.framework.core.response.TableData<com.github.itdachen.auth.sdk.vo.authClazzRoleVo>
     */
    @Override
    public TableData<AuthClazzRoleVO> page(AuthClazzRoleQuery params) throws Exception {
        Page<AuthClazzRoleVO> page = PageHelper.startPage(params.getPage(), params.getLimit());
        List<AuthClazzRoleVO> list = bizMapper.list(params);
        return new TableData<AuthClazzRoleVO>(page.getTotal(), list);
    }

    /***
     * 新增
     *
     * @author 王大宸
     * @date 2025/1/16 21:27
     * @param authClazzRoleDTO authClazzRoleDTO
     * @return com.github.itdachen.auth.sdk.vo.AuthClazzRoleVO
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public AuthClazzRoleVO saveInfo(AuthClazzRoleDTO authClazzRoleDTO) throws Exception {
        bizMapper.removeAuthClazzRole(BizContextHandler.getTenantId(), authClazzRoleDTO.getRoleId());

        List<String> clazzCodes = new ArrayList<>(Arrays.asList(authClazzRoleDTO.getClazzCode().split(",")));
        if (clazzCodes.isEmpty()) {
            return null;
        }


        ClazzInfoQuery params = new ClazzInfoQuery();
        params.setTenantId(BizContextHandler.getTenantId());
        params.setList(clazzCodes);
        List<ClazzInfoVO> clazzInfos = clazzInfoMapper.findClazzInfoByCode(params);
        if (clazzInfos.isEmpty()) {
            throw new BizException("岗位信息不存在, 请刷新数据! ");
        }


        List<AuthClazzRole> list = new ArrayList<>();
        AuthClazzRole clazzRole = null;
        for (ClazzInfoVO clazzInfo : clazzInfos) {
            clazzRole = new AuthClazzRole();
            clazzRole.setAppId(AppConstants.APP_ID);
            clazzRole.setClazzCode(clazzInfo.getClazzCode());
            clazzRole.setClazzTitle(clazzInfo.getClazzTitle());
            clazzRole.setRoleId(authClazzRoleDTO.getRoleId());
            clazzRole.setRoleTitle(authClazzRoleDTO.getRoleTitle());
            clazzRole.setValidFlag(YesOrNotConstant.Y);
            clazzRole.setRemarks(YesOrNotConstant.Y);
            clazzRole.setDeleteFlag(YesOrNotConstant.N);
            EntityUtils.setCreatAndUpdateInfo(clazzRole);
            list.add(clazzRole);
        }
        bizMapper.batchSave(list);
        return null;
    }

    /***
     * 导出
     *
     * @author 王大宸
     * @date 2025-01-01 17:12:50
     * @param params com.github.itdachen.auth.sdk.query.AuthClazzRoleQuery
     * @return void
     */
    @Override
    public void dataExpToExcel(HttpServletRequest request,
                               HttpServletResponse response,
                               AuthClazzRoleQuery params) throws Exception {
        List<LinkedHashMap<String, String>> list = bizMapper.selectAuthClazzRoleExpData(params);
        WorkBookUtils.export(request, response)
                .params(params)
                .title("身份岗位管理表")
                .rowNum(true)
                .fields(EXP_FIELDS)
                .data(list)
                .execute();
    }

}
