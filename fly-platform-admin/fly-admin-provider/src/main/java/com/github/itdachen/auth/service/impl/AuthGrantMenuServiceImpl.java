package com.github.itdachen.auth.service.impl;

import com.github.itdachen.admin.convert.MenuInfoConvert;
import com.github.itdachen.auth.interfaces.constants.AppConstants;
import com.github.itdachen.auth.mapper.IAuthClazzMenuMapper;
import com.github.itdachen.framework.context.BizContextHandler;
import com.github.itdachen.framework.context.constants.UserTypeConstant;
import com.github.itdachen.framework.context.tree.lay.LayTree;
import com.github.itdachen.framework.context.tree.lay.TreeNode;
import com.github.itdachen.framework.core.response.TableData;
import com.github.itdachen.framework.core.utils.StringUtils;
import com.github.itdachen.framework.webmvc.service.impl.BizServiceImpl;
import com.github.itdachen.framework.webmvc.poi.WorkBookUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.itdachen.auth.entity.AuthGrantMenu;
import com.github.itdachen.auth.sdk.dto.AuthGrantMenuDTO;
import com.github.itdachen.auth.sdk.query.AuthGrantMenuQuery;
import com.github.itdachen.auth.sdk.vo.AuthGrantMenuVO;
import com.github.itdachen.auth.mapper.IAuthGrantMenuMapper;
import com.github.itdachen.auth.service.IAuthGrantMenuService;
import com.github.itdachen.auth.convert.AuthGrantMenuConvert;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 权限下发 业务实现
 *
 * @author 王大宸
 * @date 2025-01-01 17:12:49
 */
@Service
public class AuthGrantMenuServiceImpl extends BizServiceImpl<IAuthGrantMenuMapper, AuthGrantMenu, AuthGrantMenuDTO, AuthGrantMenuVO, AuthGrantMenuQuery, String> implements IAuthGrantMenuService {
    private static final Logger logger = LoggerFactory.getLogger(AuthGrantMenuServiceImpl.class);
    private static final AuthGrantMenuConvert bizConvert = new AuthGrantMenuConvert();
    private final List<String> EXP_FIELDS = new ArrayList<>();

    private final IAuthClazzMenuMapper authClazzMenuMapper;

    public AuthGrantMenuServiceImpl(IAuthClazzMenuMapper authClazzMenuMapper) {
        super(bizConvert);
        this.authClazzMenuMapper = authClazzMenuMapper;
        EXP_FIELDS.add("appID");
        EXP_FIELDS.add("用户ID/人员身份ID");
        EXP_FIELDS.add("菜单ID");
    }

    /***
     * 分页
     *
     * @author 王大宸
     * @date 2025-01-01 17:12:49
     * @param params params
     * @return com.github.itdachen.framework.core.response.TableData<com.github.itdachen.auth.sdk.vo.authGrantMenuVo>
     */
    @Override
    public TableData<AuthGrantMenuVO> page(AuthGrantMenuQuery params) throws Exception {
        return new TableData<AuthGrantMenuVO>(0, new ArrayList<>());
    }


    /***
     * 获取下发可用的菜单/按钮资源
     *
     * @author 王大宸
     * @date 2025/1/2 21:04
     * @param appId      应用ID
     * @param clazzCode  岗位代码
     * @return com.github.itdachen.framework.context.tree.lay.LayTree
     */
    @Override
    public LayTree findGrantMenuTree(String appId, String clazzCode) throws Exception {
        if (StringUtils.isEmpty(appId)) {
            appId = AppConstants.APP_ID;
        }
        List<String> menuIdList = new ArrayList<>();
        if (UserTypeConstant.SUPER_ADMINISTRATOR.equals(BizContextHandler.getUserType())) {
            menuIdList = bizMapper.findAllByAppId(appId);
        } else {
            menuIdList = bizMapper.findGrantMenuByAppId(BizContextHandler.getTenantId(), appId, BizContextHandler.getRoleId());
        }
        if (null == menuIdList || menuIdList.isEmpty()) {
            return new LayTree(new ArrayList<>(), new ArrayList<>());
        }
        List<String> checkedList = authClazzMenuMapper.findClazzChecked(BizContextHandler.getTenantId(), appId, clazzCode);
        List<TreeNode> menuTree = findMenuTreeChildren(appId, menuIdList, appId);

        TreeNode treeNode = new TreeNode(appId, "太虚十境", true);
        treeNode.setType("dirt");
        treeNode.setIcon("layui-icon-home");
        treeNode.setAttr1(appId);
        treeNode.setChildren(menuTree);

        List<TreeNode> list = new ArrayList<>();
        list.add(treeNode);

        return new LayTree(checkedList, list);
    }


    private List<TreeNode> findMenuTreeChildren(String appId, List<String> menuIdList, String parentId) {
        List<TreeNode> list = bizMapper.findMenuTreeChildren(appId, parentId, menuIdList);
        for (TreeNode treeNode : list) {
            if ("uri".equals(treeNode.getType()) || "third".equals(treeNode.getType())) {
                continue;
            }
            if ("menu".equals(treeNode.getType())) {
                treeNode.setChildren(bizMapper.findElementChildren(appId, treeNode.getId(), menuIdList));
                continue;
            }
            treeNode.setChildren(findMenuTreeChildren(appId, menuIdList, treeNode.getId()));
        }
        return list;
    }


}
