package com.github.itdachen.ai.interfaces;

/**
 * ${NAME}
 *
 * @author 王大宸
 * @date 2025-02-19 9:33
 */
public class Main {

    public static void main(String[] args) {
        System.out.println("Hello world!");
    }

}