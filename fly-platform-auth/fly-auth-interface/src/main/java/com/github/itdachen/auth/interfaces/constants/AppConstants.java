package com.github.itdachen.auth.interfaces.constants;

/**
 * 应用 ID 常量
 *
 * @author 王大宸
 * @date 2025-02-05 15:23
 */
public class AppConstants {

    public static String APP_ID = "4F17C3ED134AD4DA5C830DE9F3D7C02D";

}
