package com.github.itdachen.auth.interfaces.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 操作日志
 *
 * @author 王大宸
 * @date 2025-02-08 15:29
 */
public class OplogInfoClient implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    /**
     * 平台ID
     */
    private String platId;

    /**
     * 平台名称
     */
    private String platTitle;

    /**
     * 应用ID
     */
    private String appId;

    /**
     * 应用名称
     */
    private String appName;

    /**
     * 应用版本号
     */
    private String appVersion;

    /**
     * 租户ID/公司ID
     */
    private String tenantId;

    /**
     * 租户名称/公司名称
     */
    private String tenantTitle;

    /**
     * 省代码
     */
    private String provId;

    /**
     * 省名称
     */
    private String provName;

    /**
     * 市/州代码
     */
    private String cityId;

    /**
     * 市/州名称
     */
    private String cityName;

    /**
     * 区/县代码
     */
    private String countyId;

    /**
     * 区/县名称
     */
    private String countyName;

    /**
     * 部门ID
     */
    private String deptId;

    /**
     * 部门名称
     */
    private String deptTitle;

    /**
     * 部门等级
     */
    private String deptLevel;

    /**
     * 上级部门代码
     */
    private String deptParentId;

    /**
     * 用户ID/用户代码
     */
    private String userId;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 身份ID/角色ID
     */
    private String roleId;

    /**
     * 身份名称
     */
    private String roleName;

    /**
     * 日志类型: button, uri
     */
    private String elementType;

    /**
     * 日志类型: 按钮, 链接
     */
    private String elementTitle;

    /**
     * 功能标题: 用户管理, 菜单管理等
     */
    private String oplogFunc;

    /**
     * 操作类型: SAVE,UPDATE,JUMP,REMOVE,DELETE
     */
    private String oplogType;

    /**
     * 操作类型: 新增, 修改, 删除, 查看
     */
    private String oplogTitle;

    /**
     * 用户代理
     */
    private String userAgent;

    /**
     * 操作IP地址
     */
    private String hostIp;

    /**
     * 操作系统
     */
    private String hostOs;

    /**
     * 操作地址
     */
    private String hostAddress;

    /**
     * 操作浏览器
     */
    private String hostBrowser;

    /**
     * 操作地址国家代码
     */
    private String addrCountryCode;

    /**
     * 操作地址国家名称
     */
    private String addrCountryName;

    /**
     * 操作地址所属省代码
     */
    private String addrPoveCode;

    /**
     * 操作地址所属省名称
     */
    private String addrPoveName;

    /**
     * 操作地址所属市州代码
     */
    private String addrCityCode;

    /**
     * 操作地址所属市州名称
     */
    private String addrCityName;

    /**
     * 请求ID
     */
    private String requestId;

    /**
     * 请求URI
     */
    private String requestUri;

    /**
     * 请求方式: POST, PUT, GET, DELETE
     */
    private String requestMethod;

    /**
     * 请求参数
     */
    private String requestBody;

    /**
     * 请求时间
     */
    private LocalDateTime requestTime;

    /**
     * 相应数据
     */
    private String responseBody;

    /**
     * 相应状态码
     */
    private String responseCode;

    /**
     * 返回消息
     */
    private String responseMsg;

    /**
     * 操作状态: 成功, 失败, 异常
     */
    private String responseStatus;

    /**
     * 响应时间
     */
    private LocalDateTime responseTime;

    /**
     * 异常信息
     */
    private String exInfo;

    /**
     * 服务端处理耗时
     */
    private String executeTime;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 年份
     */
    private String yearly;

    /**
     * 月份
     */
    private String monthly;


    public OplogInfoClient() {
    }

    public OplogInfoClient(String id, String platId, String platTitle, String appId, String appName, String appVersion, String tenantId, String tenantTitle, String provId, String provName, String cityId, String cityName, String countyId, String countyName, String deptId, String deptTitle, String deptLevel, String deptParentId, String userId, String nickName, String roleId, String roleName, String elementType, String elementTitle, String oplogFunc, String oplogType, String oplogTitle, String userAgent, String hostIp, String hostOs, String hostAddress, String hostBrowser, String addrCountryCode, String addrCountryName, String addrPoveCode, String addrPoveName, String addrCityCode, String addrCityName, String requestId, String requestUri, String requestMethod, String requestBody, LocalDateTime requestTime, String responseBody, String responseCode, String responseMsg, String responseStatus, LocalDateTime responseTime, String exInfo, String executeTime, String remarks, String yearly, String monthly) {
        this.id = id;
        this.platId = platId;
        this.platTitle = platTitle;
        this.appId = appId;
        this.appName = appName;
        this.appVersion = appVersion;
        this.tenantId = tenantId;
        this.tenantTitle = tenantTitle;
        this.provId = provId;
        this.provName = provName;
        this.cityId = cityId;
        this.cityName = cityName;
        this.countyId = countyId;
        this.countyName = countyName;
        this.deptId = deptId;
        this.deptTitle = deptTitle;
        this.deptLevel = deptLevel;
        this.deptParentId = deptParentId;
        this.userId = userId;
        this.nickName = nickName;
        this.roleId = roleId;
        this.roleName = roleName;
        this.elementType = elementType;
        this.elementTitle = elementTitle;
        this.oplogFunc = oplogFunc;
        this.oplogType = oplogType;
        this.oplogTitle = oplogTitle;
        this.userAgent = userAgent;
        this.hostIp = hostIp;
        this.hostOs = hostOs;
        this.hostAddress = hostAddress;
        this.hostBrowser = hostBrowser;
        this.addrCountryCode = addrCountryCode;
        this.addrCountryName = addrCountryName;
        this.addrPoveCode = addrPoveCode;
        this.addrPoveName = addrPoveName;
        this.addrCityCode = addrCityCode;
        this.addrCityName = addrCityName;
        this.requestId = requestId;
        this.requestUri = requestUri;
        this.requestMethod = requestMethod;
        this.requestBody = requestBody;
        this.requestTime = requestTime;
        this.responseBody = responseBody;
        this.responseCode = responseCode;
        this.responseMsg = responseMsg;
        this.responseStatus = responseStatus;
        this.responseTime = responseTime;
        this.exInfo = exInfo;
        this.executeTime = executeTime;
        this.remarks = remarks;
        this.yearly = yearly;
        this.monthly = monthly;
    }

    public static OplogInfoClientBuilder builder() {
        return new OplogInfoClientBuilder();
    }

    public static class OplogInfoClientBuilder {
        private String id;
        private String platId;
        private String platTitle;
        private String appId;
        private String appName;
        private String appVersion;
        private String tenantId;
        private String tenantTitle;
        private String provId;
        private String provName;
        private String cityId;
        private String cityName;
        private String countyId;
        private String countyName;
        private String deptId;
        private String deptTitle;
        private String deptLevel;
        private String deptParentId;
        private String userId;
        private String nickName;
        private String roleId;
        private String roleName;
        private String elementType;
        private String elementTitle;
        private String oplogFunc;
        private String oplogType;
        private String oplogTitle;
        private String userAgent;
        private String hostIp;
        private String hostOs;
        private String hostAddress;
        private String hostBrowser;
        private String addrCountryCode;
        private String addrCountryName;
        private String addrPoveCode;
        private String addrPoveName;
        private String addrCityCode;
        private String addrCityName;
        private String requestId;
        private String requestUri;
        private String requestMethod;
        private String requestBody;
        private LocalDateTime requestTime;
        private String responseBody;
        private String responseCode;
        private String responseMsg;
        private String responseStatus;
        private LocalDateTime responseTime;
        private String exInfo;
        private String executeTime;
        private String remarks;
        private String yearly;
        private String monthly;

        public OplogInfoClientBuilder() {
        }

        /* 主键 */
        public OplogInfoClientBuilder id(String id) {
            this.id = id;
            return this;
        }

        /* 平台ID */
        public OplogInfoClientBuilder platId(String platId) {
            this.platId = platId;
            return this;
        }

        /* 平台名称 */
        public OplogInfoClientBuilder platTitle(String platTitle) {
            this.platTitle = platTitle;
            return this;
        }

        /* 应用ID */
        public OplogInfoClientBuilder appId(String appId) {
            this.appId = appId;
            return this;
        }

        /* 应用名称 */
        public OplogInfoClientBuilder appName(String appName) {
            this.appName = appName;
            return this;
        }

        /* 应用版本号 */
        public OplogInfoClientBuilder appVersion(String appVersion) {
            this.appVersion = appVersion;
            return this;
        }

        /* 租户ID/公司ID */
        public OplogInfoClientBuilder tenantId(String tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        /* 租户名称/公司名称 */
        public OplogInfoClientBuilder tenantTitle(String tenantTitle) {
            this.tenantTitle = tenantTitle;
            return this;
        }

        /* 省代码 */
        public OplogInfoClientBuilder provId(String provId) {
            this.provId = provId;
            return this;
        }

        /* 省名称 */
        public OplogInfoClientBuilder provName(String provName) {
            this.provName = provName;
            return this;
        }

        /* 市/州代码 */
        public OplogInfoClientBuilder cityId(String cityId) {
            this.cityId = cityId;
            return this;
        }

        /* 市/州名称 */
        public OplogInfoClientBuilder cityName(String cityName) {
            this.cityName = cityName;
            return this;
        }

        /* 区/县代码 */
        public OplogInfoClientBuilder countyId(String countyId) {
            this.countyId = countyId;
            return this;
        }

        /* 区/县名称 */
        public OplogInfoClientBuilder countyName(String countyName) {
            this.countyName = countyName;
            return this;
        }

        /* 部门ID */
        public OplogInfoClientBuilder deptId(String deptId) {
            this.deptId = deptId;
            return this;
        }

        /* 部门名称 */
        public OplogInfoClientBuilder deptTitle(String deptTitle) {
            this.deptTitle = deptTitle;
            return this;
        }

        /* 部门等级 */
        public OplogInfoClientBuilder deptLevel(String deptLevel) {
            this.deptLevel = deptLevel;
            return this;
        }

        /* 上级部门代码 */
        public OplogInfoClientBuilder deptParentId(String deptParentId) {
            this.deptParentId = deptParentId;
            return this;
        }

        /* 用户ID/用户代码 */
        public OplogInfoClientBuilder userId(String userId) {
            this.userId = userId;
            return this;
        }

        /* 昵称 */
        public OplogInfoClientBuilder nickName(String nickName) {
            this.nickName = nickName;
            return this;
        }

        /* 身份ID/角色ID */
        public OplogInfoClientBuilder roleId(String roleId) {
            this.roleId = roleId;
            return this;
        }

        /* 身份名称 */
        public OplogInfoClientBuilder roleName(String roleName) {
            this.roleName = roleName;
            return this;
        }

        /* 日志类型: button, uri */
        public OplogInfoClientBuilder elementType(String elementType) {
            this.elementType = elementType;
            return this;
        }

        /* 日志类型: 按钮, 链接 */
        public OplogInfoClientBuilder elementTitle(String elementTitle) {
            this.elementTitle = elementTitle;
            return this;
        }

        /* 功能标题: 用户管理, 菜单管理等 */
        public OplogInfoClientBuilder oplogFunc(String oplogFunc) {
            this.oplogFunc = oplogFunc;
            return this;
        }

        /* 操作类型: SAVE,UPDATE,JUMP,REMOVE,DELETE */
        public OplogInfoClientBuilder oplogType(String oplogType) {
            this.oplogType = oplogType;
            return this;
        }

        /* 操作类型: 新增, 修改, 删除, 查看 */
        public OplogInfoClientBuilder oplogTitle(String oplogTitle) {
            this.oplogTitle = oplogTitle;
            return this;
        }

        /* 用户代理 */
        public OplogInfoClientBuilder userAgent(String userAgent) {
            this.userAgent = userAgent;
            return this;
        }

        /* 操作IP地址 */
        public OplogInfoClientBuilder hostIp(String hostIp) {
            this.hostIp = hostIp;
            return this;
        }

        /* 操作系统 */
        public OplogInfoClientBuilder hostOs(String hostOs) {
            this.hostOs = hostOs;
            return this;
        }

        /* 操作地址 */
        public OplogInfoClientBuilder hostAddress(String hostAddress) {
            this.hostAddress = hostAddress;
            return this;
        }

        /* 操作浏览器 */
        public OplogInfoClientBuilder hostBrowser(String hostBrowser) {
            this.hostBrowser = hostBrowser;
            return this;
        }

        /* 操作地址国家代码 */
        public OplogInfoClientBuilder addrCountryCode(String addrCountryCode) {
            this.addrCountryCode = addrCountryCode;
            return this;
        }

        /* 操作地址国家名称 */
        public OplogInfoClientBuilder addrCountryName(String addrCountryName) {
            this.addrCountryName = addrCountryName;
            return this;
        }

        /* 操作地址所属省代码 */
        public OplogInfoClientBuilder addrPoveCode(String addrPoveCode) {
            this.addrPoveCode = addrPoveCode;
            return this;
        }

        /* 操作地址所属省名称 */
        public OplogInfoClientBuilder addrPoveName(String addrPoveName) {
            this.addrPoveName = addrPoveName;
            return this;
        }

        /* 操作地址所属市州代码 */
        public OplogInfoClientBuilder addrCityCode(String addrCityCode) {
            this.addrCityCode = addrCityCode;
            return this;
        }

        /* 操作地址所属市州名称 */
        public OplogInfoClientBuilder addrCityName(String addrCityName) {
            this.addrCityName = addrCityName;
            return this;
        }

        /* 请求ID */
        public OplogInfoClientBuilder requestId(String requestId) {
            this.requestId = requestId;
            return this;
        }

        /* 请求URI */
        public OplogInfoClientBuilder requestUri(String requestUri) {
            this.requestUri = requestUri;
            return this;
        }

        /* 请求方式: POST, PUT, GET, DELETE */
        public OplogInfoClientBuilder requestMethod(String requestMethod) {
            this.requestMethod = requestMethod;
            return this;
        }

        /* 请求参数 */
        public OplogInfoClientBuilder requestBody(String requestBody) {
            this.requestBody = requestBody;
            return this;
        }

        /* 请求时间 */
        public OplogInfoClientBuilder requestTime(LocalDateTime requestTime) {
            this.requestTime = requestTime;
            return this;
        }

        /* 相应数据 */
        public OplogInfoClientBuilder responseBody(String responseBody) {
            this.responseBody = responseBody;
            return this;
        }

        /* 相应状态码 */
        public OplogInfoClientBuilder responseCode(String responseCode) {
            this.responseCode = responseCode;
            return this;
        }

        /* 返回消息 */
        public OplogInfoClientBuilder responseMsg(String responseMsg) {
            this.responseMsg = responseMsg;
            return this;
        }

        /* 操作状态: 成功, 失败, 异常 */
        public OplogInfoClientBuilder responseStatus(String responseStatus) {
            this.responseStatus = responseStatus;
            return this;
        }

        /* 响应时间 */
        public OplogInfoClientBuilder responseTime(LocalDateTime responseTime) {
            this.responseTime = responseTime;
            return this;
        }

        /* 异常信息 */
        public OplogInfoClientBuilder exInfo(String exInfo) {
            this.exInfo = exInfo;
            return this;
        }

        /* 服务端处理耗时 */
        public OplogInfoClientBuilder executeTime(String executeTime) {
            this.executeTime = executeTime;
            return this;
        }

        /* 备注 */
        public OplogInfoClientBuilder remarks(String remarks) {
            this.remarks = remarks;
            return this;
        }

        /* 年份 */
        public OplogInfoClientBuilder yearly(String yearly) {
            this.yearly = yearly;
            return this;
        }

        /* 月份 */
        public OplogInfoClientBuilder monthly(String monthly) {
            this.monthly = monthly;
            return this;
        }

        public OplogInfoClient build() {
            return new OplogInfoClient(id,
                    platId,
                    platTitle,
                    appId,
                    appName,
                    appVersion,
                    tenantId,
                    tenantTitle,
                    provId,
                    provName,
                    cityId,
                    cityName,
                    countyId,
                    countyName,
                    deptId,
                    deptTitle,
                    deptLevel,
                    deptParentId,
                    userId,
                    nickName,
                    roleId,
                    roleName,
                    elementType,
                    elementTitle,
                    oplogFunc,
                    oplogType,
                    oplogTitle,
                    userAgent,
                    hostIp,
                    hostOs,
                    hostAddress,
                    hostBrowser,
                    addrCountryCode,
                    addrCountryName,
                    addrPoveCode,
                    addrPoveName,
                    addrCityCode,
                    addrCityName,
                    requestId,
                    requestUri,
                    requestMethod,
                    requestBody,
                    requestTime,
                    responseBody,
                    responseCode,
                    responseMsg,
                    responseStatus,
                    responseTime,
                    exInfo,
                    executeTime,
                    remarks,
                    yearly,
                    monthly
            );
        }

    }


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setPlatId(String platId) {
        this.platId = platId;
    }

    public String getPlatId() {
        return platId;
    }

    public void setPlatTitle(String platTitle) {
        this.platTitle = platTitle;
    }

    public String getPlatTitle() {
        return platTitle;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantTitle(String tenantTitle) {
        this.tenantTitle = tenantTitle;
    }

    public String getTenantTitle() {
        return tenantTitle;
    }

    public void setProvId(String provId) {
        this.provId = provId;
    }

    public String getProvId() {
        return provId;
    }

    public void setProvName(String provName) {
        this.provName = provName;
    }

    public String getProvName() {
        return provName;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCountyId(String countyId) {
        this.countyId = countyId;
    }

    public String getCountyId() {
        return countyId;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptTitle(String deptTitle) {
        this.deptTitle = deptTitle;
    }

    public String getDeptTitle() {
        return deptTitle;
    }

    public void setDeptLevel(String deptLevel) {
        this.deptLevel = deptLevel;
    }

    public String getDeptLevel() {
        return deptLevel;
    }

    public void setDeptParentId(String deptParentId) {
        this.deptParentId = deptParentId;
    }

    public String getDeptParentId() {
        return deptParentId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setElementType(String elementType) {
        this.elementType = elementType;
    }

    public String getElementType() {
        return elementType;
    }

    public void setElementTitle(String elementTitle) {
        this.elementTitle = elementTitle;
    }

    public String getElementTitle() {
        return elementTitle;
    }

    public void setOplogFunc(String oplogFunc) {
        this.oplogFunc = oplogFunc;
    }

    public String getOplogFunc() {
        return oplogFunc;
    }

    public void setOplogType(String oplogType) {
        this.oplogType = oplogType;
    }

    public String getOplogType() {
        return oplogType;
    }

    public void setOplogTitle(String oplogTitle) {
        this.oplogTitle = oplogTitle;
    }

    public String getOplogTitle() {
        return oplogTitle;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setHostIp(String hostIp) {
        this.hostIp = hostIp;
    }

    public String getHostIp() {
        return hostIp;
    }

    public void setHostOs(String hostOs) {
        this.hostOs = hostOs;
    }

    public String getHostOs() {
        return hostOs;
    }

    public void setHostAddress(String hostAddress) {
        this.hostAddress = hostAddress;
    }

    public String getHostAddress() {
        return hostAddress;
    }

    public void setHostBrowser(String hostBrowser) {
        this.hostBrowser = hostBrowser;
    }

    public String getHostBrowser() {
        return hostBrowser;
    }

    public void setAddrCountryCode(String addrCountryCode) {
        this.addrCountryCode = addrCountryCode;
    }

    public String getAddrCountryCode() {
        return addrCountryCode;
    }

    public void setAddrCountryName(String addrCountryName) {
        this.addrCountryName = addrCountryName;
    }

    public String getAddrCountryName() {
        return addrCountryName;
    }

    public void setAddrPoveCode(String addrPoveCode) {
        this.addrPoveCode = addrPoveCode;
    }

    public String getAddrPoveCode() {
        return addrPoveCode;
    }

    public void setAddrPoveName(String addrPoveName) {
        this.addrPoveName = addrPoveName;
    }

    public String getAddrPoveName() {
        return addrPoveName;
    }

    public void setAddrCityCode(String addrCityCode) {
        this.addrCityCode = addrCityCode;
    }

    public String getAddrCityCode() {
        return addrCityCode;
    }

    public void setAddrCityName(String addrCityName) {
        this.addrCityName = addrCityName;
    }

    public String getAddrCityName() {
        return addrCityName;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri;
    }

    public String getRequestUri() {
        return requestUri;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestTime(LocalDateTime requestTime) {
        this.requestTime = requestTime;
    }

    public LocalDateTime getRequestTime() {
        return requestTime;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseTime(LocalDateTime responseTime) {
        this.responseTime = responseTime;
    }

    public LocalDateTime getResponseTime() {
        return responseTime;
    }

    public void setExInfo(String exInfo) {
        this.exInfo = exInfo;
    }

    public String getExInfo() {
        return exInfo;
    }

    public void setExecuteTime(String executeTime) {
        this.executeTime = executeTime;
    }

    public String getExecuteTime() {
        return executeTime;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setYearly(String yearly) {
        this.yearly = yearly;
    }

    public String getYearly() {
        return yearly;
    }

    public void setMonthly(String monthly) {
        this.monthly = monthly;
    }

    public String getMonthly() {
        return monthly;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("platId", getPlatId())
                .append("platTitle", getPlatTitle())
                .append("appId", getAppId())
                .append("appName", getAppName())
                .append("appVersion", getAppVersion())
                .append("tenantId", getTenantId())
                .append("tenantTitle", getTenantTitle())
                .append("provId", getProvId())
                .append("provName", getProvName())
                .append("cityId", getCityId())
                .append("cityName", getCityName())
                .append("countyId", getCountyId())
                .append("countyName", getCountyName())
                .append("deptId", getDeptId())
                .append("deptTitle", getDeptTitle())
                .append("deptLevel", getDeptLevel())
                .append("deptParentId", getDeptParentId())
                .append("userId", getUserId())
                .append("nickName", getNickName())
                .append("roleId", getRoleId())
                .append("roleName", getRoleName())
                .append("elementType", getElementType())
                .append("elementTitle", getElementTitle())
                .append("oplogFunc", getOplogFunc())
                .append("oplogType", getOplogType())
                .append("oplogTitle", getOplogTitle())
                .append("userAgent", getUserAgent())
                .append("hostIp", getHostIp())
                .append("hostOs", getHostOs())
                .append("hostAddress", getHostAddress())
                .append("hostBrowser", getHostBrowser())
                .append("addrCountryCode", getAddrCountryCode())
                .append("addrCountryName", getAddrCountryName())
                .append("addrPoveCode", getAddrPoveCode())
                .append("addrPoveName", getAddrPoveName())
                .append("addrCityCode", getAddrCityCode())
                .append("addrCityName", getAddrCityName())
                .append("requestId", getRequestId())
                .append("requestUri", getRequestUri())
                .append("requestMethod", getRequestMethod())
                .append("requestBody", getRequestBody())
                .append("requestTime", getRequestTime())
                .append("responseBody", getResponseBody())
                .append("responseCode", getResponseCode())
                .append("responseMsg", getResponseMsg())
                .append("responseStatus", getResponseStatus())
                .append("responseTime", getResponseTime())
                .append("exInfo", getExInfo())
                .append("executeTime", getExecuteTime())
                .append("remarks", getRemarks())
                .append("yearly", getYearly())
                .append("monthly", getMonthly())
                .toString();
    }


}
