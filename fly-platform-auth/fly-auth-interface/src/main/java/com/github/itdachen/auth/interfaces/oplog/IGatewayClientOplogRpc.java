package com.github.itdachen.auth.interfaces.oplog;

import com.github.itdachen.auth.interfaces.entity.OplogInfoClient;

/**
 * 日志处理
 *
 * @author 王大宸
 * @date 2025-02-07 11:00
 */
public interface IGatewayClientOplogRpc {

    /***
     * 添加日志
     *
     * @author 王大宸
     * @date 2025/2/10 10:38
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    void saveOplog(OplogInfoClient oplogInfoClient);

    /***
     * 追加日志基础信息
     *
     * @author 王大宸
     * @date 2025/2/10 10:37
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    void appendOplog(OplogInfoClient oplogInfoClient);

    /***
     * 追加日志响应信息
     *
     * @author 王大宸
     * @date 2025/2/10 11:07
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    void appendResponseOplog(OplogInfoClient oplogInfoClient);

    /***
     * 权限不足
     *
     * @author 王大宸
     * @date 2025/2/10 10:38
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    void deniedPermission(OplogInfoClient oplogInfoClient);

    /***
     * 删除日志
     *
     * @author 王大宸
     * @date 2025/2/10 10:38
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    void remove(OplogInfoClient oplogInfoClient);

}
