package com.github.itdachen.auth.interfaces.permissions;

import com.github.itdachen.framework.context.permission.CheckPermissionInfo;
import com.github.itdachen.framework.context.userdetails.UserInfoDetails;

/**
 * 权限校验
 *
 * @author 王大宸
 * @date 2025-02-14 14:30
 */
public interface ICheckPermissionInfoRpc {

    CheckPermissionInfo checkPermissionInfoMono(UserInfoDetails userInfoDetails, String method, String requestUri);

}
