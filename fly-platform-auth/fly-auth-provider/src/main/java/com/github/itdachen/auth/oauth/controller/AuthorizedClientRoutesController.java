package com.github.itdachen.auth.oauth.controller;


import com.github.itdachen.auth.oauth.service.IAuthorizedPermissionService;
import com.github.itdachen.auth.oauth.service.IAuthorizedLayRoutesService;
import com.github.itdachen.framework.context.annotation.CurrentUser;
import com.github.itdachen.framework.context.userdetails.UserInfoDetails;
import com.github.itdachen.framework.core.response.ServerResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/***
 * 获取菜单路由
 *
 * @author 王大宸
 * @date 2023/5/5 14:48
 */
@RestController
@RequestMapping(value = "/authorized/client")
public class AuthorizedClientRoutesController {

    private final IAuthorizedLayRoutesService layClientRoutesService;
    private final IAuthorizedPermissionService permissionService;

    public AuthorizedClientRoutesController(IAuthorizedLayRoutesService layClientRoutesService,
                                            IAuthorizedPermissionService permissionService) {
        this.layClientRoutesService = layClientRoutesService;
        this.permissionService = permissionService;
    }

    /***
     * 获取菜单/路由
     *
     * @author 王大宸
     * @date 2023/5/5 14:56
     * @return com.github.itdachen.framework.core.response.ServerResponse<java.lang.Object>
     */
    @GetMapping("/routes")
    public ServerResponse<Object> clientRoutes(@CurrentUser UserInfoDetails userDetails) throws Exception {
        return ServerResponse.ok(layClientRoutesService.routes(userDetails));
    }

    /***
     * 获取权限编码
     *
     * @author 王大宸
     * @date 2023/5/5 14:57
     * @return com.github.itdachen.framework.core.response.ServerResponse<java.lang.Object>
     */
    @GetMapping("/permissions")
    public ServerResponse<List<String>> clientPermission(@CurrentUser UserInfoDetails userDetails) throws Exception {
        return ServerResponse.ok(permissionService.permissions(userDetails));
    }

}
