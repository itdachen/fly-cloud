package com.github.itdachen.auth.oauth.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 操作日志
 *
 * @author 王大宸
 * @date 2025-02-14 10:19:06
 */
@Table(name = "fly_next_oplog_info")
public class OplogInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Id
    @Column(name = "id")
    private String id;

    /**
     * 平台ID
     */
    @Column(name = "plat_id")
    private String platId;

    /**
     * 平台名称
     */
    @Column(name = "plat_title")
    private String platTitle;

    /**
     * 应用ID
     */
    @Column(name = "app_id")
    private String appId;

    /**
     * 应用名称
     */
    @Column(name = "app_name")
    private String appName;

    /**
     * 应用版本号
     */
    @Column(name = "app_version")
    private String appVersion;

    /**
     * 租户ID/公司ID
     */
    @Column(name = "tenant_id")
    private String tenantId;

    /**
     * 租户名称/公司名称
     */
    @Column(name = "tenant_title")
    private String tenantTitle;

    /**
     * 省代码
     */
    @Column(name = "prov_id")
    private String provId;

    /**
     * 省名称
     */
    @Column(name = "prov_name")
    private String provName;

    /**
     * 市/州代码
     */
    @Column(name = "city_id")
    private String cityId;

    /**
     * 市/州名称
     */
    @Column(name = "city_name")
    private String cityName;

    /**
     * 区/县代码
     */
    @Column(name = "county_id")
    private String countyId;

    /**
     * 区/县名称
     */
    @Column(name = "county_name")
    private String countyName;

    /**
     * 部门ID
     */
    @Column(name = "dept_id")
    private String deptId;

    /**
     * 部门名称
     */
    @Column(name = "dept_title")
    private String deptTitle;

    /**
     * 部门等级
     */
    @Column(name = "dept_level")
    private String deptLevel;

    /**
     * 上级部门代码
     */
    @Column(name = "dept_parent_id")
    private String deptParentId;

    /**
     * 用户ID/用户代码
     */
    @Column(name = "user_id")
    private String userId;

    /**
     * 昵称
     */
    @Column(name = "nick_name")
    private String nickName;

    /**
     * 身份ID/角色ID
     */
    @Column(name = "role_id")
    private String roleId;

    /**
     * 身份名称
     */
    @Column(name = "role_name")
    private String roleName;

    /**
     * 日志类型: button, uri
     */
    @Column(name = "element_type")
    private String elementType;

    /**
     * 日志类型: 按钮, 链接
     */
    @Column(name = "element_title")
    private String elementTitle;

    /**
     * 功能标题: 用户管理, 菜单管理等
     */
    @Column(name = "oplog_func")
    private String oplogFunc;

    /**
     * 操作类型: SAVE,UPDATE,JUMP,REMOVE,DELETE
     */
    @Column(name = "oplog_type")
    private String oplogType;

    /**
     * 操作类型: 新增, 修改, 删除, 查看
     */
    @Column(name = "oplog_title")
    private String oplogTitle;

    /**
     * 用户代理
     */
    @Column(name = "user_agent")
    private String userAgent;

    /**
     * 操作IP地址
     */
    @Column(name = "host_ip")
    private String hostIp;

    /**
     * 操作系统
     */
    @Column(name = "host_os")
    private String hostOs;

    /**
     * 操作地址
     */
    @Column(name = "host_address")
    private String hostAddress;

    /**
     * 操作浏览器
     */
    @Column(name = "host_browser")
    private String hostBrowser;

    /**
     * 操作地址国家代码
     */
    @Column(name = "addr_country_code")
    private String addrCountryCode;

    /**
     * 操作地址国家名称
     */
    @Column(name = "addr_country_name")
    private String addrCountryName;

    /**
     * 操作地址所属省代码
     */
    @Column(name = "addr_pove_code")
    private String addrPoveCode;

    /**
     * 操作地址所属省名称
     */
    @Column(name = "addr_pove_name")
    private String addrPoveName;

    /**
     * 操作地址所属市州代码
     */
    @Column(name = "addr_city_code")
    private String addrCityCode;

    /**
     * 操作地址所属市州名称
     */
    @Column(name = "addr_city_name")
    private String addrCityName;

    /**
     * 请求ID
     */
    @Column(name = "request_id")
    private String requestId;

    /**
     * 请求URI
     */
    @Column(name = "request_uri")
    private String requestUri;

    /**
     * 请求方式: POST, PUT, GET, DELETE
     */
    @Column(name = "request_method")
    private String requestMethod;

    /**
     * 请求参数
     */
    @Column(name = "request_body")
    private String requestBody;

    /**
     * 请求时间
     */
    @Column(name = "request_time")
    private LocalDateTime requestTime;

    /**
     * 相应数据
     */
    @Column(name = "response_body")
    private String responseBody;

    /**
     * 相应状态码
     */
    @Column(name = "response_code")
    private String responseCode;

    /**
     * 返回消息
     */
    @Column(name = "response_msg")
    private String responseMsg;

    /**
     * 操作状态: 成功, 失败, 异常
     */
    @Column(name = "response_status")
    private String responseStatus;

    /**
     * 响应时间
     */
    @Column(name = "response_time")
    private LocalDateTime responseTime;

    /**
     * 异常信息
     */
    @Column(name = "ex_info")
    private String exInfo;

    /**
     * 服务端处理耗时
     */
    @Column(name = "execute_time")
    private String executeTime;

    /**
     * 备注
     */
    @Column(name = "remarks")
    private String remarks;

    /**
     * 删除标记:N-未删除;Y-已删除
     */
    @Column(name = "delete_flag")
    private String deleteFlag;

    /**
     * 年份
     */
    @Column(name = "yearly")
    private String yearly;

    /**
     * 月份
     */
    @Column(name = "monthly")
    private String monthly;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private LocalDateTime createTime;

    /**
     * 创建人
     */
    @Column(name = "create_user")
    private String createUser;

    /**
     * 创建人id
     */
    @Column(name = "create_user_id")
    private String createUserId;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private LocalDateTime updateTime;

    /**
     * 更新人
     */
    @Column(name = "update_user")
    private String updateUser;

    /**
     * 更新人id
     */
    @Column(name = "update_user_id")
    private String updateUserId;


    public OplogInfo() {
    }


    public OplogInfo(String id, String platId, String platTitle, String appId, String appName, String appVersion, String tenantId, String tenantTitle, String provId, String provName, String cityId, String cityName, String countyId, String countyName, String deptId, String deptTitle, String deptLevel, String deptParentId, String userId, String nickName, String roleId, String roleName, String elementType, String elementTitle, String oplogFunc, String oplogType, String oplogTitle, String userAgent, String hostIp, String hostOs, String hostAddress, String hostBrowser, String addrCountryCode, String addrCountryName, String addrPoveCode, String addrPoveName, String addrCityCode, String addrCityName, String requestId, String requestUri, String requestMethod, String requestBody, LocalDateTime requestTime, String responseBody, String responseCode, String responseMsg, String responseStatus, LocalDateTime responseTime, String exInfo, String executeTime, String remarks, String deleteFlag, String yearly, String monthly, LocalDateTime createTime, String createUser, String createUserId, LocalDateTime updateTime, String updateUser, String updateUserId) {
        this.id = id;
        this.platId = platId;
        this.platTitle = platTitle;
        this.appId = appId;
        this.appName = appName;
        this.appVersion = appVersion;
        this.tenantId = tenantId;
        this.tenantTitle = tenantTitle;
        this.provId = provId;
        this.provName = provName;
        this.cityId = cityId;
        this.cityName = cityName;
        this.countyId = countyId;
        this.countyName = countyName;
        this.deptId = deptId;
        this.deptTitle = deptTitle;
        this.deptLevel = deptLevel;
        this.deptParentId = deptParentId;
        this.userId = userId;
        this.nickName = nickName;
        this.roleId = roleId;
        this.roleName = roleName;
        this.elementType = elementType;
        this.elementTitle = elementTitle;
        this.oplogFunc = oplogFunc;
        this.oplogType = oplogType;
        this.oplogTitle = oplogTitle;
        this.userAgent = userAgent;
        this.hostIp = hostIp;
        this.hostOs = hostOs;
        this.hostAddress = hostAddress;
        this.hostBrowser = hostBrowser;
        this.addrCountryCode = addrCountryCode;
        this.addrCountryName = addrCountryName;
        this.addrPoveCode = addrPoveCode;
        this.addrPoveName = addrPoveName;
        this.addrCityCode = addrCityCode;
        this.addrCityName = addrCityName;
        this.requestId = requestId;
        this.requestUri = requestUri;
        this.requestMethod = requestMethod;
        this.requestBody = requestBody;
        this.requestTime = requestTime;
        this.responseBody = responseBody;
        this.responseCode = responseCode;
        this.responseMsg = responseMsg;
        this.responseStatus = responseStatus;
        this.responseTime = responseTime;
        this.exInfo = exInfo;
        this.executeTime = executeTime;
        this.remarks = remarks;
        this.deleteFlag = deleteFlag;
        this.yearly = yearly;
        this.monthly = monthly;
        this.createTime = createTime;
        this.createUser = createUser;
        this.createUserId = createUserId;
        this.updateTime = updateTime;
        this.updateUser = updateUser;
        this.updateUserId = updateUserId;
    }

    public static OplogInfoBuilder builder() {
        return new OplogInfoBuilder();
    }

    public static class OplogInfoBuilder {
        private String id;
        private String platId;
        private String platTitle;
        private String appId;
        private String appName;
        private String appVersion;
        private String tenantId;
        private String tenantTitle;
        private String provId;
        private String provName;
        private String cityId;
        private String cityName;
        private String countyId;
        private String countyName;
        private String deptId;
        private String deptTitle;
        private String deptLevel;
        private String deptParentId;
        private String userId;
        private String nickName;
        private String roleId;
        private String roleName;
        private String elementType;
        private String elementTitle;
        private String oplogFunc;
        private String oplogType;
        private String oplogTitle;
        private String userAgent;
        private String hostIp;
        private String hostOs;
        private String hostAddress;
        private String hostBrowser;
        private String addrCountryCode;
        private String addrCountryName;
        private String addrPoveCode;
        private String addrPoveName;
        private String addrCityCode;
        private String addrCityName;
        private String requestId;
        private String requestUri;
        private String requestMethod;
        private String requestBody;
        private LocalDateTime requestTime;
        private String responseBody;
        private String responseCode;
        private String responseMsg;
        private String responseStatus;
        private LocalDateTime responseTime;
        private String exInfo;
        private String executeTime;
        private String remarks;
        private String deleteFlag;
        private String yearly;
        private String monthly;
        private LocalDateTime createTime;
        private String createUser;
        private String createUserId;
        private LocalDateTime updateTime;
        private String updateUser;
        private String updateUserId;

        public OplogInfoBuilder() {
        }

        /* 主键 */
        public OplogInfoBuilder id(String id) {
            this.id = id;
            return this;
        }

        /* 平台ID */
        public OplogInfoBuilder platId(String platId) {
            this.platId = platId;
            return this;
        }

        /* 平台名称 */
        public OplogInfoBuilder platTitle(String platTitle) {
            this.platTitle = platTitle;
            return this;
        }

        /* 应用ID */
        public OplogInfoBuilder appId(String appId) {
            this.appId = appId;
            return this;
        }

        /* 应用名称 */
        public OplogInfoBuilder appName(String appName) {
            this.appName = appName;
            return this;
        }

        /* 应用版本号 */
        public OplogInfoBuilder appVersion(String appVersion) {
            this.appVersion = appVersion;
            return this;
        }

        /* 租户ID/公司ID */
        public OplogInfoBuilder tenantId(String tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        /* 租户名称/公司名称 */
        public OplogInfoBuilder tenantTitle(String tenantTitle) {
            this.tenantTitle = tenantTitle;
            return this;
        }

        /* 省代码 */
        public OplogInfoBuilder provId(String provId) {
            this.provId = provId;
            return this;
        }

        /* 省名称 */
        public OplogInfoBuilder provName(String provName) {
            this.provName = provName;
            return this;
        }

        /* 市/州代码 */
        public OplogInfoBuilder cityId(String cityId) {
            this.cityId = cityId;
            return this;
        }

        /* 市/州名称 */
        public OplogInfoBuilder cityName(String cityName) {
            this.cityName = cityName;
            return this;
        }

        /* 区/县代码 */
        public OplogInfoBuilder countyId(String countyId) {
            this.countyId = countyId;
            return this;
        }

        /* 区/县名称 */
        public OplogInfoBuilder countyName(String countyName) {
            this.countyName = countyName;
            return this;
        }

        /* 部门ID */
        public OplogInfoBuilder deptId(String deptId) {
            this.deptId = deptId;
            return this;
        }

        /* 部门名称 */
        public OplogInfoBuilder deptTitle(String deptTitle) {
            this.deptTitle = deptTitle;
            return this;
        }

        /* 部门等级 */
        public OplogInfoBuilder deptLevel(String deptLevel) {
            this.deptLevel = deptLevel;
            return this;
        }

        /* 上级部门代码 */
        public OplogInfoBuilder deptParentId(String deptParentId) {
            this.deptParentId = deptParentId;
            return this;
        }

        /* 用户ID/用户代码 */
        public OplogInfoBuilder userId(String userId) {
            this.userId = userId;
            return this;
        }

        /* 昵称 */
        public OplogInfoBuilder nickName(String nickName) {
            this.nickName = nickName;
            return this;
        }

        /* 身份ID/角色ID */
        public OplogInfoBuilder roleId(String roleId) {
            this.roleId = roleId;
            return this;
        }

        /* 身份名称 */
        public OplogInfoBuilder roleName(String roleName) {
            this.roleName = roleName;
            return this;
        }

        /* 日志类型: button, uri */
        public OplogInfoBuilder elementType(String elementType) {
            this.elementType = elementType;
            return this;
        }

        /* 日志类型: 按钮, 链接 */
        public OplogInfoBuilder elementTitle(String elementTitle) {
            this.elementTitle = elementTitle;
            return this;
        }

        /* 功能标题: 用户管理, 菜单管理等 */
        public OplogInfoBuilder oplogFunc(String oplogFunc) {
            this.oplogFunc = oplogFunc;
            return this;
        }

        /* 操作类型: SAVE,UPDATE,JUMP,REMOVE,DELETE */
        public OplogInfoBuilder oplogType(String oplogType) {
            this.oplogType = oplogType;
            return this;
        }

        /* 操作类型: 新增, 修改, 删除, 查看 */
        public OplogInfoBuilder oplogTitle(String oplogTitle) {
            this.oplogTitle = oplogTitle;
            return this;
        }

        /* 用户代理 */
        public OplogInfoBuilder userAgent(String userAgent) {
            this.userAgent = userAgent;
            return this;
        }

        /* 操作IP地址 */
        public OplogInfoBuilder hostIp(String hostIp) {
            this.hostIp = hostIp;
            return this;
        }

        /* 操作系统 */
        public OplogInfoBuilder hostOs(String hostOs) {
            this.hostOs = hostOs;
            return this;
        }

        /* 操作地址 */
        public OplogInfoBuilder hostAddress(String hostAddress) {
            this.hostAddress = hostAddress;
            return this;
        }

        /* 操作浏览器 */
        public OplogInfoBuilder hostBrowser(String hostBrowser) {
            this.hostBrowser = hostBrowser;
            return this;
        }

        /* 操作地址国家代码 */
        public OplogInfoBuilder addrCountryCode(String addrCountryCode) {
            this.addrCountryCode = addrCountryCode;
            return this;
        }

        /* 操作地址国家名称 */
        public OplogInfoBuilder addrCountryName(String addrCountryName) {
            this.addrCountryName = addrCountryName;
            return this;
        }

        /* 操作地址所属省代码 */
        public OplogInfoBuilder addrPoveCode(String addrPoveCode) {
            this.addrPoveCode = addrPoveCode;
            return this;
        }

        /* 操作地址所属省名称 */
        public OplogInfoBuilder addrPoveName(String addrPoveName) {
            this.addrPoveName = addrPoveName;
            return this;
        }

        /* 操作地址所属市州代码 */
        public OplogInfoBuilder addrCityCode(String addrCityCode) {
            this.addrCityCode = addrCityCode;
            return this;
        }

        /* 操作地址所属市州名称 */
        public OplogInfoBuilder addrCityName(String addrCityName) {
            this.addrCityName = addrCityName;
            return this;
        }

        /* 请求ID */
        public OplogInfoBuilder requestId(String requestId) {
            this.requestId = requestId;
            return this;
        }

        /* 请求URI */
        public OplogInfoBuilder requestUri(String requestUri) {
            this.requestUri = requestUri;
            return this;
        }

        /* 请求方式: POST, PUT, GET, DELETE */
        public OplogInfoBuilder requestMethod(String requestMethod) {
            this.requestMethod = requestMethod;
            return this;
        }

        /* 请求参数 */
        public OplogInfoBuilder requestBody(String requestBody) {
            this.requestBody = requestBody;
            return this;
        }

        /* 请求时间 */
        public OplogInfoBuilder requestTime(LocalDateTime requestTime) {
            this.requestTime = requestTime;
            return this;
        }

        /* 相应数据 */
        public OplogInfoBuilder responseBody(String responseBody) {
            this.responseBody = responseBody;
            return this;
        }

        /* 相应状态码 */
        public OplogInfoBuilder responseCode(String responseCode) {
            this.responseCode = responseCode;
            return this;
        }

        /* 返回消息 */
        public OplogInfoBuilder responseMsg(String responseMsg) {
            this.responseMsg = responseMsg;
            return this;
        }

        /* 操作状态: 成功, 失败, 异常 */
        public OplogInfoBuilder responseStatus(String responseStatus) {
            this.responseStatus = responseStatus;
            return this;
        }

        /* 响应时间 */
        public OplogInfoBuilder responseTime(LocalDateTime responseTime) {
            this.responseTime = responseTime;
            return this;
        }

        /* 异常信息 */
        public OplogInfoBuilder exInfo(String exInfo) {
            this.exInfo = exInfo;
            return this;
        }

        /* 服务端处理耗时 */
        public OplogInfoBuilder executeTime(String executeTime) {
            this.executeTime = executeTime;
            return this;
        }

        /* 备注 */
        public OplogInfoBuilder remarks(String remarks) {
            this.remarks = remarks;
            return this;
        }

        /* 删除标记:N-未删除;Y-已删除 */
        public OplogInfoBuilder deleteFlag(String deleteFlag) {
            this.deleteFlag = deleteFlag;
            return this;
        }

        /* 年份 */
        public OplogInfoBuilder yearly(String yearly) {
            this.yearly = yearly;
            return this;
        }

        /* 月份 */
        public OplogInfoBuilder monthly(String monthly) {
            this.monthly = monthly;
            return this;
        }

        /* 创建时间 */
        public OplogInfoBuilder createTime(LocalDateTime createTime) {
            this.createTime = createTime;
            return this;
        }

        /* 创建人 */
        public OplogInfoBuilder createUser(String createUser) {
            this.createUser = createUser;
            return this;
        }

        /* 创建人id */
        public OplogInfoBuilder createUserId(String createUserId) {
            this.createUserId = createUserId;
            return this;
        }

        /* 更新时间 */
        public OplogInfoBuilder updateTime(LocalDateTime updateTime) {
            this.updateTime = updateTime;
            return this;
        }

        /* 更新人 */
        public OplogInfoBuilder updateUser(String updateUser) {
            this.updateUser = updateUser;
            return this;
        }

        /* 更新人id */
        public OplogInfoBuilder updateUserId(String updateUserId) {
            this.updateUserId = updateUserId;
            return this;
        }

        public OplogInfo build() {
            return new OplogInfo(id,
                    platId,
                    platTitle,
                    appId,
                    appName,
                    appVersion,
                    tenantId,
                    tenantTitle,
                    provId,
                    provName,
                    cityId,
                    cityName,
                    countyId,
                    countyName,
                    deptId,
                    deptTitle,
                    deptLevel,
                    deptParentId,
                    userId,
                    nickName,
                    roleId,
                    roleName,
                    elementType,
                    elementTitle,
                    oplogFunc,
                    oplogType,
                    oplogTitle,
                    userAgent,
                    hostIp,
                    hostOs,
                    hostAddress,
                    hostBrowser,
                    addrCountryCode,
                    addrCountryName,
                    addrPoveCode,
                    addrPoveName,
                    addrCityCode,
                    addrCityName,
                    requestId,
                    requestUri,
                    requestMethod,
                    requestBody,
                    requestTime,
                    responseBody,
                    responseCode,
                    responseMsg,
                    responseStatus,
                    responseTime,
                    exInfo,
                    executeTime,
                    remarks,
                    deleteFlag,
                    yearly,
                    monthly,
                    createTime,
                    createUser,
                    createUserId,
                    updateTime,
                    updateUser,
                    updateUserId
            );
        }

    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setPlatId(String platId) {
        this.platId = platId;
    }

    public String getPlatId() {
        return platId;
    }

    public void setPlatTitle(String platTitle) {
        this.platTitle = platTitle;
    }

    public String getPlatTitle() {
        return platTitle;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantTitle(String tenantTitle) {
        this.tenantTitle = tenantTitle;
    }

    public String getTenantTitle() {
        return tenantTitle;
    }

    public void setProvId(String provId) {
        this.provId = provId;
    }

    public String getProvId() {
        return provId;
    }

    public void setProvName(String provName) {
        this.provName = provName;
    }

    public String getProvName() {
        return provName;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCountyId(String countyId) {
        this.countyId = countyId;
    }

    public String getCountyId() {
        return countyId;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptTitle(String deptTitle) {
        this.deptTitle = deptTitle;
    }

    public String getDeptTitle() {
        return deptTitle;
    }

    public void setDeptLevel(String deptLevel) {
        this.deptLevel = deptLevel;
    }

    public String getDeptLevel() {
        return deptLevel;
    }

    public void setDeptParentId(String deptParentId) {
        this.deptParentId = deptParentId;
    }

    public String getDeptParentId() {
        return deptParentId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setElementType(String elementType) {
        this.elementType = elementType;
    }

    public String getElementType() {
        return elementType;
    }

    public void setElementTitle(String elementTitle) {
        this.elementTitle = elementTitle;
    }

    public String getElementTitle() {
        return elementTitle;
    }

    public void setOplogFunc(String oplogFunc) {
        this.oplogFunc = oplogFunc;
    }

    public String getOplogFunc() {
        return oplogFunc;
    }

    public void setOplogType(String oplogType) {
        this.oplogType = oplogType;
    }

    public String getOplogType() {
        return oplogType;
    }

    public void setOplogTitle(String oplogTitle) {
        this.oplogTitle = oplogTitle;
    }

    public String getOplogTitle() {
        return oplogTitle;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setHostIp(String hostIp) {
        this.hostIp = hostIp;
    }

    public String getHostIp() {
        return hostIp;
    }

    public void setHostOs(String hostOs) {
        this.hostOs = hostOs;
    }

    public String getHostOs() {
        return hostOs;
    }

    public void setHostAddress(String hostAddress) {
        this.hostAddress = hostAddress;
    }

    public String getHostAddress() {
        return hostAddress;
    }

    public void setHostBrowser(String hostBrowser) {
        this.hostBrowser = hostBrowser;
    }

    public String getHostBrowser() {
        return hostBrowser;
    }

    public void setAddrCountryCode(String addrCountryCode) {
        this.addrCountryCode = addrCountryCode;
    }

    public String getAddrCountryCode() {
        return addrCountryCode;
    }

    public void setAddrCountryName(String addrCountryName) {
        this.addrCountryName = addrCountryName;
    }

    public String getAddrCountryName() {
        return addrCountryName;
    }

    public void setAddrPoveCode(String addrPoveCode) {
        this.addrPoveCode = addrPoveCode;
    }

    public String getAddrPoveCode() {
        return addrPoveCode;
    }

    public void setAddrPoveName(String addrPoveName) {
        this.addrPoveName = addrPoveName;
    }

    public String getAddrPoveName() {
        return addrPoveName;
    }

    public void setAddrCityCode(String addrCityCode) {
        this.addrCityCode = addrCityCode;
    }

    public String getAddrCityCode() {
        return addrCityCode;
    }

    public void setAddrCityName(String addrCityName) {
        this.addrCityName = addrCityName;
    }

    public String getAddrCityName() {
        return addrCityName;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri;
    }

    public String getRequestUri() {
        return requestUri;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestTime(LocalDateTime requestTime) {
        this.requestTime = requestTime;
    }

    public LocalDateTime getRequestTime() {
        return requestTime;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseTime(LocalDateTime responseTime) {
        this.responseTime = responseTime;
    }

    public LocalDateTime getResponseTime() {
        return responseTime;
    }

    public void setExInfo(String exInfo) {
        this.exInfo = exInfo;
    }

    public String getExInfo() {
        return exInfo;
    }

    public void setExecuteTime(String executeTime) {
        this.executeTime = executeTime;
    }

    public String getExecuteTime() {
        return executeTime;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setYearly(String yearly) {
        this.yearly = yearly;
    }

    public String getYearly() {
        return yearly;
    }

    public void setMonthly(String monthly) {
        this.monthly = monthly;
    }

    public String getMonthly() {
        return monthly;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public String getUpdateUserId() {
        return updateUserId;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("platId", getPlatId())
                .append("platTitle", getPlatTitle())
                .append("appId", getAppId())
                .append("appName", getAppName())
                .append("appVersion", getAppVersion())
                .append("tenantId", getTenantId())
                .append("tenantTitle", getTenantTitle())
                .append("provId", getProvId())
                .append("provName", getProvName())
                .append("cityId", getCityId())
                .append("cityName", getCityName())
                .append("countyId", getCountyId())
                .append("countyName", getCountyName())
                .append("deptId", getDeptId())
                .append("deptTitle", getDeptTitle())
                .append("deptLevel", getDeptLevel())
                .append("deptParentId", getDeptParentId())
                .append("userId", getUserId())
                .append("nickName", getNickName())
                .append("roleId", getRoleId())
                .append("roleName", getRoleName())
                .append("elementType", getElementType())
                .append("elementTitle", getElementTitle())
                .append("oplogFunc", getOplogFunc())
                .append("oplogType", getOplogType())
                .append("oplogTitle", getOplogTitle())
                .append("userAgent", getUserAgent())
                .append("hostIp", getHostIp())
                .append("hostOs", getHostOs())
                .append("hostAddress", getHostAddress())
                .append("hostBrowser", getHostBrowser())
                .append("addrCountryCode", getAddrCountryCode())
                .append("addrCountryName", getAddrCountryName())
                .append("addrPoveCode", getAddrPoveCode())
                .append("addrPoveName", getAddrPoveName())
                .append("addrCityCode", getAddrCityCode())
                .append("addrCityName", getAddrCityName())
                .append("requestId", getRequestId())
                .append("requestUri", getRequestUri())
                .append("requestMethod", getRequestMethod())
                .append("requestBody", getRequestBody())
                .append("requestTime", getRequestTime())
                .append("responseBody", getResponseBody())
                .append("responseCode", getResponseCode())
                .append("responseMsg", getResponseMsg())
                .append("responseStatus", getResponseStatus())
                .append("responseTime", getResponseTime())
                .append("exInfo", getExInfo())
                .append("executeTime", getExecuteTime())
                .append("remarks", getRemarks())
                .append("deleteFlag", getDeleteFlag())
                .append("yearly", getYearly())
                .append("monthly", getMonthly())
                .append("createTime", getCreateTime())
                .append("createUser", getCreateUser())
                .append("createUserId", getCreateUserId())
                .append("updateTime", getUpdateTime())
                .append("updateUser", getUpdateUser())
                .append("updateUserId", getUpdateUserId())
                .toString();
    }

}
