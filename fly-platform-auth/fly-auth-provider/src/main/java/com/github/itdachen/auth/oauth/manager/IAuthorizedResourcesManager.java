package com.github.itdachen.auth.oauth.manager;

import com.github.itdachen.framework.context.userdetails.UserInfoDetails;

import java.util.List;

/**
 * 获取用户可用资源
 *
 * @author 王大宸
 * @date 2025-02-05 15:13
 */
public interface IAuthorizedResourcesManager {

    /***
     * 获取可访问的资源ID
     *
     * @author 王大宸
     * @date 2025/2/5 15:16
     * @param userDetails userDetails
     * @return java.util.List<java.lang.String>
     */
    List<String> obtainAccessibleElementId(UserInfoDetails userDetails);


}
