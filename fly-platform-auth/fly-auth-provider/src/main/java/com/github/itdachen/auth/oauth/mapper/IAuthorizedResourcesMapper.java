package com.github.itdachen.auth.oauth.mapper;

import com.github.itdachen.framework.context.permission.PermissionInfo;

import java.util.List;

/**
 * AuthorizedResourcesMapper
 *
 * @author 王大宸
 * @date 2025-02-05 15:19
 */
public interface IAuthorizedResourcesMapper {


    /***
     * 获取所有的按钮资源ID
     *
     * @author 王大宸
     * @date 2025/2/5 16:09
     * @param appId appId
     * @return java.util.List<java.lang.String>
     */
    List<String> obtainAllElement(String appId);

    /***
     * 查询部需要授权的资源
     *
     * @author 王大宸
     * @date 2025/2/5 16:22
     * @param appId appId
     * @return java.util.List<java.lang.String>
     */
    List<String> findNoopAuthElement(String appId);

    /***
     * 根据身份ID获取授权的资源
     *
     * @author 王大宸
     * @date 2025/2/5 16:26
     * @param tenantId tenantId
     * @param roleId roleId
     * @return java.util.List<java.lang.String>
     */
    List<String> findClazzAuthElement(String appId, String tenantId, String roleId);


    /***
     * 根据资源(按钮ID)获取可访问的权限编码(前端权限控制调用)
     *
     * @author 王大宸
     * @date 2025/2/5 16:08
     * @param list list
     * @return java.util.List<com.github.itdachen.framework.context.permission.PermissionInfo>
     */
    List<String> obtainAccessibleAuthCodeByElementIdList(String appId, List<String> list);

    /***
     * 根据资源(按钮ID)获取可访问的权限(网关权限认证调用)
     *
     * @author 王大宸
     * @date 2025/2/5 16:08
     * @param list list
     * @return java.util.List<com.github.itdachen.framework.context.permission.PermissionInfo>
     */
    List<PermissionInfo> obtainAccessibleResourcesByElementIdList(String appId, List<String> list);


}
