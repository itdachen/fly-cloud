package com.github.itdachen.auth.oauth.mapper;

import java.util.List;
import java.util.Map;

/**
 * ILayClientRoutesMapper
 *
 * @author 王大宸
 * @date 2025-01-23 15:39
 */
public interface ILayClientRoutesMapper {

    List<Map<String, Object>> findMenuAll(String parentId);


}
