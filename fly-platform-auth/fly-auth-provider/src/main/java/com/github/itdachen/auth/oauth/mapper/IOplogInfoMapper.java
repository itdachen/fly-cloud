package com.github.itdachen.auth.oauth.mapper;

import com.github.itdachen.auth.oauth.entity.OplogInfo;
import tk.mybatis.mapper.common.Mapper;

/**
 * 操作日志 持久层接口
 *
 * @author 王大宸
 * @date 2025-02-14 10:20
 */
public interface IOplogInfoMapper extends Mapper<OplogInfo> {

}
