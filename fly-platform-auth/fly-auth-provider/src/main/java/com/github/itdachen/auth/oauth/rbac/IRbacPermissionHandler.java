package com.github.itdachen.auth.oauth.rbac;

import com.github.itdachen.framework.context.permission.CheckPermissionInfo;
import com.github.itdachen.framework.context.userdetails.UserInfoDetails;

/**
 * 权限校验
 *
 * @author 王大宸
 * @date 2025-02-18 14:55
 */
public interface IRbacPermissionHandler {

    /***
     * 权限校验
     *
     * @author 王大宸
     * @date 2025/2/18 14:58
     * @param userInfoDetails 用户信息
     * @param requestUri      请求地址
     * @param requestMethod   请求方式
     * @return com.github.itdachen.framework.context.permission.CheckPermissionInfo
     */
    CheckPermissionInfo handler(UserInfoDetails userInfoDetails, String requestUri, String requestMethod);

}
