package com.github.itdachen.auth.oauth.rbac.constants;

/**
 * 资源类型
 *
 * @author 王大宸
 * @date 2025-02-18 15:08
 */
public class ElementTypeConstant {

    /**
     * 权限资源类型
     */
    public final static String RESOURCE_TYPE_DIRT = "dirt";
    public final static String RESOURCE_TYPE_MENU = "menu";
    public final static String RESOURCE_TYPE_BTN = "button";
    public final static String RESOURCE_TYPE_URL = "url";

    /**
     * 请求类型
     */
    public final static String RESOURCE_REQUEST_METHOD_GET = "GET";
    public final static String RESOURCE_REQUEST_METHOD_PUT = "PUT";
    public final static String RESOURCE_REQUEST_METHOD_DELETE = "DELETE";
    public final static String RESOURCE_REQUEST_METHOD_POST = "POST";

    public final static String RESOURCE_ACTION_VISIT = "访问";

    public final static String BOOLEAN_NUMBER_FALSE = "0";

    public final static String BOOLEAN_NUMBER_TRUE = "1";

}
