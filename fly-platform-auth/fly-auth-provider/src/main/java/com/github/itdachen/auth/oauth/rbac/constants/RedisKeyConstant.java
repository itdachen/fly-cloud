package com.github.itdachen.auth.oauth.rbac.constants;

/**
 * 用户权限 存放 redis key
 *
 * @author 王大宸
 * @date 2025-02-18 15:06
 */
public class RedisKeyConstant {

    public static final String REDIS_KEY_USER_PERMISSIONS = "PERMISSIONS:USER:%s";

    public static final String REDIS_KEY_ALL_PERMISSIONS = "PERMISSIONS:ALL";

    public static final String REDIS_KEY_CAPTCHA = "CAPTCHA:%s";

    public static final String REDIS_KEY_TOKEN = "TOKEN";

    /**
     * 系统权限缓存有效期
     */
    public static final int APP_PERMISSIONS_TIME_OUT = 12;

    /**
     * 个人权限缓存有效期
     */
    public static final int USER_PERMISSIONS_TIME_OUT = 20;


}
