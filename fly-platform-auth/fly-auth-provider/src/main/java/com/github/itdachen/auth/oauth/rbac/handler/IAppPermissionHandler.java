package com.github.itdachen.auth.oauth.rbac.handler;

import com.github.itdachen.framework.context.permission.PermissionInfo;

import java.util.List;

/**
 * 查询应用所有的权限
 *
 * @author 王大宸
 * @date 2025-02-18 16:01
 */
public interface IAppPermissionHandler {

    List<PermissionInfo> permissions();


}
