package com.github.itdachen.auth.oauth.rbac.handler;

import com.github.itdachen.framework.context.permission.PermissionInfo;
import com.github.itdachen.framework.context.userdetails.UserInfoDetails;

import java.util.List;

/**
 * 获取用户权限信息接口
 *
 * @author 王大宸
 * @date 2025-02-18 16:01
 */
public interface IUserPermissionHandler {


    List<PermissionInfo> permissions(UserInfoDetails userInfoDetails);


}
