package com.github.itdachen.auth.oauth.rbac.handler.strategy;

import com.github.itdachen.auth.oauth.rbac.handler.IAppPermissionHandler;
import com.github.itdachen.framework.context.permission.PermissionInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 默认来源
 *
 * @author 王大宸
 * @date 2025-02-18 16:03
 */
public class DefaultAppPermissionHandler implements IAppPermissionHandler {
    private static final Logger logger = LoggerFactory.getLogger(DefaultAppPermissionHandler.class);

    @Override
    public List<PermissionInfo> permissions() {
        logger.warn("应用权限为加载，请实现 IAppPermissionHandler 接口！ ");
        return new ArrayList<>();
    }

}
