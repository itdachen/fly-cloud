package com.github.itdachen.auth.oauth.rbac.handler.strategy;

import com.github.itdachen.auth.oauth.rbac.handler.IUserPermissionHandler;
import com.github.itdachen.framework.context.permission.PermissionInfo;
import com.github.itdachen.framework.context.userdetails.UserInfoDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 默认实现用户权限来源
 *
 * @author 王大宸
 * @date 2025-02-18 16:05
 */
public class DefaultUserPermissionHandler implements IUserPermissionHandler {
    private static final Logger logger = LoggerFactory.getLogger(DefaultUserPermissionHandler.class);


    @Override
    public List<PermissionInfo> permissions(UserInfoDetails userInfoDetails) {
        logger.warn("用户权限为加载，请实现 IUserPermissionHandler 接口！ ");
        return new ArrayList<>();
    }
}
