package com.github.itdachen.auth.oauth.rbac.storage;

import com.github.itdachen.framework.context.permission.PermissionInfo;

import java.util.List;

/**
 * 查询应用所有权限
 *
 * @author 王大宸
 * @date 2025-02-18 15:10
 */
public interface IAppPermissionStorageHandler {


    /***
     * 获取应用所有权限
     *
     * @author 王大宸
     * @date 2025/2/18 15:19
     * @return java.util.List<com.github.itdachen.framework.context.permission.PermissionInfo>
     */
    List<PermissionInfo> findAppPermissions();


}
