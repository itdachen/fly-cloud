package com.github.itdachen.auth.oauth.rbac.storage;

import com.github.itdachen.framework.context.permission.PermissionInfo;
import com.github.itdachen.framework.context.userdetails.UserInfoDetails;

import java.util.List;

/**
 * 获取用户权限
 *
 * @author 王大宸
 * @date 2025-02-18 15:17
 */
public interface IUserPermissionStorageHandler {

    List<PermissionInfo> findUserPermissions(UserInfoDetails userInfoDetails);


}
