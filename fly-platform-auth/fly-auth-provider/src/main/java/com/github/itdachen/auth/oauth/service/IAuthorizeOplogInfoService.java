package com.github.itdachen.auth.oauth.service;

import com.github.itdachen.auth.interfaces.entity.OplogInfoClient;

/**
 * IOplogInfoService
 *
 * @author 王大宸
 * @date 2025-02-14 10:21
 */
public interface IAuthorizeOplogInfoService {

    /***
     * 添加日志
     *
     * @author 王大宸
     * @date 2025/2/10 10:38
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    void saveOplog(OplogInfoClient oplogInfoClient);

    /***
     * 追加日志基础信息
     *
     * @author 王大宸
     * @date 2025/2/10 10:37
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    void appendOplog(OplogInfoClient oplogInfoClient);

    /***
     * 追加日志响应信息
     *
     * @author 王大宸
     * @date 2025/2/10 11:07
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    void appendResponseOplog(OplogInfoClient oplogInfoClient);

    /***
     * 权限不足
     *
     * @author 王大宸
     * @date 2025/2/10 10:38
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    void deniedPermission(OplogInfoClient oplogInfoClient);

    /***
     * 删除日志
     *
     * @author 王大宸
     * @date 2025/2/10 10:38
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    void remove(OplogInfoClient oplogInfoClient);


}
