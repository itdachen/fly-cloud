package com.github.itdachen.auth.oauth.service;

import com.github.itdachen.framework.context.permission.CheckPermissionInfo;
import com.github.itdachen.framework.context.userdetails.UserInfoDetails;

/**
 * IAuthorizedCheckPermissionService
 *
 * @author 王大宸
 * @date 2025-02-14 14:36
 */
public interface IAuthorizedCheckPermissionService {

    CheckPermissionInfo checkPermissionInfo(UserInfoDetails userInfoDetails, String method, String requestUri);

}
