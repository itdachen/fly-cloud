package com.github.itdachen.auth.oauth.service;

import com.github.itdachen.framework.context.userdetails.UserInfoDetails;

/**
 * 获取菜单列表
 *
 * @author 王大宸
 * @date 2025-01-23 15:31
 */
public interface IAuthorizedLayRoutesService {

    /***
    * 获取菜单
    *
    * @author 王大宸
    * @date 2025/1/23 15:33
    * @return java.lang.Object
    */
    Object routes(UserInfoDetails userDetails);

}
