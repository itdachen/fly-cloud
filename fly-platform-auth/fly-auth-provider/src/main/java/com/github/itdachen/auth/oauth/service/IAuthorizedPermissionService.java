package com.github.itdachen.auth.oauth.service;

import com.github.itdachen.framework.context.userdetails.UserInfoDetails;

import java.util.List;

/**
 * IClientPermissionService
 *
 * @author 王大宸
 * @date 2025-02-05 15:06
 */
public interface IAuthorizedPermissionService {

    List<String> permissions(UserInfoDetails userDetails) throws Exception;

}
