package com.github.itdachen.auth.oauth.service.impl;

import com.github.itdachen.auth.interfaces.entity.OplogInfoClient;
import com.github.itdachen.auth.oauth.entity.OplogInfo;
import com.github.itdachen.auth.oauth.mapper.IOplogInfoMapper;
import com.github.itdachen.auth.oauth.service.IAuthorizeOplogInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * OplogInfoServiceImpl
 *
 * @author 王大宸
 * @date 2025-02-14 10:21
 */
@Service
public class AuthorizeOplogInfoServiceImpl implements IAuthorizeOplogInfoService {

    private final IOplogInfoMapper oplogInfoMapper;

    public AuthorizeOplogInfoServiceImpl(IOplogInfoMapper oplogInfoMapper) {
        this.oplogInfoMapper = oplogInfoMapper;
    }

    /***
     * 添加日志
     *
     * @author 王大宸
     * @date 2025/2/10 10:38
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    @Override
    public void saveOplog(OplogInfoClient oplogInfoClient) {
        LocalDateTime now = LocalDateTime.now();
        OplogInfo oplogInfo = toJavaObject(oplogInfoClient);
        oplogInfo.setCreateTime(now);
        oplogInfo.setCreateUserId(oplogInfoClient.getUserId());
        oplogInfo.setCreateUser(oplogInfoClient.getNickName());

        oplogInfo.setUpdateTime(now);
        oplogInfo.setUpdateUserId(oplogInfoClient.getUserId());
        oplogInfo.setUpdateUser(oplogInfoClient.getNickName());

        oplogInfo.setYearly(String.valueOf(now.getYear()));
        oplogInfo.setMonthly(String.valueOf(now.getMonthValue()));
        oplogInfoMapper.insertSelective(oplogInfo);
    }

    /***
     * 追加日志信息
     *
     * @author 王大宸
     * @date 2025/2/10 11:07
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    @Override
    public void appendOplog(OplogInfoClient oplogInfoClient) {
        OplogInfo oplogInfo = toJavaObject(oplogInfoClient);
        appendUpdateOplog(oplogInfo, oplogInfoClient);
        oplogInfoMapper.updateByPrimaryKeySelective(oplogInfo);
    }

    /***
     * 追加日志响应信息
     *
     * @author 王大宸
     * @date 2025/2/10 11:07
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    @Override
    public void appendResponseOplog(OplogInfoClient oplogInfoClient) {
        OplogInfo oplogInfo = toJavaObject(oplogInfoClient);
        appendUpdateOplog(oplogInfo, oplogInfoClient);
        oplogInfoMapper.updateByPrimaryKeySelective(oplogInfo);
    }

    /***
     * 权限不足
     *
     * @author 王大宸
     * @date 2025/2/10 10:38
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    @Override
    public void deniedPermission(OplogInfoClient oplogInfoClient) {
        OplogInfo oplogInfo = toJavaObject(oplogInfoClient);
        appendUpdateOplog(oplogInfo, oplogInfoClient);
        oplogInfoMapper.updateByPrimaryKeySelective(oplogInfo);
    }

    /***
     * 删除日志
     *
     * @author 王大宸
     * @date 2025/2/10 10:38
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    @Override
    public void remove(OplogInfoClient oplogInfoClient) {
        oplogInfoMapper.deleteByPrimaryKey(oplogInfoClient.getId());
    }


    /***
     * 添加更新时间
     *
     * @author 王大宸
     * @date 2025/2/14 10:49
     * @param oplogInfo oplogInfo
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    private void appendUpdateOplog(OplogInfo oplogInfo, OplogInfoClient oplogInfoClient) {
        OplogInfo oplogInfoDb = oplogInfoMapper.selectByPrimaryKey(oplogInfoClient.getId());
        final String executeTime = computeExecuteTime(oplogInfoDb, oplogInfoClient);
        oplogInfo.setExecuteTime(executeTime);
        oplogInfo.setUpdateTime(LocalDateTime.now());
        oplogInfo.setUpdateUserId(oplogInfoDb.getUserId());
        oplogInfo.setUpdateUser(oplogInfoDb.getNickName());
        oplogInfo.setYearly(oplogInfoDb.getYearly());
        oplogInfo.setMonthly(oplogInfoDb.getMonthly());
    }


    /***
     * 计算服务器处理耗时
     *
     * @author 王大宸
     * @date 2025/2/14 10:34
     * @param oplogInfo 已经入库的数据此信息
     * @param oplogInfoClient 需要处理的数据信息
     * @return java.lang.String
     */
    private String computeExecuteTime(OplogInfo oplogInfo, OplogInfoClient oplogInfoClient) {
        LocalDateTime requestTime = oplogInfo.getRequestTime();
        LocalDateTime responseTime = oplogInfoClient.getResponseTime();
        if (null == responseTime) {
            responseTime = LocalDateTime.now();
        }
        // long seconds = Math.abs(responseTime.until(requestTime, ChronoUnit.SECONDS));

        final Duration duration = Duration.between(requestTime, responseTime);
        long seconds = duration.getSeconds();

        return seconds + "";
    }


    /***
     * 类型转换
     *
     * @author 王大宸
     * @date 2025/2/14 10:51
     * @param oplogInfoClient oplogInfoClient
     * @return com.github.itdachen.auth.oauth.entity.OplogInfo
     */
    private OplogInfo toJavaObject(OplogInfoClient oplogInfoClient) {
        if (null == oplogInfoClient) {
            return null;
        }
        OplogInfo oplogInfo = new OplogInfo();
        oplogInfo.setId(oplogInfoClient.getId());
        oplogInfo.setPlatId(oplogInfoClient.getPlatId());
        oplogInfo.setPlatTitle(oplogInfoClient.getPlatTitle());
        oplogInfo.setAppId(oplogInfoClient.getAppId());
        oplogInfo.setAppName(oplogInfoClient.getAppName());
        oplogInfo.setAppVersion(oplogInfoClient.getAppVersion());
        oplogInfo.setTenantId(oplogInfoClient.getTenantId());
        oplogInfo.setTenantTitle(oplogInfoClient.getTenantTitle());
        oplogInfo.setProvId(oplogInfoClient.getProvId());
        oplogInfo.setProvName(oplogInfoClient.getProvName());
        oplogInfo.setCityId(oplogInfoClient.getCityId());
        oplogInfo.setCityName(oplogInfoClient.getCityName());
        oplogInfo.setCountyId(oplogInfoClient.getCountyId());
        oplogInfo.setCountyName(oplogInfoClient.getCountyName());
        oplogInfo.setDeptId(oplogInfoClient.getDeptId());
        oplogInfo.setDeptTitle(oplogInfoClient.getDeptTitle());
        oplogInfo.setDeptLevel(oplogInfoClient.getDeptLevel());
        oplogInfo.setDeptParentId(oplogInfoClient.getDeptParentId());
        oplogInfo.setUserId(oplogInfoClient.getUserId());
        oplogInfo.setNickName(oplogInfoClient.getNickName());
        oplogInfo.setRoleId(oplogInfoClient.getRoleId());
        oplogInfo.setRoleName(oplogInfoClient.getRoleName());
        oplogInfo.setElementType(oplogInfoClient.getElementType());
        oplogInfo.setElementTitle(oplogInfoClient.getElementTitle());
        oplogInfo.setOplogFunc(oplogInfoClient.getOplogFunc());
        oplogInfo.setOplogType(oplogInfoClient.getOplogType());
        oplogInfo.setOplogTitle(oplogInfoClient.getOplogTitle());
        oplogInfo.setUserAgent(oplogInfoClient.getUserAgent());
        oplogInfo.setHostIp(oplogInfoClient.getHostIp());
        oplogInfo.setHostOs(oplogInfoClient.getHostOs());
        oplogInfo.setHostAddress(oplogInfoClient.getHostAddress());
        oplogInfo.setHostBrowser(oplogInfoClient.getHostBrowser());
        oplogInfo.setAddrCountryCode(oplogInfoClient.getAddrCountryCode());
        oplogInfo.setAddrCountryName(oplogInfoClient.getAddrCountryName());
        oplogInfo.setAddrPoveCode(oplogInfoClient.getAddrPoveCode());
        oplogInfo.setAddrPoveName(oplogInfoClient.getAddrPoveName());
        oplogInfo.setAddrCityCode(oplogInfoClient.getAddrCityCode());
        oplogInfo.setAddrCityName(oplogInfoClient.getAddrCityName());
        oplogInfo.setRequestId(oplogInfoClient.getRequestId());
        oplogInfo.setRequestUri(oplogInfoClient.getRequestUri());
        oplogInfo.setRequestMethod(oplogInfoClient.getRequestMethod());
        oplogInfo.setRequestBody(oplogInfoClient.getRequestBody());
        oplogInfo.setRequestTime(oplogInfoClient.getRequestTime());
        oplogInfo.setResponseBody(oplogInfoClient.getResponseBody());
        oplogInfo.setResponseCode(oplogInfoClient.getResponseCode());
        oplogInfo.setResponseMsg(oplogInfoClient.getResponseMsg());
        oplogInfo.setResponseStatus(oplogInfoClient.getResponseStatus());
        oplogInfo.setResponseTime(oplogInfoClient.getResponseTime());
        oplogInfo.setExInfo(oplogInfoClient.getExInfo());
        oplogInfo.setExecuteTime(oplogInfoClient.getExecuteTime());
        oplogInfo.setRemarks(oplogInfoClient.getRemarks());
        oplogInfo.setYearly(oplogInfoClient.getYearly());
        oplogInfo.setMonthly(oplogInfoClient.getMonthly());
        return oplogInfo;
    }


}
