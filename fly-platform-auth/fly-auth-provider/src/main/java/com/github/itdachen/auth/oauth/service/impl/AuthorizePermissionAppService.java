package com.github.itdachen.auth.oauth.service.impl;

import com.github.itdachen.auth.interfaces.constants.AppConstants;
import com.github.itdachen.auth.oauth.mapper.IAuthorizedResourcesMapper;
import com.github.itdachen.auth.oauth.rbac.handler.IAppPermissionHandler;
import com.github.itdachen.framework.context.permission.PermissionInfo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 获取应用权限
 *
 * @author 王大宸
 * @date 2025-02-19 9:53
 */
@Service
public class AuthorizePermissionAppService implements IAppPermissionHandler {

    private final IAuthorizedResourcesMapper authorizedResourcesMapper;

    public AuthorizePermissionAppService(IAuthorizedResourcesMapper authorizedResourcesMapper) {
        this.authorizedResourcesMapper = authorizedResourcesMapper;
    }


    /***
     * 查询所有的权限
     *
     * @author 王大宸
     * @date 2025/2/19 10:07
     * @return java.util.List<com.github.itdachen.framework.context.permission.PermissionInfo>
     */
    @Override
    public List<PermissionInfo> permissions() {
        final String appId = AppConstants.APP_ID;
        List<String> list = authorizedResourcesMapper.obtainAllElement(appId);
        return authorizedResourcesMapper.obtainAccessibleResourcesByElementIdList(appId, list);
    }


}
