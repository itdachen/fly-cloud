package com.github.itdachen.auth.oauth.service.impl;

import com.github.itdachen.auth.interfaces.constants.AppConstants;
import com.github.itdachen.auth.oauth.manager.IAuthorizedResourcesManager;
import com.github.itdachen.auth.oauth.mapper.IAuthorizedResourcesMapper;
import com.github.itdachen.auth.oauth.rbac.handler.IUserPermissionHandler;
import com.github.itdachen.framework.context.constants.UserTypeConstant;
import com.github.itdachen.framework.context.constants.YesOrNotConstant;
import com.github.itdachen.framework.context.permission.PermissionInfo;
import com.github.itdachen.framework.context.userdetails.UserInfoDetails;
import org.checkerframework.checker.units.qual.C;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 获取用户权限
 *
 * @author 王大宸
 * @date 2025-02-19 9:54
 */
@Service
public class AuthorizePermissionUserService implements IUserPermissionHandler {
    private static final Logger logger = LoggerFactory.getLogger(AuthorizePermissionUserService.class);

    private final IAuthorizedResourcesManager authorizedResourcesManager;
    private final IAuthorizedResourcesMapper authorizedResourcesMapper;

    public AuthorizePermissionUserService(IAuthorizedResourcesMapper authorizedResourcesMapper, IAuthorizedResourcesManager authorizedResourcesManager) {
        this.authorizedResourcesMapper = authorizedResourcesMapper;
        this.authorizedResourcesManager = authorizedResourcesManager;
    }


    @Override
    public List<PermissionInfo> permissions(UserInfoDetails userInfoDetails) {
        List<String> list = new ArrayList<>();
        if (UserTypeConstant.SUPER_ADMINISTRATOR.equals(userInfoDetails.getUserType())
                && YesOrNotConstant.Y.equals(userInfoDetails.getRoleFlag())) {
            list = authorizedResourcesMapper.obtainAllElement(AppConstants.APP_ID);
        } else {
            list = authorizedResourcesManager.obtainAccessibleElementId(userInfoDetails);
        }
        return authorizedResourcesMapper.obtainAccessibleResourcesByElementIdList(AppConstants.APP_ID, list);
    }


}
