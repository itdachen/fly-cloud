package com.github.itdachen.auth.oauth.service.impl;

import com.github.itdachen.auth.oauth.rbac.IRbacPermissionHandler;
import com.github.itdachen.auth.oauth.service.IAuthorizedCheckPermissionService;
import com.github.itdachen.framework.context.permission.CheckPermissionInfo;
import com.github.itdachen.framework.context.userdetails.UserInfoDetails;
import org.springframework.stereotype.Service;

/**
 * 权限认证
 *
 * @author 王大宸
 * @date 2025-02-14 14:36
 */
@Service
public class AuthorizedCheckPermissionServiceImpl implements IAuthorizedCheckPermissionService {

    private final IRbacPermissionHandler rbacPermissionHandler;

    public AuthorizedCheckPermissionServiceImpl(IRbacPermissionHandler rbacPermissionHandler) {
        this.rbacPermissionHandler = rbacPermissionHandler;
    }


    @Override
    public CheckPermissionInfo checkPermissionInfo(UserInfoDetails userInfoDetails, String method, String requestUri) {
        return rbacPermissionHandler.handler(userInfoDetails, requestUri, method);
    }


}
