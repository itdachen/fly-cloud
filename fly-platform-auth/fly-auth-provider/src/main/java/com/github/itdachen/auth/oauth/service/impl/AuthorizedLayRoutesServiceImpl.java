package com.github.itdachen.auth.oauth.service.impl;

import com.github.itdachen.auth.interfaces.constants.AppConstants;
import com.github.itdachen.auth.oauth.mapper.ILayClientRoutesMapper;
import com.github.itdachen.auth.oauth.service.IAuthorizedLayRoutesService;
import com.github.itdachen.framework.context.constants.UserTypeConstant;
import com.github.itdachen.framework.context.constants.YesOrNotConstant;
import com.github.itdachen.framework.context.userdetails.UserInfoDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 获取菜单列表
 *
 * @author 王大宸
 * @date 2025-01-23 15:34
 */
@Service(value = "com.github.itdachen.auth.oauth.service.impl.LayClientRoutesServiceImpl")
public class AuthorizedLayRoutesServiceImpl implements IAuthorizedLayRoutesService {

    private final ILayClientRoutesMapper layClientRoutesMapper;

    public AuthorizedLayRoutesServiceImpl(ILayClientRoutesMapper layClientRoutesMapper) {
        this.layClientRoutesMapper = layClientRoutesMapper;
    }

    /***
     * 获取菜单
     *
     * @author 王大宸
     * @date 2025/1/23 15:35
     * @return java.lang.Object
     */
    @Override
    public Object routes(UserInfoDetails userDetails) {
        if (UserTypeConstant.SUPER_ADMINISTRATOR.equals(userDetails.getUserType())
                && YesOrNotConstant.Y.equals(userDetails.getRoleFlag())) {
            return getTheSuperAdministratorMenu(AppConstants.APP_ID);
        }
        return getUserMenu(userDetails, AppConstants.APP_ID);
    }


    /***
     * 获取超级管理员菜单
     *
     * @author 王大宸
     * @date 2025/1/23 15:57
     * @param parentId parentId
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     */
    private List<Map<String, Object>> getTheSuperAdministratorMenu(String parentId) {
        List<Map<String, Object>> menus = layClientRoutesMapper.findMenuAll(parentId);
        if (null == menus || menus.isEmpty()) {
            return new ArrayList<>();
        }
        List<Map<String, Object>> children = new ArrayList<>();
        for (Map<String, Object> menu : menus) {
            if (!"dirt".equals(menu.get("type"))) {
                continue;
            }
            children = getTheSuperAdministratorMenu(String.valueOf(menu.get("menuId")));
            if (null == children || children.isEmpty()) {
                continue;
            }
            menu.put("children", children);
        }

        return menus;
    }

    /***
     * 获取用户菜单
     *
     * @author 王大宸
     * @date 2025/1/23 15:58
     * @param parentId parentId
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     */
    private List<Map<String, Object>> getUserMenu(UserInfoDetails userDetails, String parentId) {
        return new ArrayList<>();
    }


}
