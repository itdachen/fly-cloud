package com.github.itdachen.auth.oauth.service.impl;

import com.github.itdachen.auth.interfaces.constants.AppConstants;
import com.github.itdachen.auth.oauth.manager.IAuthorizedResourcesManager;
import com.github.itdachen.auth.oauth.mapper.IAuthorizedResourcesMapper;
import com.github.itdachen.auth.oauth.service.IAuthorizedPermissionService;
import com.github.itdachen.framework.context.userdetails.UserInfoDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * ClientPermissionServiceImpl
 *
 * @author 王大宸
 * @date 2025-02-05 15:07
 */
@Service
public class AuthorizedPermissionServiceImpl implements IAuthorizedPermissionService {

    private final IAuthorizedResourcesManager authorizedResourcesManager;
    private final IAuthorizedResourcesMapper authorizedResourcesMapper;


    public AuthorizedPermissionServiceImpl(IAuthorizedResourcesManager authorizedResourcesManager,
                                           IAuthorizedResourcesMapper authorizedResourcesMapper) {
        this.authorizedResourcesManager = authorizedResourcesManager;
        this.authorizedResourcesMapper = authorizedResourcesMapper;
    }


    @Override
    public List<String> permissions(UserInfoDetails userDetails) throws Exception {
        List<String> list = authorizedResourcesManager.obtainAccessibleElementId(userDetails);
        if (list.isEmpty()) {
            return new ArrayList<>();
        }
        return authorizedResourcesMapper.obtainAccessibleAuthCodeByElementIdList(AppConstants.APP_ID, list);
    }

}
