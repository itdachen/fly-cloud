package com.github.itdachen.auth.rpc;

import com.github.itdachen.auth.interfaces.permissions.ICheckPermissionInfoRpc;
import com.github.itdachen.auth.oauth.service.IAuthorizedCheckPermissionService;
import com.github.itdachen.framework.context.permission.CheckPermissionInfo;
import com.github.itdachen.framework.context.userdetails.UserInfoDetails;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 权限认证
 *
 * @author 王大宸
 * @date 2025-02-14 14:35
 */
@DubboService
public class AuthCheckPermissionInfoRpc implements ICheckPermissionInfoRpc {

    @Autowired
    private IAuthorizedCheckPermissionService authorizedCheckPermissionService;

    @Override
    public CheckPermissionInfo checkPermissionInfoMono(UserInfoDetails userInfoDetails, String method, String requestUri) {
        return authorizedCheckPermissionService.checkPermissionInfo(userInfoDetails, method, requestUri);
    }



}
