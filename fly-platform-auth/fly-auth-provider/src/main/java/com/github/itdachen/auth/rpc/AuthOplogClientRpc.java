package com.github.itdachen.auth.rpc;

import com.github.itdachen.auth.interfaces.entity.OplogInfoClient;
import com.github.itdachen.auth.interfaces.oplog.IGatewayClientOplogRpc;
import com.github.itdachen.auth.oauth.service.IAuthorizeOplogInfoService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 日志入库 RPC 处理
 *
 * @author 王大宸
 * @date 2025-02-14 10:09
 */
@DubboService
public class AuthOplogClientRpc implements IGatewayClientOplogRpc {

    @Autowired
    private IAuthorizeOplogInfoService authorizeOplogInfoService;

    /***
     * 添加日志
     *
     * @author 王大宸
     * @date 2025/2/10 10:38
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    @Override
    public void saveOplog(OplogInfoClient oplogInfoClient) {
        authorizeOplogInfoService.saveOplog(oplogInfoClient);
    }

    /***
     * 追加日志响应信息
     *
     * @author 王大宸
     * @date 2025/2/10 11:07
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    @Override
    public void appendOplog(OplogInfoClient oplogInfoClient) {
        authorizeOplogInfoService.appendOplog(oplogInfoClient);
    }

    /***
     * 追加日志响应信息
     *
     * @author 王大宸
     * @date 2025/2/10 11:07
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    @Override
    public void appendResponseOplog(OplogInfoClient oplogInfoClient) {
        authorizeOplogInfoService.appendResponseOplog(oplogInfoClient);
    }

    /***
     * 权限不足
     *
     * @author 王大宸
     * @date 2025/2/10 10:38
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    @Override
    public void deniedPermission(OplogInfoClient oplogInfoClient) {
        authorizeOplogInfoService.deniedPermission(oplogInfoClient);
    }

    /***
     * 删除日志
     *
     * @author 王大宸
     * @date 2025/2/10 10:38
     * @param oplogInfoClient oplogInfoClient
     * @return void
     */
    @Override
    public void remove(OplogInfoClient oplogInfoClient) {
        authorizeOplogInfoService.remove(oplogInfoClient);
    }


}
