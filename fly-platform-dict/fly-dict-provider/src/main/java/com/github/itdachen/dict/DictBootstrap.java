package com.github.itdachen.dict;

import com.github.itdachen.auth.client.EnableAuthClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * 数据字典启动入口类
 *
 * @author 王大宸
 * @date 2024-05-31 15:07
 */
@RefreshScope
@EnableScheduling
@EnableAuthClient  // 启动认证客户端
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages = "com.github.itdachen")
@MapperScan(basePackages = "com.github.itdachen.**.mapper")
public class DictBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(DictBootstrap.class, args);
    }

}