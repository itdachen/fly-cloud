package com.github.itdachen.gateway.ignore;

/**
 * 是否不需要认证的路径
 *
 * @author 王大宸
 * @date 2025-02-17 11:00
 */
public interface IIgnoreMatchersService {

    boolean ignoreMatchers(String uri);

}
