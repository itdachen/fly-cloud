package com.github.itdachen.gateway.ignore.impl;

import com.github.itdachen.boot.autoconfigure.cloud.gateway.ignore.GatewayIgnoreProperties;
import com.github.itdachen.gateway.ignore.IIgnoreMatchersService;
import com.github.itdachen.gateway.ignore.matcher.AntPathRequestMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 是否不需要认证的路径
 *
 * @author 王大宸
 * @date 2025-02-17 11:00
 */
@Service
public class IgnoreMatchersServiceImpl implements IIgnoreMatchersService {

    @Autowired
    private GatewayIgnoreProperties ignoreProperties;


    @Override
    public boolean ignoreMatchers(String uri) {
        List<String> matchers = ignoreProperties.getMatchers();
        String uriStarts = "";
        for (String matcher : matchers) {
            if (matcher.equals(uri)) {
                return true;
            }
            if (matcher.endsWith("/**")) {
                uriStarts = matcher.substring(0, matcher.length() - 3);
                if (uri.startsWith(uriStarts)) {
                    return true;
                }
            }
        }
        return false;
    }

}
