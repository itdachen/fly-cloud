package com.github.itdachen.gateway.oplog;

import com.github.itdachen.framework.context.permission.PermissionInfo;
import com.github.itdachen.framework.context.userdetails.UserInfoDetails;
import com.github.itdachen.framework.core.response.ServerResponse;
import org.springframework.web.server.ServerWebExchange;

/**
 * 网关日志
 *
 * @author 王大宸
 * @date 2025-02-10 10:08
 */
public interface IGatewayOplogHandler {

    /***
     * 添加日志
     *
     * @author 王大宸
     * @date 2025/2/10 10:16
     * @param webExchange webExchange
     * @param userInfoDetails userInfoDetails
     * @param oplogId oplogId
     * @return void
     */
    void saveOplog(ServerWebExchange webExchange, UserInfoDetails userInfoDetails, String oplogId);


    /***
     * 新增日志
     *
     * @author 王大宸
     * @date 2025/2/14 21:11
     * @param webExchange webExchange
     * @param userInfoDetails userInfoDetails
     * @param permissionInfo permissionInfo
     * @return void
     */
    String saveOplog(ServerWebExchange webExchange, UserInfoDetails userInfoDetails, PermissionInfo permissionInfo);

    /***
     * 追加日志基础信息
     *
     * @author 王大宸
     * @date 2025/2/10 10:16
     * @param userInfoDetails userInfoDetails
     * @param permissionInfo permissionInfo
     * @param oplogId oplogId
     * @return void
     */
    void appendFuncOplog(UserInfoDetails userInfoDetails, PermissionInfo permissionInfo, String oplogId);

    /***
     * 追加日志请求体
     *
     * @author 王大宸
     * @date 2025/2/10 10:16
     * @param userInfoDetails userInfoDetails
     * @param body body
     * @param oplogId oplogId
     * @return void
     */
    void appendRequestBody(UserInfoDetails userInfoDetails, String body, String oplogId);


    /***
     * 添加响应数据
     *
     * @author 王大宸
     * @date 2025/2/14 17:00
     * @param userInfoDetails userInfoDetails
     * @param redBody redBody
     * @param oplogId oplogId
     * @return void
     */
    void appendResponseBody(UserInfoDetails userInfoDetails, String redBody, String oplogId);

    /***
     * 权限不足
     *
     * @author 王大宸
     * @date 2025/2/10 10:17
     * @param userInfoDetails userInfoDetails
     * @param permissionInfo permissionInfo
     * @param oplogId oplogId
     * @return void
     */
    void deniedPermission(UserInfoDetails userInfoDetails,
                          PermissionInfo permissionInfo,
                          ServerResponse<Object> redBody,
                          String oplogId);


}
