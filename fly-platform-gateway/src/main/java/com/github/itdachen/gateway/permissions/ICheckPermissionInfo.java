package com.github.itdachen.gateway.permissions;

import com.github.itdachen.framework.context.permission.CheckPermissionInfo;
import com.github.itdachen.framework.context.userdetails.UserInfoDetails;
import reactor.core.publisher.Mono;

/**
 * ICheckPermissionInfo
 *
 * @author 王大宸
 * @date 2025-02-10 9:47
 */
public interface ICheckPermissionInfo {

    Mono<CheckPermissionInfo> checkPermissionInfoMono(UserInfoDetails userInfoDetails, String method, String requestUri);


}
