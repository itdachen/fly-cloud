package com.github.itdachen.gateway.permissions.impl;

import com.github.itdachen.auth.interfaces.permissions.ICheckPermissionInfoRpc;
import com.github.itdachen.boot.autoconfigure.app.AppInfoProperties;
import com.github.itdachen.framework.context.permission.CheckPermissionInfo;
import com.github.itdachen.framework.context.userdetails.UserInfoDetails;
import com.github.itdachen.gateway.constants.GatewayConstant;
import com.github.itdachen.gateway.permissions.ICheckPermissionInfo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/**
 * 通过 RPC 的方式获取认证权限
 *
 * @author 王大宸
 * @date 2025-02-10 9:52
 */
@Service
public class DubboRpcCheckPermissionInfo implements ICheckPermissionInfo {

    @DubboReference // 远程调用
    private ICheckPermissionInfoRpc checkPermissionInfoRpc;

    @Autowired
    private AppInfoProperties appInfoProperties;

    @Override
    public Mono<CheckPermissionInfo> checkPermissionInfoMono(UserInfoDetails userInfoDetails, String method, String requestUri) {
        final String contextPath = appInfoProperties.getContextPath();
        if (requestUri.startsWith(contextPath)) {
            requestUri = requestUri.substring(contextPath.length());
        }
        CheckPermissionInfo checkPermissionInfo = checkPermissionInfoRpc.checkPermissionInfoMono(userInfoDetails, method, requestUri);
        return Mono.just(checkPermissionInfo);
    }

}
