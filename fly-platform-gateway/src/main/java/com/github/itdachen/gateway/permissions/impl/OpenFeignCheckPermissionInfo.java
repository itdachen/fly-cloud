package com.github.itdachen.gateway.permissions.impl;

import com.github.itdachen.framework.context.BizContextHandler;
import com.github.itdachen.framework.context.constants.UserInfoConstant;
import com.github.itdachen.framework.context.permission.CheckPermissionInfo;
import com.github.itdachen.framework.context.userdetails.UserInfoDetails;
import com.github.itdachen.gateway.permissions.ICheckPermissionInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

/**
 * 通过 open feign 的方式获取认证权限
 * 单纯的记录一下, 目前不用
 *
 * @author 王大宸
 * @date 2025-02-10 9:47
 */
public class OpenFeignCheckPermissionInfo implements ICheckPermissionInfo {
    @Autowired
    private WebClient.Builder webClientBuilder;


    @Override
    public Mono<CheckPermissionInfo> checkPermissionInfoMono(UserInfoDetails userInfoDetails, String method, String requestUri) {
        return webClientBuilder.build().
                get()
                .uri("http://auth/auth/oauth/user/{roleId}/check/permission?requestMethod=" + method + "&requestUri=" + requestUri, userInfoDetails.getRoleId())
                .header(UserInfoConstant.HEADER_AUTHORIZATION, BizContextHandler.getToken())
                .retrieve()
                .bodyToMono(CheckPermissionInfo.class);

    }



}
