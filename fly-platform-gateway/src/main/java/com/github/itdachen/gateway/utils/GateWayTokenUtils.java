package com.github.itdachen.gateway.utils;

import com.github.itdachen.framework.context.constants.UserInfoConstant;
import com.github.itdachen.framework.core.utils.StringUtils;
import org.springframework.http.server.reactive.ServerHttpRequest;

import java.util.List;

/**
 * GateWayTokenUtils
 *
 * @author 王大宸
 * @date 2025-02-08 16:31
 */
public class GateWayTokenUtils {


    public static String getToken(ServerHttpRequest request) {
        List<String> strings = request.getHeaders().get(UserInfoConstant.HEADER_AUTHORIZATION);
        String authToken = null;
        if (null != strings && 0 < strings.size()) {
            authToken = strings.get(0);
        }

        // 如果请求头里面没有token,则看请求参数上是否存在 token
        if (StringUtils.isBlank(authToken)) {
            strings = request.getQueryParams().get(UserInfoConstant.TOKEN);
            if (strings != null) {
                authToken = strings.get(0);
            }
        }

        return authToken;
    }


}
