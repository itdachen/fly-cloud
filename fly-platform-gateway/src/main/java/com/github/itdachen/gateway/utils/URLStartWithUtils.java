package com.github.itdachen.gateway.utils;

/**
 * 判断 url 请求路径开头是否包含
 *
 * @author 王大宸
 * @date 2025-02-08 16:44
 */
public class URLStartWithUtils {

    /***
     * 判断 url 请求路径开头是否包含
     *
     * @author 王大宸
     * @date 2025/2/8 16:44
     * @param requestUri 请求路径
     * @param startWith  路径开头
     * @return boolean
     */
    public static boolean isStartWith(String requestUri, String startWith) {
        for (String s : startWith.split(",")) {
            if (requestUri.startsWith(s)) {
                return true;
            }
        }
        return false;
    }


}
