package com.github.itdachen.oss;

import com.github.itdachen.auth.client.EnableAuthClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * 文件上传项目启动入口
 *
 * @author 王大宸
 * @date 2025-01-01 16:51
 */
@EnableAuthClient  // 启动认证客户端
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages = "com.github.itdachen")
@MapperScan(basePackages = "com.github.itdachen.**.mapper")
public class FileOssBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(FileOssBootstrap.class, args);
    }

}