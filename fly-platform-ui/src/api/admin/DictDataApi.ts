import {HttpRequest} from '@/fly/http/rest/HttpRequest';
import {DictData, DictDataQuery} from '@/api/admin/model/DictDataModel';


/**
 * 请求路径
 */
const DICT_DATA_PATH = '/admin/dict/data';

/**
 * 数据字典信息表 接口
 *
 * @author 王大宸
 * @date 2025-01-15 20:38:26
 */
class DictDataApi extends HttpRequest< DictData, DictDataQuery, string > {

    constructor() {
        super(DICT_DATA_PATH)
    }


}

export default DictDataApi