import {HttpRequest} from '@/fly/http/rest/HttpRequest';
import {DictType, DictTypeQuery} from '@/api/admin/model/DictTypeModel';


/**
 * 请求路径
 */
const DICT_TYPE_PATH = '/admin/dict/type';

/**
 * 数据字典类型 接口
 *
 * @author 王大宸
 * @date 2025-01-15 20:38:26
 */
class DictTypeApi extends HttpRequest< DictType, DictTypeQuery, string > {

    constructor() {
        super(DICT_TYPE_PATH)
    }


}

export default DictTypeApi