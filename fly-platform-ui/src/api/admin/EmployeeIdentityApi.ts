import {HttpRequest} from '@/fly/http/rest/HttpRequest';
import {UserRoleInfo, UserRoleInfoQuery} from '@/api/admin/model/UserRoleInfoModel';


/**
 * 请求路径 EmployeeIdentity
 */
const EMPLOYEE_IDENTITY_PATH = '/admin/employee/identity';

/**
 * 身份信息 接口
 *
 * @author 王大宸
 * @date 2025-01-11 16:32:42
 */
class EmployeeIdentityApi extends HttpRequest< UserRoleInfo, UserRoleInfoQuery, string > {

    constructor() {
        super(EMPLOYEE_IDENTITY_PATH)
    }


}

export default EmployeeIdentityApi