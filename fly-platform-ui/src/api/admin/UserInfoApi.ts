import {HttpRequest} from '@/fly/http/rest/HttpRequest';
import {UserInfo, UserInfoQuery} from '@/api/admin/model/UserInfoModel';
import http from "@/fly/http";
import {ServerResponse} from "axios";


/**
 * 请求路径
 */
const USER_INFO_PATH = '/admin/user/inf';

/**
 * 基础用户信息 接口
 *
 * @author 王大宸
 * @date 2025-01-11 16:32:42
 */
class UserInfoApi extends HttpRequest<UserInfo, UserInfoQuery, string> {

    constructor() {
        super(USER_INFO_PATH)
    }

    /**
     * 获取机构/部门树
     */
    findUserByAccount(loginAccount: string) {
        return http.get(USER_INFO_PATH + '/account/' + loginAccount);
    }


}

export default UserInfoApi