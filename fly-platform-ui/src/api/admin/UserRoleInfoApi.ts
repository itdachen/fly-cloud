import {HttpRequest} from '@/fly/http/rest/HttpRequest';
import {UserRoleInfo, UserRoleInfoQuery} from '@/api/admin/model/UserRoleInfoModel';


/**
 * 请求路径
 */
const USER_ROLE_INFO_PATH = '/admin/user/role/info';

/**
 * 身份信息 接口
 *
 * @author 王大宸
 * @date 2025-01-11 16:32:42
 */
class UserRoleInfoApi extends HttpRequest< UserRoleInfo, UserRoleInfoQuery, string > {

    constructor() {
        super(USER_ROLE_INFO_PATH)
    }


}

export default UserRoleInfoApi