/*
 ++++++++++++++++++++++++++++++++++
 + 数据字典信息表
 + @author 王大宸
 + @date 2025-01-15 20:38:26
 ++++++++++++++++++++++++++++++++++
 */

import {BizQuery} from "@/fly/biz/BizModel";
import {reactive, ref} from "vue";
import {TableData} from "axios";

/**
 * 数据字典信息表 查询参数
 *
 * @author 王大宸
 * @date 2025-01-15 20:38:26
 */
export interface DictDataQuery extends BizQuery {
    /** 数据字典类型ID */
    typeId?: string,
    /** 字典类型, 例如:Y_OR_N */
    dictType?: string,
    /** 字典标签 */
    dictLabel?: string,
    /** 字典键值 */
    dictValue?: string,
    /** 是否展示/有效标志: Y-有效;N-无效 */
    validFlag?: string,
}


/**
 * 数据字典信息表 向后端传值对象
 *
 * @author 王大宸
 * @date 2025-01-15 20:38:26
 */
export interface DictData {
    /** 数据字典ID */
    id?: string,
    /** 数据字典类型ID */
    typeId?: string,
    /** 数据字典类型名称 */
    typeLabel?: string,
    /** 字典类型, 例如:Y_OR_N */
    dictType?: string,
    /** 字典标签 */
    dictLabel?: string,
    /** 字典键值 */
    dictValue?: string,
    /** 是否默认（1是 0否） */
    isDefault?: string,
    /** 字典排序 */
    dictSort?: string,
    /** 是否展示/有效标志: Y-有效;N-无效 */
    validFlag?: string,
    /** 备注 */
    remarks?: string
}


/**
 * 初始化方法
 */
export default function useDictDataBuilder() {

    /**
     * 实例化查询数据对象
     */
    const queryDictDataParams = reactive<DictDataQuery>({
        page: 1,
        limit: 10,
        /** 数据字典类型ID */
        typeId: '',
        /** 字典类型, 例如:Y_OR_N */
        dictType: '',
        /** 字典标签 */
        dictLabel: '',
        /** 字典键值 */
        dictValue: '',
        /** 是否展示/有效标志: Y-有效;N-无效 */
        validFlag: ''
    });

    /**
     * 分页数据
     */
    const tableDictDataData = reactive<TableData<DictData>>({
        loading: false,
        total: 0,
        rows: [],
    });

    /**
     * 实例化对象
     */
    const dictData = ref<DictData>({
        /** 数据字典ID */
        id: '',
        /** 数据字典类型ID */
        typeId: '',
        /** 数据字典类型名称 */
        typeLabel: '',
        /** 字典类型, 例如:Y_OR_N */
        dictType: '',
        /** 字典标签 */
        dictLabel: '',
        /** 字典键值 */
        dictValue: '',
        /** 是否默认（1是 0否） */
        isDefault: '',
        /** 字典排序 */
        dictSort: '',
        /** 是否展示/有效标志: Y-有效;N-无效 */
        validFlag: '',
        /** 备注 */
        remarks: ''
    });

    /**
     * 分页列表展示项
     */
    const dictDataColumns = [
        // {title: '字典类型', key: 'typeLabel', ellipsisTooltip: true, align: 'center'},
        // {title: '字典类型', key: 'dictType', ellipsisTooltip: true, align: 'center'},
        {title: '字典标签', key: 'dictLabel', ellipsisTooltip: true, align: 'center'},
        {title: '字典键值', key: 'dictValue', ellipsisTooltip: true, align: 'center'},
        {
            title: '是否默认', key: 'isDefault', ellipsisTooltip: true, align: 'center',
            width: 200,
            customSlot: function (obj: any) {
                return '0' === obj.data.validFlag ? '是' : '否';

            }
        },
        {
            title: '有效标志', key: 'validFlag', ellipsisTooltip: true, align: 'center', width: 200,
            customSlot: function (obj: any) {
                return 'Y' === obj.data.validFlag ? '有效' : '无效';

            }
        },
        {title: '字典排序', key: 'dictSort', ellipsisTooltip: true, align: 'center', width: 200},
        {title: '操作', width: '280px', customSlot: 'operator', key: 'operator', align: 'center', fixed: 'right'}
    ];

    /**
     *  新增/修改/查看 弹窗
     */
    const refDictDataComponent = ref();

    return {
        queryDictDataParams,
        tableDictDataData,
        dictData,
        dictDataColumns,
        refDictDataComponent
    }

}