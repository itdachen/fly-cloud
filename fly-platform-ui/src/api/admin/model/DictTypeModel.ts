/*
 ++++++++++++++++++++++++++++++++++
 + 数据字典类型
 + @author 王大宸
 + @date 2025-01-15 20:38:26
 ++++++++++++++++++++++++++++++++++
 */

import {BizQuery} from "@/fly/biz/BizModel";
import {reactive, ref} from "vue";
import {TableData} from "axios";

/**
 * 数据字典类型 查询参数
 *
 * @author 王大宸
 * @date 2025-01-15 20:38:26
 */
export interface DictTypeQuery extends BizQuery {
    /** 字典标签 */
    dictLabel?: string,
    /** 字典类型 */
    dictType?: string,
    /** 是否展示/有效标志: Y-有效;N-无效 */
    validFlag?: string,
}


/**
 * 数据字典类型 向后端传值对象
 *
 * @author 王大宸
 * @date 2025-01-15 20:38:26
 */
export interface DictType {
    /** 数据字典类型ID */
    id?: string,
    /** 字典标签 */
    dictLabel?: string,
    /** 字典类型 */
    dictType?: string,
    /** 字典排序 */
    dictSort?: string,
    /** 是否展示/有效标志: Y-有效;N-无效 */
    validFlag?: string,
    /** 备注 */
    remarks?: string
}


/**
 * 初始化方法
 */
export default function useDictTypeBuilder() {

    /**
     * 实例化查询数据对象
     */
    const queryDictTypeParams = reactive<DictTypeQuery>({
        page: 1,
        limit: 10,
        /** 字典标签 */
        dictLabel: '',
        /** 字典类型 */
        dictType: '',
        /** 是否展示/有效标志: Y-有效;N-无效 */
        validFlag: ''
    });

    /**
     * 分页数据
     */
    const tableDictTypeData = reactive<TableData<DictType>>({
        loading: false,
        total: 0,
        rows: [],
    });

    /**
     * 实例化对象
     */
    const dictType = ref<DictType>({
        /** 数据字典类型ID */
        id: '',
        /** 字典标签 */
        dictLabel: '',
        /** 字典类型 */
        dictType: '',
        /** 字典排序 */
        dictSort: '',
        /** 是否展示/有效标志: Y-有效;N-无效 */
        validFlag: '',
        /** 备注 */
        remarks: ''
    });

    /**
     * 分页列表展示项
     */
    const dictTypeColumns = [
        {title: '字典标签', key: 'dictLabel', ellipsisTooltip: true, align: 'center'},
        {title: '字典类型', key: 'dictType', ellipsisTooltip: true, align: 'center'},
        {
            title: '有效标志', key: 'validFlag', ellipsisTooltip: true,
            align: 'center',
            width: 150,
            customSlot: function (obj: any) {
                return 'Y' === obj.data.validFlag ? '有效' : '无效';

            }
        },
        {title: '操作', width: '400px', customSlot: 'operator', key: 'operator', align: 'center', fixed: 'right'}
    ];

    /**
     *  新增/修改/查看 弹窗
     */
    const refDictTypeComponent = ref();

    return {
        queryDictTypeParams,
        tableDictTypeData,
        dictType,
        dictTypeColumns,
        refDictTypeComponent
    }

}