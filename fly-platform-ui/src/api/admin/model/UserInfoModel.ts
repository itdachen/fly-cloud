/*
 ++++++++++++++++++++++++++++++++++
 + 基础用户信息
 + @author 王大宸
 + @date 2025-01-11 16:32:42
 ++++++++++++++++++++++++++++++++++
 */

import {BizQuery} from "@/fly/biz/BizModel";
import {reactive, ref} from "vue";
import {TableData} from "axios";

/**
 * 基础用户信息 查询参数
 *
 * @author 王大宸
 * @date 2025-01-11 16:32:42
 */
export interface UserInfoQuery extends BizQuery {
    /** 登录账号/人员代码 */
    username?: string,
    /** 昵称 */
    nickName?: string,
    /** 性别 */
    sex?: string,
    /** 电话号码 */
    telephone?: string,
    /** 用户类型 */
    userType?: string,
    /** 有效标志: Y-是;N-否; 与 ta_fly_login_info 表保持一致 */
    validFlag?: string
}


/**
 * 基础用户信息 向后端传值对象
 *
 * @author 王大宸
 * @date 2025-01-11 16:32:42
 */
export interface UserInfo {
    /** 用户id唯一标识 */
    id?: string,
    /** 租户ID */
    tenantId?: string,
    /** 租户名称 */
    tenantTitle?: string,
    /** 登录账号/人员代码 */
    username?: string,
    /** 昵称 */
    nickName?: string,
    /** 身份证号码 */
    idCard?: string,
    /** 性别 */
    sex?: string,
    /** 电话号码 */
    telephone?: string,
    /** 头像 */
    avatar?: string,
    /** 电子邮箱 */
    eMailBox?: string,
    /** 用户类型 */
    userType?: string,
    /** 有效标志: Y-是;N-否; 与 ta_fly_login_info 表保持一致 */
    validFlag?: string,
    /** 备注 */
    remarks?: string
}


/**
 * 初始化方法
 */
export default function useUserInfoBuilder() {

    /**
     * 实例化查询数据对象
     */
    const queryUserInfoParams = reactive<UserInfoQuery>({
        page: 1,
        limit: 10,
        /** 登录账号/人员代码 */
        username: '',
        /** 昵称 */
        nickName: '',
        /** 性别 */
        sex: '',
        /** 电话号码 */
        telephone: '',
        /** 用户类型 */
        userType: '',
        /** 有效标志: Y-是;N-否; 与 ta_fly_login_info 表保持一致 */
        validFlag: ''
    });

    /**
     * 分页数据
     */
    const tableUserInfoData = reactive<TableData<UserInfo>>({
        loading: false,
        total: 0,
        rows: [],
    });

    /**
     * 实例化对象
     */
    const userInfo = ref<UserInfo>({
        /** 用户id唯一标识 */
        id: '',
        /** 租户ID */
        tenantId: '',
        /** 租户名称 */
        tenantTitle: '',
        /** 登录账号/人员代码 */
        username: '',
        /** 昵称 */
        nickName: '',
        /** 身份证号码 */
        idCard: '',
        /** 性别 */
        sex: '',
        /** 电话号码 */
        telephone: '',
        /** 头像 */
        avatar: '',
        /** 电子邮箱 */
        eMailBox: '',
        /** 用户类型 */
        userType: '',
        /** 有效标志: Y-是;N-否; 与 ta_fly_login_info 表保持一致 */
        validFlag: '',
        /** 备注 */
        remarks: '',
    });

    /**
     * 分页列表展示项
     */
    const userInfoColumns = [
        {title: '昵称', key: 'nickName', ellipsisTooltip: true, align: 'center'},
        // {title: '头像', key: 'avatar', ellipsisTooltip: true, align: 'center'},

        {
            title: '性别', key: 'sex', ellipsisTooltip: true, width: '150px', align: 'center',
            customSlot: function (obj: any) {
                if ('1' === obj.data.sex) {
                    return '男';
                }
                if ('2' === obj.data.sex) {
                    return '女';
                }
                return '未知';
            }
        },
        {title: '电话号码', key: 'telephone', ellipsisTooltip: true, align: 'center'},
        {title: '邮箱', key: 'eMailBox', ellipsisTooltip: true, align: 'center'},
        {title: '身份证号码', key: 'idCard', ellipsisTooltip: true, align: 'center'},
        //  {
        //      title: '有效标志',
        //      key: 'validFlag',
        //      ellipsisTooltip: true,
        //      align: 'center',
        //      customSlot: function (obj: any) {
        //          return 'Y' === obj.data.validFlag ? '有效' : '无效';
        //      }
        //  },
        {title: '操作', width: '400px', customSlot: 'operator', key: 'operator', align: 'center', fixed: 'right'}
    ];

    /**
     *  新增/修改/查看 弹窗
     */
    const refUserInfoComponent = ref();

    return {
        queryUserInfoParams,
        tableUserInfoData,
        userInfo,
        userInfoColumns,
        refUserInfoComponent
    }

}