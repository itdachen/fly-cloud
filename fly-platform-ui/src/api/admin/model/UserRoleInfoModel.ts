/*
 ++++++++++++++++++++++++++++++++++
 + 身份信息
 + @author 王大宸
 + @date 2025-01-11 16:32:42
 ++++++++++++++++++++++++++++++++++
 */

import {BizQuery} from "@/fly/biz/BizModel";
import {reactive, ref} from "vue";
import {TableData} from "axios";

/**
 * 身份信息 查询参数
 *
 * @author 王大宸
 * @date 2025-01-11 16:32:42
 */
export interface UserRoleInfoQuery extends BizQuery {
    /** 用户ID */
    userId?: string,
    /** 登录账号/人员代码 */
    username?: string,
    /** 昵称 */
    nickName?: string,
    /** 身份名称 */
    roleName?: string,
    /** 主身份标志: Y/N */
    mainFlag?: string,
    /** 有效标志: Y/N */
    validFlag?: string,
    /** 身份部门ID */
    deptCode?: string,
    /** 身份部门名称 */
    deptTitle?: string,
    /** 身份有效期起 */
    startTime?: string,
    /** 身份有效期止 */
    endTime?: string,
}


/**
 * 身份信息 向后端传值对象
 *
 * @author 王大宸
 * @date 2025-01-11 16:32:42
 */
export interface UserRoleInfo {
    /** 身份ID */
    id?: string,
    /** 租户ID */
    tenantId?: string,
    /** 用户ID */
    userId?: string,
    /** 登录账号/人员代码 */
    username?: string,
    /** 昵称 */
    nickName?: string,
    /** 身份名称 */
    roleName?: string,
    /** 主身份标志: Y/N */
    mainFlag?: string,
    /** 有效标志: Y/N */
    validFlag?: string,
    /** 身份部门ID */
    deptCode?: string,
    /** 身份部门名称 */
    deptTitle?: string,
    /** 身份有效期起 */
    startTime?: string,
    /** 身份有效期止 */
    endTime?: string,
    /** 排序 */
    orderNum?: string,
    /** 备注 */
    remarks?: string
}


/**
 * 初始化方法
 */
export default function useUserRoleInfoBuilder() {

    /**
     * 实例化查询数据对象
     */
    const queryUserRoleInfoParams = reactive<UserRoleInfoQuery>({
        page: 1,
        limit: 10,
        /** 用户ID */
        userId: '',
        /** 登录账号/人员代码 */
        username: '',
        /** 昵称 */
        nickName: '',
        /** 身份名称 */
        roleName: '',
        /** 主身份标志: Y/N */
        mainFlag: '',
        /** 有效标志: Y/N */
        validFlag: '',
        /** 身份部门ID */
        deptCode: '11',
        /** 身份部门名称 */
        deptTitle: '',
        /** 身份有效期起 */
        startTime: '',
        /** 身份有效期止 */
        endTime: ''
    });

    /**
     * 分页数据
     */
    const tableUserRoleInfoData = reactive<TableData<UserRoleInfo>>({
        loading: false,
        total: 0,
        rows: [],
    });

    /**
     * 实例化对象
     */
    const userRoleInfo = ref<UserRoleInfo>({
        /** 身份ID */
        id: '',
        /** 租户ID */
        tenantId: '',
        /** 用户ID */
        userId: '',
        /** 登录账号/人员代码 */
        username: '',
        /** 昵称 */
        nickName: '',
        /** 身份名称 */
        roleName: '',
        /** 主身份标志: Y/N */
        mainFlag: '',
        /** 有效标志: Y/N */
        validFlag: '',
        /** 身份部门ID */
        deptCode: '',
        /** 身份部门名称 */
        deptTitle: '',
        /** 身份有效期起 */
        startTime: '',
        /** 身份有效期止 */
        endTime: '',
        /** 排序 */
        orderNum: '',
        /** 备注 */
        remarks: ''
    });

    /**
     * 分页列表展示项
     */
    const userRoleInfoColumns = [
        {title: '昵称', key: 'nickName', ellipsisTooltip: true, width: '120px', align: 'center'},
        {title: '身份名称', key: 'roleName', ellipsisTooltip: true, align: 'center'},
        //   {title: '所属部门', key: 'deptTitle', ellipsisTooltip: true, align: 'center'},
        {title: '有效期起', key: 'startTime', ellipsisTooltip: true, width: '100px', align: 'center'},
        {title: '有效期止', key: 'endTime', ellipsisTooltip: true, width: '100px', align: 'center'},
        {
            title: '主身份标志 ', key: 'mainFlag', ellipsisTooltip: true, align: 'center',
            width: '120px',
            customSlot: function (obj: any) {
                return 'Y' === obj.data.mainFlag ? '是' : '否';
            }
        },
        {
            title: '有效标志 ', key: 'validFlag', ellipsisTooltip: true, align: 'center',
            width: '100px',
            customSlot: function (obj: any) {
                return 'Y' === obj.data.validFlag ? '是' : '否';
            }
        },
        // {title: '排序', key: 'orderNum', ellipsisTooltip: true, width: '100px', align: 'center'},
        {title: '操作', width: '400px', customSlot: 'operator', key: 'operator', align: 'center', fixed: 'right'}
    ];

    /**
     *  新增/修改/查看 弹窗
     */
    const refUserRoleInfoComponent = ref();

    return {
        queryUserRoleInfoParams,
        tableUserRoleInfoData,
        userRoleInfo,
        userRoleInfoColumns,
        refUserRoleInfoComponent
    }

}