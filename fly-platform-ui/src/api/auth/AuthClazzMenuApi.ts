import {AuthClazzMenu} from '@/api/auth/model/AuthClazzMenuModel';
import http from "@/fly/http";


/**
 * 请求路径
 */
const AUTH_CLAZZ_MENU_PATH = '/admin/auth/clazz/menu';

/**
 * 岗位菜单 接口
 *
 * @author 王大宸
 * @date 2025-01-01 17:12:50
 */
class AuthClazzMenuApi   {

    /**
     * 新增
     * @param data
     */
    saveInfo(data: AuthClazzMenu) {
        return http.post(AUTH_CLAZZ_MENU_PATH, data);
    }


}

export default AuthClazzMenuApi