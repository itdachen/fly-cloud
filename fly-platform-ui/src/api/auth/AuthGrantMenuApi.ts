import {AuthGrantMenu} from '@/api/auth/model/AuthGrantMenuModel';
import http from "@/fly/http/index";

/**
 * 请求路径
 */
const AUTH_GRANT_MENU_PATH = '/admin/auth/grant/menu';

/**
 * 权限下发 接口
 *
 * @author 王大宸
 * @date 2025-01-01 17:12:49
 */
class AuthGrantMenuApi {

    /**
     * 获取菜单按钮树
     * @param clazzCode
     */
    findMenuElementTree(clazzCode: string | undefined) {
        return http.get(AUTH_GRANT_MENU_PATH + '/list?clazzCode=' + clazzCode + '&appId=');
    }

    /**
     * 新增
     * @param data
     */
    saveInfo(data: AuthGrantMenu) {
        return http.post(AUTH_GRANT_MENU_PATH, data);
    }


}

export default AuthGrantMenuApi