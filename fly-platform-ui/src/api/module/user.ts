import Http from '../http';

import http from "@/fly/http";

// export const login = function(loginForm: any) {
//     return Http.post('/user/login', loginForm)
// }


// export const menu = function () {
//     return Http.get('/user/menu')
// }


// export const permission = function() {
//     return Http.get('/user/permission')
// }


// export const login = function (loginForm: any) {
//     return http.post('/user/login', loginForm)
// }

export const menu = function() {
    return http.get('/auth/authorized/client/routes')
}

export const permission = function () {
    return http.get('/auth/authorized/client/permissions')
}
