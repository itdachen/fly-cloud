import {layer} from "@layui/layui-vue";

import {StringUtils} from '@/fly/utils/StringUtils';
import useDictDataBuilder, {DictData, DictDataQuery} from '@/api/admin/model/DictDataModel';
import DictDataApi from '@/api/admin/DictDataApi';
import {FormTypeEnum} from "@/fly/biz/BizModel";
import {LayTableUtils} from "@/fly/utils/LayTableUtils";
import useLayTableComposable from "@/fly/table";
import {ref} from "vue";
import {DictType} from "@/api/admin/model/DictTypeModel";

const {
    dictData,
    dictDataColumns,
    queryDictDataParams,
    tableDictDataData,
    refDictDataComponent
} = useDictDataBuilder();
const {flyLayPage} = useLayTableComposable();

const dictDataApi = new DictDataApi();


/**
 * 数据字典信息表 处理
 *
 * @author 王大宸
 * @date 2025-01-15 20:38:26
 */
export default function useDictDataComposable() {


    /**
     * 实例化对象
     */
    const dictType = ref<DictType>({
        /** 数据字典类型ID */
        id: '',
        /** 字典标签 */
        dictLabel: '',
        /** 字典类型 */
        dictType: '',
        /** 是否展示/有效标志: Y-有效;N-无效 */
        validFlag: '',
        /** 备注 */
        remarks: ''
    });

    /**
     * 加载分页数据
     * @author 王大宸
     * @param params
     */
    const loadTableDictDataData = (params: DictDataQuery) => {
        tableDictDataData.loading = true;
        dictDataApi.page(params).then(res => {
            tableDictDataData.total = res.data.total;
            tableDictDataData.rows = res.data.rows;
            tableDictDataData.loading = false;
        });
    };

    /**
     * 重新加载数据
     * @author 王大宸
     * @param page
     * @param limit
     */
    const reloadDictDataDate = (page: number = 1, limit: number = 10) => {
        queryDictDataParams.page = page;
        queryDictDataParams.limit = limit;
        loadTableDictDataData(queryDictDataParams);
    };

    /**
     * 新增按钮
     */
    const onTapDictDataAdd = () => {
        console.log('onTapDictDataAdd', dictType)
        refDictDataComponent.value?.open(FormTypeEnum.ADD, dictType.value, null);
    }

    /**
     * 编辑按钮
     * @param data 编辑时的数据信息
     */
    const onTapDictDataEdit = (data: DictData) => {
        refDictDataComponent.value?.open(FormTypeEnum.EDIT, dictType.value, data);
    }

    /**
     * 查看按钮
     * @param data 查看时的数据信息
     */
    const onTapDictDataView = (data: DictData) => {
        refDictDataComponent.value?.open(FormTypeEnum.VIEW, dictType.value, data);
    }

    /**
     * 数据保存处理(新增/编辑)
     * @param data 保存的数据
     */
    const dictDataDataHandler = (data: DictData) => {
        if (StringUtils.isEmpty(data.id)) {
            dictDataApi.saveInfo(data).then((res) => {
                reloadDictDataDate(1, flyLayPage.limit); // 表格重新加载数据
                layer.msg(res.msg, {time: 1500, icon: 1}); // 操作成功提示
                refDictDataComponent.value?.onTapClose();  // 关闭弹窗
            })
        } else {
            dictDataApi.updateInfo(data, data.id).then((res) => {
                reloadDictDataDate(1, flyLayPage.limit);  // 表格重新加载数据
                layer.msg(res.msg, {time: 1500, icon: 1}); // 操作成功提示
                refDictDataComponent.value?.onTapClose();  // 关闭弹窗
            })
        }
    }


    /**
     * 删除按钮
     * @param id 需要删除的数据主键
     */
    const removeDictDataHandler = (id: string, title?: string) => {
        LayTableUtils.remove({
            title: title,
            callback: () => {
                dictDataApi.remove(id).then((res) => {
                    reloadDictDataDate(1, flyLayPage.limit);
                    layer.msg(res.msg, {time: 1500, icon: 1});
                })
            }
        })
    }


    return {
        dictType,
        refDictDataComponent,
        dictData,
        tableDictDataData,
        dictDataColumns,
        queryDictDataParams,
        /* 加载表格 */
        reloadDictDataDate,
        loadTableDictDataData,

        /* 新增, 编辑, 查看按钮 */
        onTapDictDataAdd,
        onTapDictDataEdit,
        onTapDictDataView,

        /* 接口 */
        removeDictDataHandler,
        dictDataDataHandler,
        flyLayPage

    };
}