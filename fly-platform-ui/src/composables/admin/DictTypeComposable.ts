
import {layer} from "@layui/layui-vue";

import {StringUtils} from '@/fly/utils/StringUtils';
import useDictTypeBuilder, {DictType, DictTypeQuery} from '@/api/admin/model/DictTypeModel';
import DictTypeApi from '@/api/admin/DictTypeApi';
import {FormTypeEnum} from "@/fly/biz/BizModel";
import {LayTableUtils} from "@/fly/utils/LayTableUtils";
import useLayTableComposable from "@/fly/table";
import {ref} from "vue";

const {
    dictType,
    dictTypeColumns,
    queryDictTypeParams,
    tableDictTypeData,
    refDictTypeComponent
} = useDictTypeBuilder();
const {flyLayPage} = useLayTableComposable();

const dictTypeApi = new DictTypeApi();


/**
 * 数据字典类型 处理
 *
 * @author 王大宸
 * @date 2025-01-15 20:38:26
 */
export default function useDictTypeComposable() {


    /**
     * 加载分页数据
     * @author 王大宸
     * @param params
     */
    const loadTableDictTypeData = (params: DictTypeQuery) => {
        tableDictTypeData.loading = true;
        dictTypeApi.page(params).then(res => {
            tableDictTypeData.total = res.data.total;
            tableDictTypeData.rows = res.data.rows;
            tableDictTypeData.loading = false;
        });
    };

    /**
     * 重新加载数据
     * @author 王大宸
     * @param page
     * @param limit
     */
    const reloadDictTypeDate = (page: number = 1, limit: number = 10) => {
        queryDictTypeParams.page = page;
        queryDictTypeParams.limit = limit;
        loadTableDictTypeData(queryDictTypeParams);
    };

    /**
     * 新增按钮
     */
    const onTapDictTypeAdd = () => {
        refDictTypeComponent.value?.open(FormTypeEnum.ADD, null);
    }

    /**
     * 编辑按钮
     * @param data 编辑时的数据信息
     */
    const onTapDictTypeEdit = (data: DictType) => {
        refDictTypeComponent.value?.open(FormTypeEnum.EDIT, data);
    }

    /**
     * 查看按钮
     * @param data 查看时的数据信息
     */
    const onTapDictTypeView = (data: DictType) => {
        refDictTypeComponent.value?.open(FormTypeEnum.VIEW, data);
    }

    /**
     * 数据保存处理(新增/编辑)
     * @param data 保存的数据
     */
    const dictTypeDataHandler = (data: DictType) => {
      if (StringUtils.isEmpty(data.id)){
         dictTypeApi.saveInfo(data).then((res) => {
              reloadDictTypeDate(1, flyLayPage.limit); // 表格重新加载数据
              layer.msg(res.msg, {time: 1500, icon: 1}); // 操作成功提示
              refDictTypeComponent.value?.onTapClose();  // 关闭弹窗
          })
      } else {
          dictTypeApi.updateInfo(data, data.id).then((res) => {
              reloadDictTypeDate(1, flyLayPage.limit);  // 表格重新加载数据
              layer.msg(res.msg, {time: 1500, icon: 1}); // 操作成功提示
              refDictTypeComponent.value?.onTapClose();  // 关闭弹窗
          })
      }
    }
    

    /**
     * 删除按钮
     * @param id 需要删除的数据主键
     */
    const removeDictTypeHandler = (id: string, title?: string) => {
        LayTableUtils.remove({
            title: title,
            callback: () => {
                dictTypeApi.remove(id).then((res) => {
                    reloadDictTypeDate(1, flyLayPage.limit);
                    layer.msg(res.msg, {time: 1500, icon: 1});
                })
            }
        })
    }


    return {
        refDictTypeComponent,
        dictType,
        tableDictTypeData,
        dictTypeColumns,
        queryDictTypeParams,
        /* 加载表格 */
        reloadDictTypeDate,
        loadTableDictTypeData,

        /* 新增, 编辑, 查看按钮 */
        onTapDictTypeAdd,
        onTapDictTypeEdit,
        onTapDictTypeView,

        /* 接口 */
        removeDictTypeHandler,
        dictTypeDataHandler,
        flyLayPage

    };
}