import {layer} from "@layui/layui-vue";

import {StringUtils} from '@/fly/utils/StringUtils';
import useUserInfoBuilder, {UserInfo, UserInfoQuery} from '@/api/admin/model/UserInfoModel';
import UserInfoApi from '@/api/admin/UserInfoApi';
import {FormTypeEnum} from "@/fly/biz/BizModel";
import {LayTableUtils} from "@/fly/utils/LayTableUtils";
import useLayTableComposable from "@/fly/table";

const {
    userInfo,
    userInfoColumns,
    queryUserInfoParams,
    tableUserInfoData,
    refUserInfoComponent
} = useUserInfoBuilder();
const {flyLayPage} = useLayTableComposable();

const userInfoApi = new UserInfoApi();


/**
 * 基础用户信息 处理
 *
 * @author 王大宸
 * @date 2025-01-11 16:32:42
 */
export default function useUserInfoComposable() {

    /**
     * 加载分页数据
     * @author 王大宸
     * @param params
     */
    const loadTableUserInfoData = (params: UserInfoQuery) => {
        tableUserInfoData.loading = true;
        userInfoApi.page(params).then(res => {
            tableUserInfoData.total = res.data.total;
            tableUserInfoData.rows = res.data.rows;
            tableUserInfoData.loading = false;
        });
    };

    /**
     * 重新加载数据
     * @author 王大宸
     * @param page
     * @param limit
     */
    const reloadUserInfoDate = (page: number = 1, limit: number = 10) => {
        queryUserInfoParams.page = page;
        queryUserInfoParams.limit = limit;
        loadTableUserInfoData(queryUserInfoParams);
    };

    /**
     * 新增按钮
     */
    const onTapUserInfoAdd = () => {
        refUserInfoComponent.value?.open(FormTypeEnum.ADD, null);
    }

    /**
     * 编辑按钮
     * @param data 编辑时的数据信息
     */
    const onTapUserInfoEdit = (data: UserInfo) => {
        refUserInfoComponent.value?.open(FormTypeEnum.EDIT, data);
    }

    /**
     * 查看按钮
     * @param data 查看时的数据信息
     */
    const onTapUserInfoView = (data: UserInfo) => {
        refUserInfoComponent.value?.open(FormTypeEnum.VIEW, data);
    }

    /**
     * 数据保存处理(新增/编辑)
     * @param data 保存的数据
     */
    const userInfoDataHandler = (data: UserInfo) => {
        if (StringUtils.isEmpty(data.id)) {
            userInfoApi.saveInfo(data).then((res) => {
                reloadUserInfoDate(1, flyLayPage.limit); // 表格重新加载数据
                layer.msg(res.msg, {time: 1500, icon: 1}); // 操作成功提示
                refUserInfoComponent.value?.onTapClose();  // 关闭弹窗
            }, err => {
                console.log('err', err)
            })
        } else {
            userInfoApi.updateInfo(data, data.id).then((res) => {
                reloadUserInfoDate(1, flyLayPage.limit);  // 表格重新加载数据
                layer.msg(res.msg, {time: 1500, icon: 1}); // 操作成功提示
                refUserInfoComponent.value?.onTapClose();  // 关闭弹窗
            })
        }
    }


    /**
     * 删除按钮
     * @param id 需要删除的数据主键
     */
    const removeUserInfoHandler = (id: string, title?: string) => {
        LayTableUtils.remove({
            title: title,
            callback: () => {
                userInfoApi.remove(id).then((res) => {
                    reloadUserInfoDate(1, flyLayPage.limit);
                    layer.msg(res.msg, {time: 1500, icon: 1});
                })
            }
        })
    }


    return {
        refUserInfoComponent,
        userInfo,
        tableUserInfoData,
        userInfoColumns,
        queryUserInfoParams,
        /* 加载表格 */
        reloadUserInfoDate,
        loadTableUserInfoData,

        /* 新增, 编辑, 查看按钮 */
        onTapUserInfoAdd,
        onTapUserInfoEdit,
        onTapUserInfoView,

        /* 接口 */
        removeUserInfoHandler,
        userInfoDataHandler,
        flyLayPage

    };
}