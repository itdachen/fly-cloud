import {defineStore} from 'pinia'
import {menu, permission} from "../api/module/user";

export const useUserStore = defineStore({
    id: 'user',
    state: () => {
        return {
            token: '',
            userInfo: {},
            permissions: [],
            menus: [],
        }
    },
    actions: {
        async loadMenus() {
            const {data, status} = await menu();
            if (status == 200) {
                this.menus = data;
            }
        },
        async loadPermissions() {
            const {data, status} = await permission();
            if (status == 200) {
                this.permissions = data;
            }
        }
    },
    persist: {
        storage: localStorage,
        paths: ['token', 'userInfo', 'permissions', 'menus'],
    }
})