package com.github.itdachen.shop.auth.client;

import com.github.itdachen.shop.auth.client.runner.ShopAuthorizedClientTokenSecretRunner;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 启动商城认证客户端注解
 *
 * @author 王大宸
 * @date 2025-03-03 15:43
 */
@Inherited
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({ShopAuthorizedClientTokenSecretRunner.class})
public @interface EnableShopAuthClient {
}
