package com.github.itdachen.shop.auth.client.runner;

import com.github.itdachen.auth.interfaces.IShopAuthClientTokenSecretRpc;
import com.github.itdachen.auth.interfaces.key.ShopTokenUserPubKey;
import com.github.itdachen.boot.autoconfigure.cloud.auth.properties.CloudAppClientProperties;
import com.github.itdachen.cloud.jwt.IVerifyTicketPublicKeyHelper;
import com.github.itdachen.cloud.jwt.parse.TokenPublicKey;
import com.github.itdachen.framework.core.response.ServerResponse;
import org.apache.dubbo.config.annotation.DubboReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.scheduling.annotation.Scheduled;

import java.security.PublicKey;

/**
 * ShopAuthorizedClientTokenSecretRunner
 *
 * @author 王大宸
 * @date 2025-03-03 15:44
 */
public class ShopAuthorizedClientTokenSecretRunner implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger(ShopAuthorizedClientTokenSecretRunner.class);

    @Autowired
    private TokenPublicKey tokenPublicKey;
    @Autowired
    private IVerifyTicketPublicKeyHelper verifyTicketPublicKeyHelper;

    @DubboReference // 远程调用
    private IShopAuthClientTokenSecretRpc clientTokenSecretRpc;

    private final CloudAppClientProperties appClientProperties;

    public ShopAuthorizedClientTokenSecretRunner(CloudAppClientProperties appClientProperties) {
        this.appClientProperties = appClientProperties;
    }

    @Override
    public void run(String... args) throws Exception {
        logger.info("正在初始化用户 pubKey");
        try {
            refreshUserSecretKey();
            logger.info("初始化用户 pubKey 完成");
        } catch (Exception e) {
            logger.error("初始化用户 pubKey 失败, 1 分钟后自动重试!", e);
        }
    }

    /***
     * 每隔一分钟同步一次
     *
     * @author 王大宸
     * @date 2023/5/5 15:09
     * @return void
     */
    @Scheduled(cron = "0 0/1 * * * ?")
    public void refreshUserSecretKey() throws Exception {
        ServerResponse<ShopTokenUserPubKey> secretPublicKey = clientTokenSecretRpc.getSecretPublicKey(
                appClientProperties.getAppId(),
                appClientProperties.getAppSecret()
        );

        ShopTokenUserPubKey pubKey = secretPublicKey.getData();

        PublicKey publicKey = verifyTicketPublicKeyHelper.publicKey(pubKey.getUserPubKey(), pubKey.getAlgorithm());

        this.tokenPublicKey.setPublicKey(publicKey);
    }

}
