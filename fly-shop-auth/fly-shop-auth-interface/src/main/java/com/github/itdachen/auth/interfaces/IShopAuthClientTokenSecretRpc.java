package com.github.itdachen.auth.interfaces;

import com.github.itdachen.auth.interfaces.key.ShopTokenUserPubKey;
import com.github.itdachen.framework.core.response.ServerResponse;

/**
 * IShopAuthClientTokenSecretRpc
 *
 * @author 王大宸
 * @date 2025-03-03 15:42
 */
public interface IShopAuthClientTokenSecretRpc {

    ServerResponse<ShopTokenUserPubKey> getSecretPublicKey(String appId, String appSecret) throws Exception;

}
