package com.github.itdachen.auth.interfaces.key;

import java.io.Serializable;

/**
 * ShopTokenUserPubKey
 *
 * @author 王大宸
 * @date 2025-03-03 15:41
 */
public class ShopTokenUserPubKey implements Serializable {
    private static final long serialVersionUID = 2599565458276273529L;

    /**
     * 公钥 base64
     */
    private String userPubKey;

    /**
     * 算法
     */
    private String algorithm;

    public ShopTokenUserPubKey(String userPubKey, String algorithm) {
        this.userPubKey = userPubKey;
        this.algorithm = algorithm;
    }

    public String getUserPubKey() {
        return userPubKey;
    }

    public void setUserPubKey(String userPubKey) {
        this.userPubKey = userPubKey;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }
}
